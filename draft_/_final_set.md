## 28.

### El Bolsón, Rio Negro

O ruído das palmas era súbito mais forte do que o murmúrio da multidão; ritmadas, quase monótonas, provocadas com o propósito claro de chamar a atenção de quem estivesse distraído com as miudezas da feira de artesanato, as empanadas caseiras, as frutas vermelhas, os potes de geleia. Téo, do outro lado da praça, as escutou como um chamado de outro tempo: os anos vividos no subúrbio de Buenos Aires e esse solidário hábito argentino de convocar a gente a bater palmas quando se encontrava criança perdida num aglomerado de gente. Seu primo mais velho, diziam, vivia se perdendo quando pequeno.

Pensou nos últimos meses, no último ano: a sina de investigações complicadas quando sua especialidade

por dizer: o que de fato sabia fazer

era seguir cônjuge infiel.

Que ele sabia de encontrar gente desaparecida?

Levantou-se e cruzou o cimento até a borda da praça, por onde se estendia a feira de fim de semana. Mais gente se havia juntado ao chamado e as palmas marcavam seus passos. Percebeu em algumas senhoras o olhar esperançoso de que fosse ele o pai encontrado.

Um menino de cinco ou seis anos estava parado no epicentro do ruído. Uma mulher de uns trinta anos, ao lado dele, parecia decidida a manter o ritmo enquanto não aparecesse um responsável para reclamar a criança. Ela usava calças largas e coloridas como muitos dos artesãos, e um agasalho de lã verde e azul com a estampa andina de lhamas enfileiradas; as mangas puxadas até o cotovelo. Um lenço marrom lhe cobria a cabeça e deixava escapar um par de tranças de um castanho claro, quase loiro. Seria muito branca não fosse o óbvio bronzeado e algo dessa poeira cinzenta que se agarra à pele e se infiltra na alma de tudo e todos naquela região. Os pés, descalços, estavam imundos.

Téo juntou-se às palmas. Não viu quando uma moça jovem se aproximou e tomou o menino nos braços. As palmas cessaram, a mãe agradeceu a mulher de tranças, e as pessoas seguiram adiante com seus passeios.

A artesã retomou seu posto atrás de uma das mesas de pulseiras e brincos e Téo voltou a pensar em seu primo mais velho: a insistência de Pablo contra aquela busca e a fúria em seu olhar na última vez que se viram, quando Téo o encontrou um dia antes de tomar o rumo de Córdoba.

— _Yo no quiero encontrarlo_ — ele havia dito. — _No me interesa que pasó._

Era estar dividido entre o companheirismo futebolístico e a fraternidade intelectual: Pablo, o primo mais velho, companhia para os estádios, para as partidas na televisão e o amor incondicional ao Racing, _la academia_; Roberto, o primo artista, companhia nos gostos musicais e filmes obscuros ou ouvir jazz em pé na cozinha com um mate entre eles, sem ver o tempo passar.

Por que desaparecem os adultos? As crianças se distraem, se confundem, confiam demais. Não aprenderam ainda a medir o tamanho do mundo. Jamais esperam que os pais lhes tirem os olhos de cima. Mirou outra vez a mulher das calças coloridas e quis imaginar a criança que havia sido. Aquele mundo — os artesanatos, as feiras e os turistas, o caminhar descalço sobre o asfalto — havia sido sempre o seu? Como as crianças que brincavam no gramado da praça, sujas, soltas, juntando pedaços de papelão e canudinhos usados; os filhos desse universo de roupas coloridas e alpargatas remendadas, mochilas e nomadismo.

Aquele mundo;

ou haveria um dia olhado em volta a cidade e o trânsito e a pressa de chegar a um novo emprego e percebido que não importava, que nada daquilo tinha sentido e mais lógico seria juntar um par de roupas, metê-las numa mochila e tomar o rumo do sul.

Por quê?

Seria talvez cretino perguntar-se por que — mas Téo não pôde evitar o eco. Era a pista que restava: a informação que poderia de fato ajudá-lo a encontrar aquele homem de sessenta e oito anos que mais de trinta anos antes havia abandonado a esposa e dois filhos pequenos e desaparecido, levando consigo uma maleta cheia de roupas, o rádio de pilha, dois pares de sapatos e o jornal do dia. Havia deixado sobre a mesa a aliança de casamento, um pacote de pão e, sob ele, um maço de dinheiro atado com um elástico: quase todo o salário que havia recebido no dia anterior.

Quase um mês que a investigação começada; Téo deixava-se arrastar pelos dias deslizando em direção ao sul por pura inércia; as certezas todas desfeitas tanto mais concretas lhe haviam chegado as informações. O tio se havia transformado numa ideia improvável: a figura difusa de um pai ausente, um ex-caminhoneiro que não sabe viver longe da estrada, todos os homens que alguma vez abandonaram um casamento por

(por quê?)

Perguntava-se de que lhe havia adiantado: o percurso que havia feito até ali atrás de migalhas de passado em busca talvez de uma coincidência que daria sentido às últimas semanas. O vento da Patagônia aos pés da cordilheira não permitia que se assentassem as pegadas: desapareciam os rastros todos e permanecia a paisagem, renovada e soberana.

Sentia-se exausto.

Mais alguns dias, uma semana. Era hora de desistir e voltar para casa.

## 1.

### São Paulo, SP

— Não!

Era mais reação do que negação: o hábito de dizer _não_ a tudo que lhe escapava do controle. Gustavo riu porque conhecia bem o homem sentado do outro lado da mesa: tinham alguns meses juntos, num relacionamento de indas e vindas e recomeços e incertezas. Sabia que Téo dizia _não_ por padrão, antes mesmo que compreendesse a pergunta.

— Por que não? — Gustavo esforçou-se para conter o riso. Tinha o corpo inclinado para frente, as mãos sobre a mesa, por hábito dosando cauteloso a distância entre eles. — Tenho umas semanas de folga e não conheço a Argentina.

— Não vai ser viagem de férias.

Também Téo não conhecia tanto assim da Argentina, apesar de ter morado em Avellaneda por quase quatro anos, na época do colégio, na casa da tia. Se voltava era sempre a Buenos Aires para visitar a família. Ainda assim parecia errado misturar investigação com passeio; a concentração necessária à busca do tio desaparecido e a distração que seria a presença de Gustavo ao seu lado com uma câmera fotográfica e sorrisos.

— É trabalho — Téo insistiu.

— Eu posso te ajudar.

— Ajudar como?

— Você não dirige.

Téo apoiou os cotovelos sobre a mesa, a testa nas mãos.

(Por que não?)

Era o que mais lhe custava: explicar-se, justificar-se. Dizer que a partir das sete estou em casa e estava no cinema e sim não avisei porque sim e porque quis. Era pedir demais: Gustavo percebia as implicâncias e mudava de estratégia, dava-lhe o espaço que queria, passava uma semana ocupado e súbito aparecia na agência com seu sorriso de advogado, seu sotaque carioca desterrado, terno e gravata e um convite para o almoço.

Entre seus silêncios e as ausências calculadas de Gustavo — podia chamar aquilo relacionamento? — era como se fugisse da definição por vício de profissão: detetive particular especializado em investigar cônjuges infiéis. Qual relacionamento pode durar sem que uma das partes estivesse vivendo algum tipo de vida dupla?

Às vezes sequer sabia se estavam juntos.

— Você nem gosta de dirigir — Téo disse.

— Eu não gosto de dirigir em São Paulo.

Fora da sala na recepção eram a secretária e Elisa conversando num murmúrio, quase por não incomodar a discussão que imaginavam atrás da porta fechada. Se Téo fosse menos reservado em relação a Gustavo, Elisa não precisaria tanto malabarismo para entender o que se passava na cabeça do amigo. Se nem Téo sabia chamar aquilo _relacionamento_, que ia dizer Elisa.

— Você lembra o que aconteceu a última vez que pedi sua ajuda com trabalho.

Gustavo esticou os braços por sobre a mesa até tocar as mãos de Téo.

— Já sou grandinho, Téo. Se digo que quero ir, e depois me arrependo, o problema é meu. Já te disse isso: não estamos amarrados.

E por que tanto o incômodo? Gustavo só fazia todo tempo demonstrar que o conhecia e o entendia e o aceitava e não estava ali para mudá-lo obrigá-lo a andar de mãos dadas na rua. Se também Gustavo não gostava de andar de mãos dadas na rua. Que mais ele precisava? Essa companhia insistente, paciente. Todo o espaço para se esconder num cinema no fim da tarde, porque estivesse passando e havia gostado do cartaz. Sem avisar ninguém.

— Você conheceu minha mãe — Gustavo disse.

Era quase uma acusação: Téo a tomou como quem leva golpe na cara. Recolheu as mãos para si; sentia o rosto quente e teve vontade de sumir. Gustavo o encarava insistente; a camisa branca desabotoada no pescoço e a gravata afrouxada — como sempre fazia no primeiro instante em que não lhe fosse exigida formalidade. Depois endireitou-se na cadeira, coçou o pescoço onde a gola da camisa lhe pegava. Olhou a desordem sobre a mesa de Téo com ímpetos talvez de empilhar num canto os papeis e as caixas plásticas de cd. Não era surpresa que Téo não encontrava os óculos e dizia que por isso não havia respondido qualquer mensagem no telefone.

Téo abaixou a cabeça, atordoado. Quão terrível podia ser uma viagem daquelas, um ajuda ao primo, uma visita breve aos pais. Ficaria dois ou três dias em Buenos Aires e seguiria aonde quer que estivesse a pista mais conveniente. Era o tempo de recolher com Roberto as informações últimas que faltassem, se é que haveria ainda algo mais a se saber, de trinta anos que o pai estava desaparecido e Téo nem se lembrava dele: Antonio, esse tio ausente, quase inexistente — havia visto aquele homem, alguma vez?

Que ele sabia senão que havia sumido?; que a tia havia recusado ajuda financeira da irmã, mãe de Téo — e por isso seus pais inventaram de mandá-lo à Argentina terminar os estudos, que assim mandavam dinheiro para sustentá-lo e um pouco mais, porque sim. Ninguém falava do tio naquela casa: para a tia era como se sequer houvesse existido. O primo Roberto tão mais criança parecia sempre mais preocupado com outros temas; Pablo, o primo mais velho, ocupava-se de criar confusão, gritar com a mãe, brigar com os vizinhos.

Olhou Gustavo do outro lado da mesa: o meio sorriso que lhe era típico quando sabia que tinha razão mas se segurava para não dizer nada. Os olhos dum castanho tão escuro que não deixavam ver as pupilas. Os dedos entrelaçados na frente do corpo e a forma mui vagarosa com que movia os polegares, em gesto distraído.

Téo pensava em talvez tomar a viagem como respiro para se afastar da sensação de que estava atado em infalível desastre: que estivesse já demasiado acostumado à companhia de Gustavo os sorrisos de Gustavo as mãos de Gustavo sobre as suas a gravata frouxa no pescoço os óculos escuros no bolso da camisa e tão pior a queda quanto mais alto;

a queda que sempre vem.

Então Gustavo se oferecia de motorista e companhia e _não conheço a Argentina_. Que a ele fosse só isso: uma chance de aproveitar as semanas de folga que havia acumulado, fugir um pouco das esquisitices de São Paulo, sentir o vento na estrada.

Téo não percebeu quando deixou escapar um sorriso meio aborrecido, e talvez aí Gustavo houvesse percebido o desnecessário da agressividade. Respirou fundo, pediu desculpas e levantou-se, disse que precisava voltar ao trabalho e de qualquer forma tinha coisas a resolver. Inclinou-se por sobre a mesa para beijar a cabeça de Téo antes de se afastar.

Téo pensaria que _yo siempre lo supe_ era — seria? — a mais óbvia das reações e

por que pensava que escondia algo de;

e se tinha mais de quarenta anos. Se seu pai — esse que tão presente ainda em sua distração de homem bronco da roça — havia contrariado o conservadorismo da família de cidade pequena ao se casar com argentina metida a comunista fugida da ditadura militar e vá saber como ela tinha ido parar no interior paulista, naquela cidadezinha tão fora do caminho — qualquer caminho. Um pouco mais de crédito àqueles dois rebeldes que eram seus pais, que sessenta anos de idade haviam saído daquele fim de mundo e se metido em Buenos Aires para abrir uma agência de viagem e recomeçar a vida; sim?

Gustavo já tinha a mão na porta — Téo quis dizer que _espera_ e _a coisa é um pouco mais complicada do que_. Mas Gustavo virou-se e sorriu:

— Não precisa se preocupar. Te ligo mais tarde.

## 15.

### Buenos Aires, BsAs

Téo jogou-se no banco do passageiro e fechou a porta de uma só vez; soltou o ar com força, num suspiro exagerado. Tinha nas mãos dois livros que a mãe lhe insistira que levasse, porque

enfim

livros;

sua mãe nunca havia precisado de desculpas ou justificativas para livros.

Gustavo o encarava. A camisa de manga longa dobrada até antes do cotovelo e os óculos escuros por sobre a cabeça; tão inevitável turista brasileiro.

— E?

— Vamos; — Téo disse — se a gente demorar mais tempo pra sair vamos acabar pegando trânsito demais.

Talvez não fosse essa a pergunta; a resposta errada. Gustavo resignado conferiu o aparelho GPS afixado no painel e apertou alguns botões. Uma voz feminina os cumprimentou em espanhol e indicou a direção que deviam tomar.

— Se ela falar alguma coisa esquisita você vai ter que me traduzir.

— _Izquierda, derecha_, não pode ser tão difícil.

Era para ser brincadeira. Soou mais agressivo do que teria sido a intenção, e Gustavo fez só erguer as sobrancelhas, como se registrando consigo a súbita mudança de humor de Téo. Os poucos dias que estavam na cidade haviam sido o tempo todo medir palavras e testar reações — mais ainda que o normal. Quase se acostumava; se acostumaria, talvez, não sentisse ele também a paciência oscilando a cada dia passado. Diria a si mesmo que estava tudo bem, que a inquietação era temporária e a estrada dissolveria o incômodo.

Estava tudo bem.

Manobrou de volta para a rua, devagar. O carro alugado era pequeno mas confortável o suficiente para viagem longa. Rumo a Córdoba.

— Ruta nueve — Gustavo murmurou, em inevitável sotaque aportuguesado; tentativa de criar assunto quando já cruzavam Palermo e faltava ainda caminho para alcançar a borda da cidade. — Qual é aquela famosa?

— Aquela o quê?

— A rodovia.

— Quarenta. _Ruta cuarenta_.

Téo descruzou os braços. Havia percebido o despropositado de fechar a cara daquele jeito, porque.

Porque.

— E você não conhece mesmo nada do país?

— Conheço um pouco de Córdoba.

— Só isso?

Téo tornou a pegar um dos livros que havia deixado aos pés do assento. Mario Benedetti; era uruguaio, a mãe lhe havia dito. Um livro de contos. O outro era Juan Jose Saer, _La pesquisa_. Pelo título não sabia se ficava irritado ou curioso: que passava na cabeça da mãe? De que lhe ia servir a literatura quando a realidade era imensa e o país era imenso e bem fosse verdade seu tio poderia estar ali mesmo em Buenos Aires todo aquele tempo, anônimo.

— O dinheiro que eu tinha quando era adolescente eu gastava com ingresso de jogo de futebol, camisa de time, sorvete — Téo disse, enfim. — Não tinha para viajar.

— Acho que quis dizer depois de adulto. Você vêm pelo menos uma vez por ano.

Téo pensou que continuava gastando boa parte do dinheiro das viagens com jogo de futebol e sorvete. Mas — claro — não era de dinheiro que Gustavo estava falando. Seria talvez explicar que lhe bastava: a capital, o tempo com os primos e as conversas com a mãe, os hobbies inesperados do pai.

— Córdoba capital?

Téo afirmou com um gesto de cabeça, distraído. O que conhecia da região era de um caso mal resolvido: investigação a pedido do primo Pablo; uma história mal contada que o fez se meter onde não devia. Sobrevivera por pouco. Os detalhes não havia contado a Gustavo, ou Elisa, ou mesmo ao primo.

Era história repetida? Investigar o que não devia, meter-se pela estrada rumo ao norte e buscar gente desaparecida.

(Como desaparecem os adultos?)

Investigar suspeitas de infidelidade era mais simples: seguir, observar, registrar. Havia mandado a Elisa as informações que tinha sobre o tio; que ela as transmitisse a Alexandre e os amigos jornalistas que saberiam ajudar com pesquisas internet adentro.

Não era assim que se encontravam as pessoas?

Um homem de mais de sessenta anos?

— Tem uma cidadezinha legal no caminho, antes da sua primeira parada. A gente podia ficar uma noite, dar uma volta. Dizem que por ali passam muitos discos voadores.

Téo riu e Gustavo, aliviado, fez o mesmo.

— Você acha que meu tio foi abduzido?

— Nunca se sabe!

O sorriso fez-se mais fraco, quase desaparecido. Fosse algo assim, absurdo. Ou porque mesmo essa primeira parte da viagem parecia impossível, interminável. Téo queria fechar os olhos e manter-se calado, perder-se em pensamentos ou análises mentais sobre influência artística num ou noutro músico do jazz. Não a obrigação de companhia e criar conversa e manter conversa. Mais ainda porque o assunto — os dias desde que haviam chegado a Buenos Aires, o jantar com os pais de Téo, as conversas pela metade — pairava pesado, ocupando o espaço vazio dentro do carro.

Discutir alienígenas não os salvaria de nove horas de estrada.

— A gente não precisa conversar sobre o que você não quer conversar, Téo — Gustavo também a expressão séria. Apontar o elefante monstruoso em que haviam se transformado todos os não-ditos. Ou: que sejamos adultos, e que haja diálogo

ou não.

— Eu não disse isso.

Mas era uma espécie de brecha por onde se meter e insistir

(eu não disse isso)

porque então havia espaço e sim, era necessário conversar sobre o que Téo não queria conversar.

— Que foi que tua mãe disse?

Téo sentiu que lhe tomava um mal-estar.

— Ela não disse nada.

— Por isso você voltou mal humorado e se jogou no carro como quem escapou da forca.

— Minha mãe fala muito.

Gustavo deixou escapar um riso nervoso. Parou num sinal vermelho e virou o corpo para o lado. Téo tinha os olhos ainda no livro, como se buscasse na capa colorida uma rota de fuga. Era possível que se esgotaria enfim a paciência infinita de Gustavo — ali no trânsito da manhã portenha, poucos dias passados desde que haviam chegado. Já Gustavo havia aprendido a se comunicar com Roberto e andar sozinho pela cidade sem se atropelar demais nas palavras. Compreendia um pouco do espanhol apressado da gente local e sabia pedir que _más despacio, por favor_ com seu sorriso enorme de brasileiro.

Pensaria — o sinal verde, acelerar, voltar a prestar atenção adiante — frustrado;

pensaria: de que adiantava saber mais de um idioma se em nenhum deles Téo era capaz de se comunicar, fazer-se entender.

— Ela não falou nada — Téo disse, enfim. — Ela não falou nada, meu pai não falou nada. Que te parece? Que você acha que eles iam dizer?

— Não é disso que estou falando.

— Então é o quê?

— O jeito que você voltou para o carro. Você disse que sua mãe só queria que você buscasse um livro. Se fosse só isso, você não estaria tão irritado.

— Se a situação fosse simples a gente nem precisaria ter esse tipo de conversa.

Era bem provável o mais sensato que Téo havia conseguido articular. Gustavo respirou fundo: era melhor estar atento ao caminho e às instruções da voz eletrônica soando em língua estrangeira pelo aparelho GPS.

— Eu conversei com sua mãe, Téo.

— Como assim?

— Não, não — Gustavo quis rir, mas conteve-se. — Quero dizer, a gente esteve com ela; não entendo de que você tem medo. Tua mãe tem a cabeça aberta. Mais do que a minha mãe. E teu pai também.

— Não é isso. Você não entendeu.

Gustavo apertou com força o volante. A viagem mal começada. Que ele mesmo houvesse se disposto a dirigir e fazer-se assistente de detetive quando poderia ter seguido ao Rio de Janeiro visitar a própria família e rever os amigos. Que sabia desde o começo não estavam ali para discutir a relação e muito provável só por isso Téo havia topado que o acompanhasse.

Engolisse a frustração, mudasse o tema. Ligou o rádio e perguntou a Téo se ele indicava alguma estação. Pareceu então que escapava dali uma nuvem pesada. Téo assumiria o posto de curador musical, como lhe convinha: súbito mais animado buscou na mochila o aparelho de MP3 para conectá-lo ao sistema do carro e enfim levar o assunto aonde lhe interessava, e perder-se em divagações sobre estilos e métodos e influências artísticas.

## 2.

### São Paulo, SP

O silêncio da agência era o ruído dos carros na rua, que ali do oitavo andar parecia quase o murmúrio do mar, incansável. Téo meteu na mochila a câmera fotográfica e os acessórios todos: bateria, carregador, um emaranhado de cabos. Uma caderneta de anotações pequena, lápis, o aparelho de mp3 que ainda não sabia usar. Sentiu-se um pouco atrasado — retrasado. Uns vinte anos se escondendo do mundo e agarrando-se a um ideal de atemporalidade: a música em cds era-lhe ainda excesso de modernidade. Se nem mesmo alcançava os cinquenta anos.

Sabia o que Gustavo ia dizer: o absurdo porque, sim, tinha mais de quarenta anos e os pais não sabiam que era gay, que Elisa era sócia e melhor amiga mas não era e jamais seria sua namorada ou amante secreta.

Ou: sabiam, como poderiam não saber?

E se sabiam — todo esse fingimento?

Porque moravam em outro país? Téo havia saído de casa aos catorze anos para fazer o colégio em Buenos Aires, na casa da tia irmã gêmea de sua mãe. Não voltaria mais: meteu-se depois na capital paulista, inventou de estudar direito — a carreira desistida antes mesmo que terminasse a faculdade e o diploma que jamais lhe serviria para muito além de compreender as piadinhas jurídicas que Gustavo vez ou outra deixava escapar.

Gustavo tinha a mãe como confidente e um pai mais ou menos distante que encontrava em eventuais festas de família no interior do Rio de Janeiro. Nenhum segredo, nenhum não-dito, nenhuma surpresa.

Era mesmo necessário?

Explicar-se —

chamar de _namorado_ e apresentar à família.

— Que horas é seu voo amanhã?

Elisa havia parado com o corpo no batente da porta. Téo abaixou o volume da música.

— Três da tarde.

— Posso sair da agência meio dia e te buscar em casa, que te parece?

— Não precisa. Eu pego um táxi.

— Gustavo vai também?

Aproximou-se para ocupar uma das cadeiras em frente à mesa. Téo fechou o zíper da mochila e jogou o corpo para trás. Pela janela fugia a tarde, naquela pressa de outubro pelos dias de verão futuro.

— É trabalho, Elisa.

— Pois bote um mapa de Buenos Aires na mão dele e vá atrás do seu tio enquanto ele conhece San Telmo.

Téo sentia-se à vontade com Elisa porque ela não exigia as explicações que ele imaginava os outros queriam, sempre. Entendia quando Téo mudava de assunto e permitia que crescesse entre eles o silêncio quando no fim do dia sentavam juntos no escritório de um deles e distraíam-se com a vista da janela e os ruídos da cidade. Compreendiam-se no silêncio, como convinha.

— Ele vai — disse.

E saberia fazer-se tonto, o tanto que Téo lhe pedisse: fingir-se amigo e falar de mulher, como se bastasse. Téo sabia que Gustavo tinha razão no ridículo do fingimento e afinal tinha mais de quarenta anos já ia o tempo de deixar de lado as inseguranças e paixões platônicas que lhe haviam estragado a adolescência e o começo da vida adulta. Mas era também constatar o complicado que se tornavam os relacionamentos que duravam mais de duas ou três semanas; como ia dizer que _esse é Gustavo, meu namorado_ se nem mesmo acreditava que.

Pensar no tio que

fugiu de casa?

e que desespero era esse: uma vida tão errada que era preciso abandonar tudo sem olhar para trás, desaparecer. Mais fácil seria concordar com Pablo e _mi padre ese es un hijo de puta que me importa que le pasó_. Quanto desespero para fugir assim das responsabilidades, dos filhos pequenos, da esposa e da casa e

nunca mais.

— Não sei. Tenho a sensação de que.

— _I have a bad feeling about this_, — Elisa interrompeu, engrossando um pouco a voz.

— _That's no moon, that's a space station_.

Sorriso esboçado. Antes fosse Elisa insistindo para acompanhá-lo na investigação. Mas havia a agência e o trabalho _de verdade_; era afinal necessário pagar as contas e o aluguel do escritório em oitavo andar num edifício da Bela Vista. À Elisa as questões administrativas e lidar com os clientes — a função mais difícil de se delegar. Para as diligências sempre haveria jornalista ou policial procurando bico.

— E por aqui você fica bem?

Ela riu.

— Alexandre me ajuda com a parte remota. E você sabe que Vitória está doida por um pouco de trabalho de campo.

Mero protocolo: tudo que Téo já sabia. Era hábito: desviar o tema e discutir trabalho quando a vida parecia demasiado complicada. Antes também investigar casal desconhecido e destruir matrimônios à distância com uma ou outra fotografia suspeita. Escrever relatórios, guardar a informação num envelope e adiante ao próximo caso. O glamour da profissão não existia nem mesmo no cinema, não mais — e para Téo e Elisa estava muito bom.

— Vocês estão juntos já tem alguns meses, Téo.

Téo gesticulou com a cabeça para concordar.

— Não é como se ele estivesse te apontando uma arma na cabeça — ela disse.

— Eu sei. Eu gosto dele.

— Então?

Téo abriu a boca para responder, mas só fez transformar a expressão numa careta e deixar escapar um _ah_ resmungado. Dizer que _não nasci para isso_ embora nem soubesse que podia ser _isso_. Relacionamentos, intimidade, companhia silenciosa para o café da manhã; Gustavo no espelho do banheiro abotoando a camisa muito devagar feito guardasse nas mãos todo o tempo do mundo e depois um sair apressado porque vou chegar atrasado o trânsito nesta cidade é uma merda. Aquele _erre_ arrastado do sotaque carioca que Téo seguia escutando na cabeça por todos os lados, na cidade inteira.

Meter-se contra a corrente numa sociedade que exigia de quem não se adequasse uma declaração assinada da diferença: caminhar a todo tempo com bandeira hasteada. Tão cansativa essa constante afirmação reafirmação de identidade; bater o pé por exigir a monotonia de

(uma vida normal?)

Que tanto lhes importava se era gay ou canhoto ou.

— Podia ser suficiente.

— É suficiente, Téo.

Téo fez outra careta. Olhou sobre a mesa os papéis e envelopes espalhados e deu de juntá-los numa pilha; encontrou os óculos de leitura e os guardou na caixa, enfiou na lateral da mochila.

— Obviamente não é; tem que apresentar pra mãe.

Elisa riu. Fez que ia responder, mas Téo continuou:

— Não vai durar.

— Isso você diz quase todo dia.

Que mais Elisa ia dizer? Também ela não se entendia com os relacionamentos; mais fácil desmontá-los com meia dúzia de fotografias. Isso sabiam fazer. Quisesse talvez concordar

(não vai durar)

porque tão mais fácil o inevitável pessimismo do ofício e concluir que nenhum relacionamento é possível.

— Meio dia está bom — Téo disse. — Vou preparar uns sanduíches e fazemos um piquenique no aeroporto.

## 16.

### Capilla del Monte, Córdoba

Havia adormecido passado o lago antes de Cosquín, primeiro _pueblo de ruta_ do Valle de Punilla, menos de uma hora depois da cidade de Córdoba. Talvez porque não tivesse a melhor das lembranças daquela região, única que havia conhecido para além de Buenos Aires. Haviam almoçado em Córdoba e seguia um pouco arrastado aquele segundo dia de viagem, depois de pernoite num povoado minúsculo brotado aos pés da _Ruta 9_.

Gustavo o chacoalhou de leve; o carro estava estacionado em uma rua estreita, antes de uma intersecção. O sol flutuava a oeste, atrás deles, fazendo que se esticasse adiante a sombra do carro. Na esquina em frente havia uma placa turística mostrando o que parecia ser o mapa do povoado: Capilla del Monte. Téo ajeitou-se no banco, apertou os olhos e arrumou o cabelo.

— Chegamos — Gustavo disse.

— Cruz del Eje?

— Capilla del Monte — e apontou a placa.

Passado o cruzamento, ainda, a rua transformava-se em atração turística: parte dela seguia sob uma estrutura coberta, cercada por árvores baixas com as folhas novas num verde vivo de primavera; o nome do lugar repetia-se também ali, como na placa, para o caso — talvez — de alguém esquecer-se onde estava.

— Falei que a gente podia parar aqui por um dia — Gustavo continuou. Havia desligado o GPS e virou-se para buscar a mochila atrás do banco. — Não custa descansar um pouco da viagem antes de começar a investigação.

Quisesse completar: _longe da sua família_.

Téo esfregou o rosto mais uma vez. Preferia que seguissem direto e alcançariam Cruz del Eje no fim da tarde. Ali estaria o primeiro pedaço de investigação que lhe cabia: um endereço, uma amizade antiga.

Não porque tivesse pressa. Trinta anos: um dia a mais ou a menos não deveria fazer diferença. Gustavo era quem tinha contados os dias de folga e precisava medir o tempo para estar de volta. Era mais porque a pausa criava espaços e os espaços gritavam por preenchimento. Conversar, fazer assunto — para quem estava acostumado à solidão, a constância da companhia em viagem a dois pesava

tanto quanto havia imaginado que pesaria.

(Não é como se ele estivesse te apontando uma arma na cabeça.)

— Que horas são?

— Três e algo. Três e vinte? A gente podia tomar um sorvete.

Téo riu consigo com a ideia de tomar sorvete sob a mira de uma arma; as obrigações. Concordou. Soltou enfim o cinto de segurança e abriu a porta. Saiu do carro num pulo. Deixou que lhe tomasse o ar da tarde, os cheiros desconhecidos. Da intersecção se viam as ruas todas vazias, silenciosas. Pouco provável que encontrassem sorveteria aberta — ou um mercadinho, um quiosco que fosse.

Gustavo ajeitou a carteira no bolso e os óculos escuros no topo da cabeça. Gesticulou na direção da rua coberta adiante. Téo percebeu no gesto a hesitação: um tremor nos dedos ou o pulso que virava de leve em contração involuntária — tão acostumado a observar o movimento preciso das mãos de Gustavo era fácil notar o desvio.

— Aqui estão os alienígenas, então.

— Cerro Uritorco — Gustavo arriscou, atrapalhado na pronúncia dos erres.

— _Cerro_ — Téo corrigiu, com um sorriso, cutucando Gustavo de leve no braço. — Quê? Eles moram na montanha?

— Acho que a montanha atrai os discos voadores.

— Humano mesmo não estou vendo nenhum.

Pararam depois de atravessar o cruzamento. Um cachorro dormia sob a sombra de um banco. Os negócios ao longo da parte coberta da rua estavam fechados. Plena tarde estariam todos recolhidos para a _siesta_. Se soubessem onde procurar encontrariam talvez uma hospedagem aberta — e o dono provavelmente dormido na recepção.

Seguiram pela rua, no passo lento dos turistas, imaginando o movimento que haveria mais tarde. A área coberta tomava pouco menos de um quarteirão e também os negócios rareavam depois dela.

— Plano B? — Téo perguntou.

Continha-se para não decretar que seguissem viagem e deixassem de lado as pausas desnecessárias, os sorvetes desnecessários;

as conversas desnecessárias.

— A gente pode olhar o mapa — e apontou outra placa idêntica à que havia próxima ao lugar em que estacionaram. — Têm alguns rios, umas trilhas. Preciso caminhar um pouco, estou cansado de dirigir.

— Algum lugar em que a gente chegue a pé?

Gustavo distraiu-se olhando o mapa. Seguiu com o dedo as informações escritas na lateral e Téo viu que mais uma vez o movimento se fazia firme e seguro, como se inabalável.

— Ou matamos tempo por aqui — Gustavo concluiu — esperamos as coisas abrirem. Cinco horas deve abrir, não é isso?

— Provavelmente.

— Não falta tanto tempo assim. Você gosta de caminhar.

Havia pouco vento e a temperatura estava agradável. Também Téo gostava de ver as árvores que insistentes brotavam primavera adentro de troncos podados durante o inverno: o verde brilhante das folhas novas. Concordou. Gustavo aproximou-se e passou o braço por sobre seus ombros.

— Topa uma trilha amanhã de manhã? Depois do almoço a gente segue viagem.

A cabeça dizia _não_ mas era mais fácil aceitar e fazer que sim, num gesto breve

talvez incerto;

desacostumado com que alguém lhe tomasse as rédeas do dia e decidisse por ele o caminho a tomar.

Tomaram a esquerda e Gustavo criou espaço entre eles, encolhendo o braço e metendo as mãos nos bolsos da calça. Que o tempo entre eles fosse sempre preenchido de pequenas frustrações e o que poderia ter sido

gestos incompletos

(a rua vazia)

um pouco como se todo instante significasse um recomeço

(mais uma vez)

renovadas as incertezas e inseguranças de princípio de relacionamento.

## 3.

### São Paulo, SP

Claro que era ridículo: o tapinha na mão meio tonto, meio demorado, quase sem querer; feito fossem adolescentes que não podiam revelar uma paixão proibida. Era ridículo porque sim, era homem de quarenta e quatro anos e Gustavo sabia: Gustavo agia daquela forma de propósito, em esforço para se adequar ao que quer que fosse o ideal de relacionamento de Téo. Aquele relacionamento inexistente que até a comissária de bordo adivinhava, como se não houvesse outra possibilidade na intimidade que se mostrava nas entrelinhas dos movimentos.

— Odeio avião — Gustavo disse, com uma sacudida de ombros.

— Pudesse você ficava pra sempre no mesmo lugar.

Gustavo riu. Hesitou em responder, mas acabou afirmando com a cabeça.

— Nem teria saído do Rio — concordou.

— Você nunca viajou para fora?

— Do país? Ah, sim.

— Com Fabiano.

Aquele sorriso. Téo desviou o olhar para prestar atenção na gente que circulava pelo corredor procurando o assento marcado na passagem, pedindo licença e se espremendo por entre os outros. Téo que já tanto havia se mudado — de cidade, de país, de vida — preferia também o endereço fixo e a rotina num só espaço. A avenida Paulista e o centro de São Paulo. Talvez porque muito tempo havia sido estrangeiro e estava cansado de não pertencer. Ou porque jamais seria capaz de pertencer: brasileiro filho de mãe argentina torcedor do Racing Club de Avellaneda _que pensa que Maradona é melhor do que Pelé_ — como teria dito um amigo jornalista, indignado.

Tinham isso em comum, ele e Gustavo: querer fincar os pés num só lugar e não sair mais. Ambos longe da cidade natal inventando de transformar São Paulo em lar doce lar.

— Senhoras e senhores aqui fala o comandante.

Os comissários ainda se moviam de ponta a outra do corredor com os últimos preparativos e as instruções de segurança. A conversa tinha acabado ali, minutos antes, com o nome do ex-namorado de Gustavo atirado no vazio. Não que fosse tema proibido; era mais Téo quem evitava o assunto porque sabia que Gustavo não se importava.

Gustavo tinha as mãos sobre as pernas e a vista perdida adiante embora inevitável o contato físico porque ambos se apoiassem no braço das poltronas e era preciso preencher o espaço com palavras ou

_bem-vindos a bordo do voo 4589_

_o tempo em Buenos Aires_

_temperatura atual de 23 graus_

_tempo estimado de voo_

_senhoras e senhores_

e tirar do bolso a embalagem de chiclete comprada no aeroporto oferecer a Gustavo _porque a pressão_ etc. Gustavo aceitou com um sorriso, outro sorriso. Gustavo; sempre;

_decolagem autorizada_

sorrisos.

Gustavo fechou os olhos enquanto o avião ganhava velocidade, encolhido como convinha ao espaço do assento. Era mais alto que Téo — passava fácil de metro e noventa —, um tanto mais forte; bicho que cresceu na praia e acostumou-se a passar o dia exibindo-se sem camisa sob o sol. Concentrado no pedaço de chiclete que tinha na boca enquanto as rodas se desgarravam da pista.

Téo ouviu Gustavo respirar fundo e quis lhe tomar a mão

mas.

Você sabe que.

Você sabe que meus pais.

Meus pais.

Porque minha mãe.

E você não está um pouco grandinho para esse tipo de discurso? As palavras dando voltas em sua cabeça enquanto o avião sacudia-se com o vento e ganhava altitude.

Claro que Gustavo sabia.

— Qual vai ser a história, Téo?

Gustavo virou a cabeça para o lado, amassando os cachos desordenados que lhe escapavam atrás da orelha. Assim sentados os olhos se encontravam na mesma altura. Téo quis rir. Sentiu foi os pelos do braço arrepiarem-se, um frio na barriga;

a pressão e o incômodo nos ouvidos.

— Roberto é o único que sabe.

— Seu primo mais novo.

— Sim.

— E você vai contar?

Quis dizer que sim, claro; ou

que isso não interessava a ninguém e não fazia diferença.

Abriu a boca; ia falar, deteve-se. Houvesse se arrastado até ali com respostas evasivas; aos pais, ao primo mais velho

a Gustavo.

As brincadeiras tão inofensivas de moleque: pensava nos amigos de colégio e nos amigos de Pablo com quem vez ou outra havia ido ao estádio, depois de adulto, quando havia voltado para visitar. Pensou que sim: estava velho demais para aquele tipo de discurso, aquele tipo de insegurança. Ninguém se importava.

(Não fazia diferença.)

Pensou em Fabiano e a história trágica que Gustavo obstinado carregava nas costas como se não doesse

como se não pesasse o passado;

as brincadeiras de moleque

tão inofensivas.

— Tenho certeza que tua mãe é muito mais esperta do que você pensa, Téo.

Claro que era. Quem não saberia? Seu pai toda vez perguntava quando ele ia tomar vergonha na cara e pedir Elisa em casamento. _Vocês se dão tão bem_. Juntos assistindo a _Casablanca_ pela vigésima vez repetindo em voz alta os diálogos decorados.

— Eu sei o nome de sua mãe?

— Julieta.

— Julieta — Gustavo repetiu, imitando a pronúncia em espanhol. — E teu pai?

— Joaquim.

— Julieta, Joaquim, Juan Teodoro — riu.

Téo moveu-se no assento, afrouxou o cinto de segurança. Gustavo lhe havia contado o quanto antes queria ter seguido carreira no teatro em vez de se formar em direito. Tão fácil saberia se encaixar no papel que lhe fosse atribuído. Era o que fazia no trabalho: entrava sem esforço na conversa com os colegas quando davam de discutir mulher e carro e essas coisas que — ele reclamava — homem hétero tanto gosta.

— Você acha mesmo que Roberto não contou para teu outro primo, para tua tia?

— Eu conheço meu primo.

— E eles conhecem você.

Era; isso: teatro.

(Podia ser suficiente.)

Enfim o avião estabilizava-se depois de uma curva, e pela janela era a represa Guarapiranga, enorme. A cidade de São Paulo feia e cinzenta um emaranhado de blocos de concreto. Fazia sentido aquilo?, insistir a homem adulto que não escondesse relacionamento.

— A vida é tua, cara — Gustavo moveu o maxilar como que para aliviar a dor da mudança de pressão.

Voltou a encarar adiante. Procurou no bolso da poltrona da frente algo com o que se distrair, desistiu e fechou os olhos. A viagem seria curta o suficiente para um cochilo e longa demais para que seguisse insistindo naquela conversa. Téo gostava de observá-lo — mais do que seria capaz de admitir. O cuidado com que se vestia ou a tranquilidade com que descalçava os sapatos. A forma mui ordenada de tirar a carteira do bolso e pousá-la alinhada sobre a mesa ao lado dos óculos escuros e das chaves do carro. A linha dos cabelos fazendo curva atrás da orelha. Convinha que Gustavo não o encarasse;

ou porque Gustavo soubesse e satisfeito fechava os olhos aceitando calado o carinho imaginado.

— Mas eu preciso saber a história que você quer contar se for para participar do mesmo jogo — completou.

## 29.

### El Bolsón, Rio Negro

A Patagônia era principalmente o vento. Fazia lembrar Córdoba e o vento norte, abafado, que _enlouquecia as mulheres_ e perturbava todo ser movente. Mas ali era só um vento frio que gastava as coisas e as pessoas e levantava uma poeira cinzenta de terra preta. Que volta absurda havia feito, de Tucumán a Catamarca de Catamarca ao sul

cada vez mais ao sul.

Pensar que terminaria em Ushuaia, _fin del mundo_, e nem mesmo ali estaria o tio, desaparecido para além do espaço ou perdido pelo caminho numa curva ao lado errado. Cada passo adiante era também a chance de que Antonio houvesse ficado atrás, cada vez mais distante. Téo não se lembrava de uma Argentina tão grande e impossível. Lembrava-se de um percurso entre Avellaneda e o porto, do centro de Buenos Aires ao estádio do Racing, a linha do trem por onde corriam para se esconder dos torcedores furiosos do time adversário.

A moça das calças coloridas e agasalho de lhamas organizava sua mesa com o fim da feira; duas crianças brincavam por perto. Provocavam-se: a mais velha perturbava a menorzinha, e vez ou outra a artesã lhes interrompia com a expressão séria.

Téo observava, distante, ainda a garrafa vazia de cerveja artesanal ao seu lado, um pote de plástico com um resto de framboesas.

Queria falar com ela — entender essa energia para montar desmontar a mesa de artesanato todo domingo e quem sabe expor em outro canto no resto da semana, caminhar pela rua identificando turistas e sorrisos e se nada parecesse funcionar quem sabe mudar de cidade, pedir carona na pista e descer onde fosse conveniente. Fosse o que lhe restasse: colocar-se no lugar do tio e deixar que o destino se construísse nas incertezas.

— _Y vos? Te dejaron plantado?_

Um momento de distração e a artesã estava já alguns metros em sua frente; mochila nas costas, a tábua da mesa debaixo do braço e os cavaletes que faziam as pernas da mesa pendurados no ombro. As crianças — um menino e uma menina — haviam corrido adiante. Téo levantou-se, meio atrapalhado tomando do chão a garrafa e a embalagem de framboesas.

— _No, no._ — E conferiu as horas. — _Te ayudo?_

Ela pareceu considerar a oferta; que o medisse e repensasse o peso nas costas, o difícil de carregar aquele tanto de pedaço de madeira. Sorriu, aceitou com um gesto e se aproximou. Entregou-lhe as partes da mesa e tomou-lhe das mãos a garrafa vazia e as framboesas.

— _Justo_ — Téo disse. — _A ver si los niños no quieren unas frambuesas?_

— _A ver._

E voltou-se para onde estavam as crianças; gritou-lhes os nomes e ergueu o pote de plástico quando pararam para prestar atenção. A mais velha pareceu discutir a questão com o irmão pequeno, talvez para decidir se aceitariam a presença inesperada no caminho de volta. Terminada a conferência seguiram de volta na direção dos adultos. A menina gesticulou para que o irmão pegasse a embalagem, e logo voltaram correndo na mesma direção de antes.

Téo gesticulou adiante para que a artesã indicasse o caminho.

— _Soy Juan_ — disse, e sentiu daquela vez que lhe pesava o primeiro nome, feito estivesse falando de outra pessoa.

— _Violeta._

— _Vivís acá?_

— _Ahora, sí._

— _Hace tiempo?_

— _Un año, creo. Un año y unos meses._

Cruzaram a praça até a avenida. As crianças esperavam na esquina; o pote de framboesas vazio. Logo tornaram a correr, depois de um momento breve de cuidado para atravessar a rua. Aproximavam-se as sete da noite e o sol seguia firme no horizonte. Um vento gelado havia tomado as ruas, levantando a poeira cinza e obrigando os mais friorentos a buscar os caminhos que ainda não haviam sido tomados pelas sombras do fim do dia.

O povoado era todo plano, esticado no espaço como se também ele houvesse sido trazido pelo vento. As calçadas de terra se confundiam com as calçadas de cimento e com o asfalto da pista. A artesã tinha a pele e os cabelos quase da mesma cor; uns olhos cinzentos que eram também da mesma cor de todo o resto do lugar.

— _Y vos? Vacacciones?_

— _No. Trabajo._

— _En serio? Qué hacés?_

Parecia demasiado complexo explicar o que fazia; que era investigador particular e que buscava um tio desaparecido trinta anos antes. Que na verdade não sabia mais o que estava fazendo, do outro lado do país, quando mais lógico teria sido encontrar o tio em outro canto, em outro clima, em outros ventos. Seguia por inércia: a _Ruta 40_ o chamava e ele se permitia deslizar por mais um instante.

Só mais um instante.

Fizeram atalho pelo calçamento de pedregulho de um estacionamento e dobraram à esquerda numa rua estreita. As crianças continuavam adiantadas, já quase na esquina seguinte. Téo não tinha tempo suficiente por ali para saber exatamente a que lado iam; sabia que o povoado se esticava de norte a sul e não havia muito como se perder. Que encontrasse depois a avenida San Martín — era sempre uma avenida San Martín — e saberia tomar o rumo da hospedagem.

Explicaria: o pedido de um primo, o tio desaparecido décadas antes; o quanto não entendia de encontrar gente desaparecida e mais ainda desaparecida há tanto tempo. O quanto naquele momento lhe parecia que apenas seguia adiante, de olhos fechados, tateando a poeira ancestral que parecia ter criado todas as cidades e povoados Patagônia adentro.

Ela o ouviu atenta, ainda que os olhos nas crianças.

— _El viento trae todo tipo de cosa hacia acá, sabés_ — ela disse, com um sorriso enigmático, pensando talvez em seus próprios motivos para estar ali, na divisa entre Rio Negro e Chubut, aos pés dos Andes.

Tomariam a direita em uma rua de terra ainda mais estreita, ladeada de casas miúdas com pinta de chalé. As crianças haviam parado próximas a um portão baixo de madeira, e com um salto desapareceram atrás dele. Via-se uma área grande de jardim, algumas barracas; ao fundo uma casa grande com as paredes externas pintadas de amarelo. Próximo ao portão havia uma placa de madeira com letras pintadas em branco: “camping”.

A rua canalizava uma brisa fria e as árvores impediam que os alcançasse o sol. Seguiram até o portão e Violeta estendeu as mãos para tomar de Téo as partes de sua mesa.

— _Te quedás más tiempo en el pueblo?_

Téo deu de ombros primeiro; hesitou. Não sabia mais a que lado seguir. Ou ia adiante pela RN40, porque

por que não?

Pensou que devia voltar ao quarto e juntar notas e pedaços de informação, mais uma vez; como se pudesse encontrar uma resposta escondida, uma solução inesperada. Que o tio devesse estar ali naquele emaranhado de pessoas e lugares e lembranças. A investigação toda apontava naquela direção: ao sul, sempre ao sul, cada vez mais ao sul. Que diferença fazia?

O país era todo sul.

As mensagens com o primo ficavam cada vez mais esparsas. Na última delas alguns dias antes Roberto não fez perguntas; escreveu, apenas

_Gustavo está acá_

esperando talvez uma explicação

porque Gustavo não houvesse sabido explicar-se

sequer houvesse tentado.

Téo não escreveu de volta.

— _Te gusta caminar?_ — Violeta quis saber.

Ele afirmou com a cabeça.

— _Mañana vamos al cerro Pitriquitón, yo y unos amigos. Sin los niños. Vení con nosotros._

## 17.

### Capilla del Monte, Córdoba

O movimento de turistas em pleno outubro era quase inexistente, e àquela hora também não havia criatura que em férias decidisse acordar e seguir pela trilha contra o vento frio da manhã. Gustavo se havia feito o brasileiro curioso e encontrado um centro de informações ao turista na rodoviária, na noite anterior. Saiu de lá com mapa e ideias, suficiente para tomar a iniciativa no dia seguinte e decidir como ocupariam a manhã.

Téo não se incomodou: antes assim do que sentar e discutir e entrar em acordo. Que Gustavo fizesse questão daquele desvio na montanha não era tanto um problema. Era fácil estar com ele quando havia o que fazer e o assunto não corria de volta ao tema do relacionamento, o que eram e aonde iam e o que queriam e o tempo, as horas, as semanas

os meses.

Dizer: o relacionamento mais longo de Gustavo havia durado quinze anos e só terminou porque

uma tragédia.

Téo —

quanto tempo tinha junto a Gustavo?

seis, sete meses? Devia estar pensando na investigação, organizando as notas, preparando as perguntas.

Como podia gostar tanto da companhia de alguém e ao mesmo tempo

esse ímpeto de escapar, desaparecer.

O sol havia surgido enfim por detrás da serra ameaçando dia quente ainda que na sombra o ar continuasse gelado. Subir o Uritorco exigiria um dia inteiro e pareceu mais pertinente seguir pela base buscando no leito do rio um lugar para parar e arriscar os pés na água. Poderiam pegar estrada depois do almoço e em uma hora estariam em Cruz del Eje.

O rio corria preguiçoso por entre as pedras; o esverdeado da água se iluminou com a luz do sol e Gustavo tirou do bolso a câmera digital. Téo parou atrás dele. Buscou os óculos escuros na mochila e viu que Gustavo fez o mesmo, tão logo tirou a primeira foto. Girou o corpo e apontou a câmera na direção de Téo.

— Faz cara de quem não está me odiando.

Téo riu, sem jeito. Ajustou o boné na cabeça. Que estivesse distraído pensando no tio ou.

— Acho que eu não tinha percebido o quão argentino você é até te ver metido entre os argentinos — Gustavo disse.

Guardou a câmera no bolso e gesticulou com a cabeça adiante pela trilha que acompanhava o rio.

— Que isso quer dizer?

— Você se encaixa no cenário.

— Aqui?

— Entre os argentinos.

— Eu tenho cara de argentino?

— Mais do que eu imaginava!

— Você continua com a mesma cara de brasileiro.

Gustavo riu. Perdeu o equilíbrio com uma passada entre pedras e Téo avançou para tomar-lhe o braço. Téo não se sentia nem brasileiro nem argentino, ou ao menos não o suficiente para sentir que pertencia; jamais fosse possível pertencer tanto quanto adolescente em meio à torcida no estádio de futebol, fazendo desaparecer o próprio grito junto ao grito da multidão. Antes fosse questão de aparência; mesmo na adolescência era sempre _el gringo_, com ou sem o sotaque que não tinha.

O tio havia nascido em Catamarca, crescido em duas ou três cidades diferentes, seguido a Tucumán para trabalhar e;

Buenos Aires, família, filhos.

Que se sentisse estrangeiro também, um pouco errado

um pouco gringo.

Isso Téo podia compreender; o que não sabia era o impulso para abandonar tudo, meter as roupas numa mala, desaparecer. Qual fosse o tamanho do desespero ou

medo?

Quanto tempo ainda ele aceitaria a convivência com Gustavo, a boa companhia, os almoços de vinte minutos em dia de semana. Mais seis meses

seis semanas

seis dias.

Gustavo ali era todo o estereótipo do modelo brasileiro de exportação: a pele um tom de bronze, os cabelos cacheados que ele mantinha curtos, a barba que ele aparava com diligência todos os dias antes de sair para trabalhar e que ali longe do escritório havia deixado por fazer; uns olhos castanhos quase pretos e o sorriso tão fácil. Alguns meses de São Paulo, era certo — longe do Rio de Janeiro sua cidade natal — e um pouco o bronzeado lhe falhava, acostumado que estava ao terno e gravata no dia-a-dia de advogado.

Adaptava-se.

Estava em casa.

Pulando pedras em leito de rio nas serras de Córdoba;

(estava em casa).

Adiante surgiria um poço mais fundo formado na sequência de uma queda d'água pequena. Uma clareira na lateral fazia do lugar um bom ponto para parar e descansar e molhar o rosto. Gustavo sorriu para Téo e tirou das costas a mochila; apoiou-a numa pedra e aproximou-se do rio.

Téo livrou-se também da mochila. Parecia-lhe ainda frio o suficiente que melhor seria aproveitar o sol manso da manhã e a ausência de gente naquele pedaço de lugar. Gustavo havia tirado a camiseta e descalçado o tênis. Deixou a camiseta dobrada sobre a mochila; o gesto cuidadoso que Téo já conhecia. Sentou-se na borda e fez careta quando meteu os pés na água.

Téo riu.

— _No estás en la playa, brasilero_ — disse.

— Água gelada da porra — Gustavo resmungou.

— Me dê a câmera que tiro uma foto se você conseguir entrar. Você pode dizer pra tua mãe que estava cinco graus.

— Deve estar mesmo.

— Exagerado.

— Próxima vez que eu tiver folga eu vou é pra Bahia.

— Aqui no verão fica quente o suficiente.

— E agora não é primavera?

— Primavera aqui não é igual a nossa. Aposto que você nunca nem tinha visto primavera de verdade.

Gustavo sacudiu a cabeça em negação. Tirou as pernas da água e as esticou no sol. A cicatriz enorme que tinha na barriga parecia mais marcada tanto que havia perdido o bronzeado com os meses de viver em São Paulo. Buscou a câmera no bolso da bermuda para bater algumas fotos do lugar e Téo se acomodou ao lado dele, apoiando a mochila numa pedra e usando-a como encosto.

(Calmaria.)

O lugar pedia silêncio, um dia inteiro com o boné na cara e esquecer que adultos desapareciam, às vezes, por iniciativa própria, sem deixar explicação. Procurou na mochila o livro e a caixa com os óculos de leitura.

— Que você acha? — Gustavo havia perguntado algo, meio de costas, observando a vegetação.

A câmera sobre a camiseta; apoiou as costas na pedra ao lado de Téo e virou o corpo, colando o braço no dele. Téo pôs os óculos no rosto e abriu o livro na primeira página. Não tinha vontade de conversar. Sentia cada vez mais que a presença de Gustavo era como um desastre anunciado;

inevitável.

— De quê? — os olhos ainda no livro.

— Seu tio.

— Que eu acho? Não é assim que eu costumo fazer investigação.

— E nunca te apareceu um caso em que o cara desapareceu porque... não sei; arrumou outra?

Téo fechou o livro, ergueu os óculos até a cabeça e olhou Gustavo.

— A última vez que fui investigar gente desaparecida encontrei foi um monte de ossos — disse, e se arrependeu; sentiu que se arrepiavam os pelos dos braços, da nuca.

Era lembrança de que, sim, toda vez que se metia a detetive de cinema as coisas acabavam mal; a investigação no início do ano, quando conheceu Gustavo, não havia sido diferente.

Gustavo esboçou um sorriso que se desfez no instante seguinte. Era às vezes tão difícil: começar conversa, manter conversa, encontrar assunto. Gesticulou em direção ao poço como quem pergunta se Téo pretendia entrar na água, mas Téo:

— Eu posso ter cara de argentino mas cresci no interior de São Paulo. Esse negócio de água fria não é comigo.

— E se eu entrar?

— Não é uma competição!

— Mas eu sou do Rio.

— Que isso tem a ver?

— O Rio é mais quente que o interior de São Paulo.

— E daí?

— Se eu conseguir entrar, você não tem desculpa.

Téo tirou os óculos da cabeça e os deixou junto ao livro sobre a pedra. Cruzou os braços e mirou Gustavo ao seu lado — tão carioca: descalço e sem camisa e na cara os óculos escuros que ele quase nunca tirava quando ao ar livre. Téo a expressão que era quase um desafio:

— Já te falei o nome da cidade em que nasci?

— Não.

Téo soltou um riso abafado. Sacudiu a cabeça e buscou mais uma vez o livro e os óculos, como se para dizer que estava encerrada a conversa.

— Isso significa que você não vai dizer o nome da cidade?

— Significa que não vou entrar na água! — e não pode conter ainda outro riso, meio contrariado.

Tornou a pôr os óculos no rosto, abriu o livro. Fingiu se concentrar nas palavras que flutuavam fugidias na primeira página da novela. Gustavo o encarou por um instante, calado. Ainda um sorriso cansado. Que se contentasse com a companhia e aproveitasse a pausa até que fosse hora de voltar a pegar estrada.

Talvez fosse suficiente.

## 4.

### Buenos Aires, BsAs

Mais nada além de um comentário sobre o lanche ou o desconforto quando Gustavo sorriu demais para um dos comissários. Gustavo, os sorrisos. Não, Téo; lógico que não era ciúmes. Que ciúmes exigisse alguma vaga noção de posse e — convenhamos — isso ele não tinha, não podia ter

(lógico que não era).

Esperaram em silêncio enquanto todos se levantavam e tiravam as malas dos compartimentos, metiam-se pelo corredor e aguardavam que se abrisse a porta. Para além do portão de desembarque estaria seu pai ou sua mãe ou ambos, como sempre se dispunham a atravessar a cidade para buscar o filho no aeroporto quando visitava.

E afinal não havia história. _Esse é Gustavo._ A história pelas lacunas. Que Téo estava esperando? Gustavo não era de se fazer tímido ou calado. Não houvesse um papel a ocupar, Gustavo seria o que sabia melhor: ele mesmo.

Sentia mais que se dirigia a um destino terrível. Gustavo esperava talvez que Téo se manifestasse enquanto havia ainda tempo; dissesse-lhe o que esperava dele e ao menos fizesse claras as mentiras que precisavam ser contadas. Nada: Téo de todo desaparecido em seu próprio mundo.

Então as luzes e a confusão do _duty free_. Passar direto pela esteira de bagagem e a gente cansada que se aglomerava por ali e Téo não se lembrava de em outros momentos se sentir tão nervoso ao atravessar o corredor do desembarque. Reconheceu entre os rostos estranhos a expressão sorridente de sua mãe ao lado do primo Roberto. O primo ergueu o braço e se adiantou tão logo os olhares se cruzaram, os olhinhos brilhando por trás das lentes grossas dos óculos por trás dos reflexos das luzes do salão.

Abraçaram-se.

— _Hiciste buen viaje?_ — Roberto lhe tomou a mochila maior e voltou a atenção ao homem atrás dele; o carioca um tanto mais alto, a pele um tom mais escuro. — _Hola! Roberto, el primo. Placer._ — E lhe estendeu a mão.

— Gustavo —

(o namorado).

Sorriram-se. A mãe também: abraçou e beijou Gustavo como quem cumprimenta amigo antigo, disse em português que bem-vindo e como foi o voo, vocês chegaram na hora, não atrasou nem um pouco!

— _Trabajás com Juan?_ — Roberto perguntou.

Era estranho que chamassem a Téo assim: Juan. De repente um desconhecido. Quanto o apego que se podia ter a um nome. Juan Teodoro. Téo antes lhe havia contado como súbito mudava de nome bastasse cruzar a fronteira. Que a mãe sempre lhe havia chamado Juan e o pai Teodoro. Eram os nomes dos avós, o argentino e o brasileiro.

Gustavo buscou em Téo a resposta certa, a história que não haviam inventado. Podia dizer que era o assistente ou o motorista mas duvidou talvez de sua capacidade de falar sem dar risada da invenção. Fez-se distraído enquanto conferia o zíper no bolso da mochila e afinal tinha a barreira do idioma como desculpa. Tanto que Téo lhe prometia ensinar algo de espanhol mas nunca passava de meia dúzia de frases, perguntas e respostas, _hola que tal_ etc.

— _Es un amigo_ — Téo respondeu, mais para a mãe do que para o primo, e ficasse assim estabelecido o que havia para se estabelecer. — _Quería conocer a Buenos Aires, qué sé yo, cada loco con su tema_.

Julieta riu. Roberto repetiu a Gustavo _bienvenido_ e _está buenísima la ciudad si no vivís acá_ — como tanto Téo já ouvia ouvido de muito paulistano. Já era Gustavo centro das atenções; Roberto tentava lhe fazer perguntas no melhor de seu portunhol para saber de onde se conheciam e que você faz e já sabe o que quer ver aqui na cidade? Tomaram o rumo do estacionamento e a mãe tomou de Téo o braço, colando o corpo ao dele.

— Bom te ver, _mi hijo_.

Assim nessa mistura de idiomas que se havia tornado comum nas conversas entre os dois.

— Você sabe que vim a trabalho dessa vez.

— _Tu papá se quedó en la agéncia. Hoy tenemos unos clientes, y él se pone nervioso solo en la autopista._

Está bem, está melhor assim. Um passo por vez. Ainda precisava chegar, ajustar-se ao clima, ao idioma, aos ares. Pensar em encarar Pablo que se opunha com fúria à empreitada.

Ficariam hospedados na casa de Roberto. A mãe mais uma vez insistiu que mudasse de ideia porque o quarto de hóspedes ficava sempre vazio e a gente se vê tão pouco. Ela sabia o quanto durava a convivência pacífica entre Téo e o pai, mas, sim, insistia. Família, enfim. Porque Roberto tem as crianças, a casa dele fica uma confusão e em casa é mais tranquilo e teu pai vai ficar contente de ter mais alguém pra conversar em português.

— Assim que a gente terminar de chegar eu vou ver vocês — Téo disse. — _Vengo para trabajar_ — insistiu.

— _Dale, dale. Para vos es todo trabajo todo el tiempo, chico. Llegá, tomá un mate. Tu tío el boludo este no va a estar más desaparecido que ya está._

A última parte disse em voz baixa quando recebia o troco do estacionamento, e conferia que Roberto continuava tentando conversa com o mui brasileiro Gustavo.

Téo seguiu o olhar da mãe. Fingir que nada, estava tudo bem. A pressão da convivência e as obrigações de família. Explicar-se, justificar-se. Como fazia essa gente? Antes meter-se num cinema e

claro, os cinemas de Buenos Aires

as livrarias

as lojas que ainda vendiam discos e cds e as lojas de livros usados onde se escondia um livreiro barbudo e meio rabugento resmungando sobre a falta de cultura dos argentinos na cidade.

Perder-se na avenida Corrientes que toda sua adolescência havia sido como o centro do mundo — o segundo centro do mundo;

depois do estádio _del Racing Club_ —

porque a cidade era toda nostalgia e Téo sentia cada vez que voltava que perdia mais um pedaço da memória; que o passado se emaranhava no presente e o presente se escondia atrás do passado e era de repente um adolescente de quarenta e quatro anos procurando na avenida uma loja de discos que não existia mais.

Antes o passado soubesse ocupar o lugar que lhe era devido.

Haviam saído ao estacionamento. Téo respirou fundo buscando consolo no ar frio do fim de tarde. A mãe ainda falava: contava sobre o trabalho e a política e os vizinhos. As conversas sobre política e literatura eram também estar de volta à terra de sua mãe, ao mundo de sua mãe e do primo mais novo. Gustavo caminhava atrás, junto de Roberto, que menos falante parecia vez ou outra apontar a um lado e soltar um comentário breve.

Téo quis sorrir para Gustavo mas não teve certeza da própria expressão e talvez fosse só um erguer de sobrancelhas, dizer que enfim aqui estamos e não há mais o que imaginar sobre o que podia acontecer.

## 30.

### El Bolsón, Rio Negro

Deixaram o carro no estacionamento. Dali se tinha vista ampla do povoado: a avenida principal esticando-se por toda sua extensão e a RN40 que vinha de longe e a serpenteava, mudava de nome, cruzava o subúrbio e seguia adiante rumo ao sul, interminável. Ao fundo as montanhas nevadas que rodeavam aquele pedaço plano de terreno.

Violeta lhe tomou o braço. Estavam também outros três amigos dela e Téo não sabia como haviam feito caber cinco adultos naquele carro minúsculo e mais ainda como o carro havia sobrevivido estrada acima até o começo da trilha.

O dono do carro chamava-se Luiz; estavam ele e a filha, de dezenove anos, que morava em Bariloche e o visitava de vez em quando. Ele tinha talvez a idade de Téo ou

um pouco menos? — era um homem sorridente de rosto rosado e o excesso de cabelos brancos o fazia parecer talvez mais velho do que era: tinha uns olhos grandes de criança. A menina conversava feito estivesse entre gente de sua idade, sem se incomodar que o pai decidisse arrastá-la naquele programa de marmanjos. Contava sobre a última competição de esqui de que havia participado, de treinar em Ushuaia, do trabalho de verão que começaria ao terminar a viagem.

O quinto integrante do grupo era um andarilho de pouco mais de sessenta anos; um tipo miúdo e forte, os cabelos grisalhos meio crescidos e barba desajeitada. Orlando era artesão, como Violeta; vivia de povoado em povoado naquela região havia já alguns anos. Havia viajado ao norte mas preferia mesmo a Patagônia — _la Patagónia es infinita_, teria dito.

A Violeta havia ocorrido que ele talvez soubesse algo sobre o tal tio desaparecido, tantos anos que o homem rodava a região, incansável.

A memória se atrapalhava.

Quanto mundo era preciso ver e conhecer até que as coisas todas parecessem meio iguais, meio repetidas, meio o mesmo de sempre. A paisagem cinzenta, o vento cinzento, a poeira. O verde das montanhas, ali de cima. Só um pedaço minúsculo de Patagônia e o tio podia estar ali naquele mesmo povoado, fazendo outro caminho diário, tomando a esquerda um instante antes que Téo entrasse na avenida, sempre um passo à frente.

— _Sí, sí, creo que sí lo conocí_ — Orlando disse, quando viu a cópia da fotografia que Téo carregava consigo.

Era o primeiro a ter algum tipo de reação útil com aquela imagem velha de um homem que certo já estava muito diferente. Alguns, mais solícitos, tomavam tempo examinando a foto, feito se esforçassem para acrescentar os anos perdidos, os cabelos brancos, as rugas. Téo havia passado em todos os negócios mais antigos; conversou com os velhos na praça e com as figuras mais históricas do lugar.

Era conveniente: um povoado turístico aos pés da RN40, no rumo de quem seguisse mais ao sul; no rumo de quem tivesse como objetivo cruzar o país de La Quiaca a Ushuaia. Era conveniente demais, talvez; mais um lugar que Antonio teria passado batido, sem pensar duas vezes, parando talvez para um almoço rápido no último restaurante da avenida antes que lhe envolvesse outra vez a estrada e o deserto.

Orlando disse que sim, _tinha quase certeza_, mas precisava pensar um pouco e talvez a caminhada lhe despertasse as memórias antigas. Téo lhe poderia contar mais da história, e a vista era bonita, e o esforço da subida era uma forma de meditação.

Tomaram as mochilas no porta-malas. Téo levava também a câmera fotográfica porque, enfim

por que não?

Pensou mais uma vez

(mais uma vez; e desde quando o pensamento não lhe saía da cabeça?)

pensou que Gustavo deveria estar ali; que ele caminharia todo tempo com a câmera nas mãos, hipnotizado pela paisagem e pela neve no topo das montanhas. Que se esforçaria para arrancar de Téo sorriso mais sincero apesar do tumulto dos argentinos e do carro desconfortável. A ausência de Gustavo era súbito uma espécie de sonho: perceber que as coisas haviam tomado uma direção meio errada e era necessário fechar os olhos e voltar no tempo.

Saber o momento exato em que haviam começado seus problemas;

a velha obsessão.

Não fosse o tio desaparecer ele jamais teria ido a Buenos Aires terminar o colégio — a mãe o havia enviado como desculpa para mandar dinheiro à irmã que teimosa insistia não precisava de ajuda. Dali a amizade construída com os primos e todas as outras amizades muito erradas que havia feito no colégio. Dali as decisões futuras: a faculdade mal escolhida, as paixões mal resolvidas.

Ou: que seus problemas houvessem de fato começado no instante em que havia dito _sim_ à pergunta _vamos tomar uma cerveja qualquer dia_, meses antes, como era de se esperar.

Que estava fazendo ali, sozinho, cercado de gente que mal conhecia, uns tipos com quem tinha pouco assunto, aos pés dos Andes, numa das últimas paradas habitadas antes da imensidão da Patagônia no rumo da _Tierra del Fuego_

_el fin del mundo_.

A trilha adiante começava larga entre um bosque de árvores gigantescas, morro acima. Téo deixou que tomassem a dianteira e diminuiu o passo atrás de Violeta e Orlando, enquanto pai e filha seguiram na frente. Queria era interromper a conversa e insistir na memória perdida, a quase certeza; que Orlando não falasse sobre outra trilha que havia feito ou sobre receitas caseiras contra gripe e garganta inflamada.

Parariam depois do bosque em uma pequena construção de madeira que fazia as vezes de lanchonete e cervejaria. Dali a caminhada começava, de verdade. A vista se abriu do lado direito para as montanhas de picos nevados do outro lado do vale. Ao fundo, adiante, um lago azulado fazia-se espelho do céu. Luiz entrou na lanchonete para pegar emprestado um binóculo e levou a filha a uma pedra grande que servia de mirante, e Orlando aproveitou para descansar em um banco de pedra.

Violeta aproximou-se.

Téo havia tirado a câmera do bolso para fotografar a paisagem: as montanhas cercando o povoado e as ruas desenhando quadradinhos brancos no meio da vegetação.

— _Es muy lindo_ — Téo disse, mais porque não sabia o que dizer.

— _Me encanta este lugar._

— _Viniste acá antes?_

— _Creo que es la tercera vez. Preparate que la subida es dura._

Téo riu, sacudindo a cabeça numa afirmação meio incerta.

— _Viajás siempre solo?_

— _Es por el trabajo._

Por não dizer que na verdade sequer viajava, sozinho ou acompanhado. Visitava a família, quando muito. Bastava-lhe o bairro, as caminhadas repetidas na avenida Paulista. Que ia dizer a essa gente que ansiava a mudança constante de cenário e sotaques e rostos. Que era lindo, sim: a paisagem de revista de viagens, enorme, escapando ao enquadramento da câmera. Parecia-lhe outro universo. Dizer que entendia — entenderia — o apelo daquela vida, mas era um pouco como assistir a um filme e encantar-se com a direção de arte e desligar a televisão e dormir para seguir ao trabalho no dia seguinte.

Ela conteve o riso. Percebesse a conversa repetida. Trabalho? Que havia para se encontrar no topo, se não a vista. Saber a imensidão da Patagônia e sentir-se minúsculo. Vencer ladeira acima as pedras soltas e aquela areia vulcânica que cobria as partes mais altas e áridas das montanhas feito uma camada de poeira grossa, e davam ao observador distante a sensação de contemplar um castelo abandonado.

Téo passou a mão nos cabelos para assentá-los à revelia do vento que soprava forte. O suor esfriava a pele apesar do sol.

— _Te pasó algo_ — ela afirmou, feito contasse a Téo algo que nem ele sabia. — _Me parecés un poco triste._

Téo riu. Incomodado por não entender o rumo que tomaria aquela conversa. Se não tinha interesse em falar de si e seus problemas. Um pouco triste? Sentia que havia levantado pedras e espiado em arbustos e nada de Antonio; uma pilha de pistas que terminavam em lugar nenhum.

Sentia a ausência de Gustavo lhe gritando no silêncio.

Também assim desapareciam as pessoas?, um equívoco.

— _Un poco cansado, no más_ — Téo disse, e tentou um sorriso.

— _Por el de la pesquisa?_

Téo afirmou com a cabeça.

— _Bueno, yo conozco a una gente, acá y en Lago Puelo y Esquel; a ver si te puedo ayudar._

Luiz chamou e acenou, ainda sentado na pedra mirante. A filha havia erguido a câmera fotográfica e a mirava na direção do resto do grupo. Violeta passou um braço nas costas de Téo e levantou o outro em aceno alegre.

— _Vamo arriba, mi amigo detective_ — ela disse, gesticulando na direção da trilha.

## 5.

### Buenos Aires, BsAs

_Llegá, tomá um mate_;

e era o mate sempre tão mais amargo tanto mais tempo ficava longe

e tanto mais amargo mais ainda a nostalgia dissolvida

que parecia —

era possível?

a cidade toda tinha um cheiro diferente

(ou porque os carros tão velhos os motores desregulados tanta fumaça)

o vento gelado apesar do fim da primavera e o mate.

A casa de Roberto era um caos: as filhas pequenas brincavam no tapete da sala e discutiam a personalidade de uma boneca enquanto um gato amarelo observava a confusão sentado na mais altas das prateleiras — um labirinto de brinquedos pelo chão, quadros empilhados num canto próximo à porta, os livros e discos que o primo acumulava e não mais cabiam nas estantes. Na mesa da cozinha a esposa de Roberto esforçava-se no laptop para digitar um e-mail entre o ruído das crianças e a movimentação dos recém-chegados.

Era a familiaridade de estar em pé na cozinha encostados no balcão com um mate entre eles e a conversa

(um silêncio)

porque parecia um exagero que já discutissem a razão pela qual Téo estava ali, mas ao mesmo tempo não havia muito mais que dizer a não ser o óbvio da presença inesperada de Gustavo.

Gustavo um pouco sem jeito perguntou sobre os quadros e falaram da escola e da carreira paralela que Roberto levava como artista plástico enquanto mantinha-se como professor — porque havia afinal duas filhas para alimentar etc. Téo aborrecia-se. A conversa tão sem propósito e preferia falar de cinema ou música ou

enfim

da investigação —

e não aquele papo que era tão obviamente preencher espaços e não-ditos.

Gustavo teria se afastado por um instante para ir ao banheiro e Roberto:

— _Che, y este? Tu novio?_

Como quem supõe o óbvio, mas pede confirmação. Fez escapar com as palavras a frustração toda porque o primo não lhe contasse as coisas mais importantes, e afinal não eram também amigos? Téo quis sorrir, mas saiu foi uma careta esquisita, incerta.

— _Dale. Lo veo_ — Roberto disse. — _Complicado, sí?_

Téo concordou com a cabeça; quis lembrar que estava tudo bem, que era Roberto com quem conversava, que não havia segredos ou. Roberto, sim — entendia? Conhecia-o. Quisesse no comentário reafirmar aproximação depois de se sentir excluído. A esposa de Roberto não prestava atenção na conversa, mas também ali sua presença parecia uma intromissão.

— _Igual es lindo_ — Roberto completou.

— _Lo sé_ — e riu.

— _Más lindo que vos. Estás muy flaquito, primo._

— _Dale!_ — fingindo-se ofendido.

Ou dizer: não sei o que ele viu em mim. Isso se perguntava, mais do que era talvez saudável. A diferença de idade era só cinco anos mas às vezes ao lado dele Téo se sentia um pouco peça de museu.

Gustavo voltaria com seu sorriso de advogado e logo era a conversa do jantar e pizzas e escolher os sabores e procurar na porta da geladeira o ímã com o telefone da pizzaria. Téo teve vontade de se afastar e conversar com as meninas na sala, ainda que tivesse menos ainda que dizer a elas — ele que nunca havia sido o mais habilidoso na comunicação com crianças.

Seguiria o vai-e-vem em dois idiomas, enfim a pizza, algo de vinho — e Téo de canto de olho porque sabia o quão fraco Gustavo podia ser para bebida mas

quanto tempo mais

botar as meninas na cama

a sala com a televisão ligada

Karina semidormida no ombro do marido e Gustavo que _acho que preciso deitar e descansar_. Sabia — Téo leu em seus olhos — Téo sabia que era forma de dizer _conversem vocês sem mim porque é necessário_ e era;

era necessário.

— Vou deitar e fechar os olhos, estou meio tonto.

E fez discreto um gesto com os dedos que era para dizer havia bebido mais do que aguentava a cabeça. Disse _buenas noches_ um pouco desajeitado antes de seguir pelo corredor ao quarto.

Era estranho pensar que mal haviam conversado até ali: no aeroporto, no avião. Téo sentia que lhe devia;

algo, uma explicação.

Roberto quis saber havia quanto tempo se conheciam.

— _Che_ — Téo resmungou. Buscou na memória o primeiro contato em um boteco lanchonete da avenida Faria Lima; a cerveja uns dias depois e a chuva que o deixaria ilhado no apartamento de Gustavo. — _Desde febrero o... marzo?_

— _Tiene tiempo._

Téo respondeu com um gesto dos ombros. Estava na poltrona ao lado do sofá, com o corpo meio virado para a televisão a que não assistia. Sentia ainda a tensão do que estaria por dizer, das perguntas suspensas.

Dizer que sim, gostava da companhia de Gustavo e de estar com ele e almoçar juntos e acordar juntos. Gostava de observá-lo quando distraído dava de alinhar os talheres e o guardanapo sobre a mesa; a forma compenetrada com que amarrava os cadarços. Gostava dos silêncios e que Gustavo não perguntasse demais e o aceitasse e

entendia?

Roberto o encarava.

— _Es que no me gusta la idea de tener un novio_ — disse, enfim.

Roberto ergueu uma das sobrancelhas, solitária surgindo por cima dos óculos. Téo abriu a boca e gesticulou. Como ia explicar sem parecer egoísta sem parecer o pior dos seres humanos.

— _Me suena un poco..._ — e outro gesto vago adiante — _demasiado._

— _A mí me parece que das demasiada importancia a una palabrita. Qué dijiste a tu mamá?_

— _Nada._

— _Nada? En serio?_

— _Qué le importa esto, Beto?_

— _Es tu mamá._

— _Y bueno. Va a tener un montón de preguntas. La conocés._

— _Claro; si es la primera vez que venís así con alguien._

E tirou os óculos do rosto para limpar as lentes na camiseta, conferindo-as depois na luz da televisão. Téo o encarava, a expressão um pouco perplexa, e Roberto guardou para si a vontade de rir daquele primo mais velho que agia de repente feito seus alunos de quinze anos.

— _No es mi novio._

— _No, claro. Es tu amigo._

Téo percebeu o tom irônico e estreitou os olhos; quis lhe atirar um daqueles brinquedos jogados no chão da sala mas só fez cruzar os braços. Roberto riu, cobrindo a boca com a mão.

— _Che, te das conta del ridículo de eso? Sos grande. Tenés tu vida. No tiene sentido mentir para tus padres._

— _Y voy ahora cuarenta y pico años decir que soy gay._

— _Si no lo dijiste antes_ — e ergueu as mãos com as palmas para cima como para indicar o óbvio da formulação. — _Me sorprende que tu mamá ya no lo sepa._

Téo desviou mais uma vez o olhar. A penumbra da sala iluminada pela luz da televisão. Karina havia acordado com a conversa. Disse que ia deitar e precisava acordar cedo no dia seguinte; beijou o marido, abraçou Téo e saiu arrastando os pés pelo corredor. Roberto virou o corpo no sofá para olhar o primo de frente.

— _Pará, che_ — disse, gesticulando a postura defensiva que Téo havia assumido, feito fosse criança ressentida. — _Yo te conozco, primo. Te hace mal eso._

— _Pasa que no es algo tan serio como para._

— _Es suficiente serio para que lo traigas, no?_ — Roberto interrompeu. — _Llamalo compañero o novio o amigo, qué sé yo, da igual. No importa la palabra. Que yo sepa es como la primera relación tuya que te dure más de._

— _Mirá: él quería venir; vino. Yo vine por vos, y por la pesquisa que me pediste. No vine a mostrar el novio a la familia._

Depois pensou

e tomou-lhe um mal estar

que seria talvez mais fácil: dizer veja, este é Gustavo, meu namorado. Saltar assim esse processo de declarar aos pais identidade sexualidade passados os quarenta anos

um exagero.

Nem tanto pensava que a mãe fosse reagir de forma negativa — ou mesmo o pai? — e de qualquer formar

(diria a si mesmo)

tampouco importava.

Quis dizer que _che_, e teu pai, por onde vamos começar? Mas Roberto havia voltado a atenção à televisão, aborrecido com a resposta atravessada. Ou porque conhecia o primo e sabia melhor deixar assentar o tema e voltar a ele em outro momento. Téo olhou no chão os brinquedos coloridos das meninas e o gato que dormia no braço do sofá feito fosse um coelho. Quase compreendia esse tio desaparecido que havia fugido de

(as obrigações

os brinquedos de criança pelo chão

explicar-se justificar-se

amarrar-se).

— _Estoy cansado_ — Téo disse, e levantou-se. — _Buena noche, Beto._

## 18.

### Cruz del Eje, Córdoba

Cruz del Eje era menos como essas vilas de beira de estrada mas era também mais cinza, mais esquecida, mais o silêncio da hora do almoço quando chegaram. Uma curva à esquerda na estrada depois da serra. Gustavo disse que encontraria uma sombra para deixar o carro; esperava por ali ou saía para caminhar quando se aborrecesse. Que Téo tomasse seu tempo com a investigação e conversasse com quem fosse necessário conversar. Decidiam depois onde dormir ou onde jantar.

Era também esse Gustavo prático e objetivo que mantinha a relação inteira, e sabia mudar de assunto quando convinha, e aceitava o que ditavam as curvas e as horas. Fazia-se motorista ou

(cansado de espremer de Téo migalhas de diálogo).

Téo verificou o endereço e despediu-se. Seguiu a pé rumo à suposta casa da família de Javier, o único contato que ainda restava conectado ao tio Antonio: um amigo dos tempos de quando era caminhoneiro e rodava as estradas do norte do país.

Tinha era só o endereço. Cada passo era antecipação do fracasso: ninguém ali saberia de Javier ou Antonio ou qualquer outro caminhoneiro e seria o fim da investigação, mal começada.

Havia um pequeno sino ao lado do portão baixo de madeira. Téo respirou fundo e o tocou de leve.

Esperou.

A janela estava aberta. Dela chegou um murmúrio de vozes e pratos.

— _Ja, ja!_

Uma senhora apareceu na fresta aberta da janela. Tinha os cabelos curtos pintados numa cor clara; uns óculos de leitura na cabeça. Teria talvez cinquenta e tantos anos; parecia certamente mais velha que Elisa, mais nova que sua mãe. A expressão era só expectativa, se não um vago estranhamento pelo inesperado do horário.

— _Discúlpame si interrumpo. Busco Javier...? Javier Moran._

A mulher franziu as sobrancelhas; era mais surpresa que irritação. Téo sentiu no peito a fúria da expectativa. Ela abriu a boca mas pareceu lhe custar articular palavra.

— _Soy Juan_ — Téo continuou. — _Soy sobrino del Antonio. Era un amigo de Javier, de hace unos años._

— _Bueno_ — ela disse, enfim. — _Pasa que..._

Deteve-se. Sacudiu a cabeça e gesticulou para que Téo entrasse, sumindo da vista em seguida. O portão de madeira estava destrancado e Téo passou por ele; a mulher abriu a porta da frente.

— _Pasá, pasá_ — disse, com um sorriso atrapalhado.

Fez espaço e Téo aproximou-se, hesitou por um instante, cedeu à insistência. Entrou. A sala era simples: o sofá, um móvel de madeira enorme que acomodava a televisão e uma porção de miudezas, porta-retratos e enfeites. Um gato gordo estava deitado na poltrona mais ao lado, próxima ao corredor que levava ao resto da casa.

— _Soy Solange, la hermana de Javier_ — e ofereceu um sorriso tímido. — _Javier no vive acá hace unos años, viste._

Não morava mais ali, mas

referia-se a ele ao menos no tempo presente e era supor que estivesse vivo;

sim?

Fosse tanto o pessimismo adquirido esperava que;

quê? Que estivesse morto o homem, por acidente de estrada, de coração?

Téo um gesto de cabeça — para indicar a ela que continuasse. Teria certamente um número de telefone ou endereço ou e-mail. Não era bem o tipo de investigação a que Téo estava acostumado: fazer perguntas, medir respostas. Se tanto, Elisa era melhor naquele tipo de diligência. Téo sabia seguir, observar, prever movimentos. Investigava na distância tomando notas e fotografias. Não ali cara a cara com gente desconhecida para espremer informações de memórias distantes.

— _Sobrino del Antonio, decis? Antonio Gonzalez?_

Ela tinha o sotaque cantado da província de Córdoba, ondulando na frase a entonação das palavras e para o ouvido destreinado de Téo dificultando a compreensão do que dizia. Téo afirmou com a cabeça e buscou no bolso a cópia da fotografia do tio. Ela riu ao reconhecer a figura da imagem. Comentou que _era un buen tipo, este_ e havia anos não sabia mais dele e havia se mudado para a capital, não?

Era informação pouco útil mas era informação — saber que ao menos estava no caminho. Que a existência de Antonio no planeta houvesse deixado mais rastros do que esposa e filhos abandonados, uma fotografia velha, um punhado de memórias deformadas.

— _La verdad es que esperava preguntar a su hermano sobre el Antonio. Hace unos años ni sus hijos saben donde está._

— _Hijos, sí... qué raro_ — ela sorriu. Gesticulou na direção do sofá para que Téo se sentasse. Tomou o gato da poltrona e sentou-se também, deixando que o animal se acomodasse em seu colo. — _Sí, me acuerdo de que Javier dijo algo sobre Antonio tener hijos._

Téo perguntou se ela sabia algo de depois da mudança de Antonio para Buenos Aires; se ele havia estado na cidade, encontrado Javier. Um nome numa mensagem de telefone, talvez. A situação parecia a ela talvez cômica ou absurda — que tanto alguém poderia querer com aquele sujeito tão simples que havia sido parceiro de seu irmão em comboios passados.

Conversaram; à parte que estivesse atrapalhando o horário da _siesta_ e impondo-se no silêncio da casa. Téo teria preferido arrancar dela o contato de Javier e sair dali, seguir viagem, mas.

Solange tinha uma filha, sim, vivia na capital da província e trabalhava no hospital enquanto completava a formação. Javier não — nunca. Que houvesse se casado em algum momento, antes mesmo de Antonio, mas o relacionamento durou dois ou três anos. Vivia na estrada, como ditaria o estereótipo do caminhoneiro que não é capaz de criar raízes.

Isso Téo — e Roberto, antes, tantas vezes — havia considerado: o clichê do ex-caminhoneiro feito motorista de ônibus, descontente e insatisfeito por rodar em círculos dentro da cidade. Fosse psicologia barata mas

por que as pessoas abandonam uma vida inteira?;

para onde vão?

Isso não perguntou à Solange — que ela discorria sobre qualquer situação do passado em que os dois amigos houvessem se metido em encrenca numa passagem pela cidade. Ela parecia contente em seu entendimento do irmão como representação pura do espírito do caminhoneiro livre, figura que mulher nenhuma seria capaz de amarrar em matrimônio.

Pensar que também Antonio — enfim, o inevitável. Não se acomodaria jamais, menos ainda na cidade grande atrás do volante errado e a gente reclamando dos atrasos e criando confusão porque não tinha moedas e não tinha troco e o trânsito e a chuva.

Solange deixaria com Téo o telefone de Javier e um endereço. Javier fazia um circuito entre La Rioja e Salta e ficava quase sempre incomunicável mas toda semana estava de volta em Tucumán, na capital, onde estava a sede da empresa e onde ficava sempre numa mesma pensão. Javier, sim, saberia de Antonio; algo deveria saber, tão amigos e tão inseparáveis que haviam sido antes.

## 6.

### Buenos Aires, BsAs

O quarto de hóspedes era estreito e mal dava espaço para a cama de solteiro, de sob a qual se produzia ainda outra cama. Postas uma ao lado da outra, então, sobrava pouco espaço entre elas e a parede ao lado, onde haviam acomodado as bagagens. Em frente à porta, ao pé da cama, uma cômoda baixa com um abajur aceso. A luz amarelada iluminava a figura de Gustavo na cama mais alta: calça de moletom e camiseta, deitado de lado sobre o cobertor.

Um silêncio.

Téo sentou-se na cama mais baixa e teve vontade de deitar da forma que estava, calça jeans e camisa, por sobre o cobertor, sem nem escovar os dentes. Era cansaço, isso;

(se num dia comum trabalhando em São Paulo chegava a caminhar vinte ou trinta quilômetros a pé).

Que horas eram?

Buscou na mochila pequena o telefone celular e apertou os olhos para conseguir ler as letrinhas miúdas: uma mensagem de Elisa perguntando se haviam chegado bem. Téo clicou para responder:

“Chegamos,” escreveu, e hesitou antes de completar: “bem”. Sentia arderem os olhos com a luz da tela do aparelho. Deixou que os dedos deslizassem pelo teclado: “Não sei se foi boa ideia ter trazido Gustavo. Isso não vai dar certo.”

Enviou a mensagem e desligou a tela. Em seguida voltou a ela e escreveu mais: “Amanhã vou conversar com Beto sobre meu tio. Mando as informações que aparecerem.”

Moveu-se por sobre a cama até alcançar a mochila maior, tentando não fazer barulho. Abriu o zíper e a ajeitou aberta, uma parte apoiada na parede. Olhou ao lado a mala de Gustavo com a roupa do dia dobrada sobre ela; os tênis no chão com os cadarços metidos dentro.

Ouviu que atrás dele Gustavo se mexeu.

— Desculpa, te acordei.

Gustavo fez escapar um murmúrio. Teria se enfiado por debaixo do cobertor, arrumado a posição do travesseiro; Téo de costas adivinhou a movimentação mas manteve-se ainda imóvel.

(Desculpa, te acordei.)

Queria dizer algo. Queria a conversa miúda. Ouvir quando Gustavo resmungava sobre trabalho e petições e clientes; quando Gustavo reclamava de advogado ou dos papos de homem hétero no escritório; quando dava de falar mal de paulistano, indignado com o trânsito ou o mau humor dos funcionários públicos.

(Desculpa.)

A Téo pouco importava o dia-a-dia de um escritório de advocacia e as reclamações sobre a cidade e o trabalho eram sempre as mesmas, trocados os personagens. Era mais porque Gustavo deixava-se tagarelar noite adentro e era como o conforto de um filme repetido, um livro conhecido, uma trama previsível.

O quarto de hóspedes na casa do primo em Buenos Aires era todo o oposto. O idioma errado. Estar com a família que o chamava _Juan_ — à parte o pai, que continuava chamando-o Teodoro — e um pouco sentia-se outra pessoa;

e não sabia bem qual delas preferia ser.

— Esse colchão de baixo é meio mole — ouviu Gustavo dizer. — Se quiser trocar eu consigo dormir em qualquer lugar.

— Não tem problema.

— Se quiser dividir a cama também eu faço espaço.

Téo virou o corpo para trás, enfim. A cama era estreita, talvez ainda mais que uma cama de solteiro comum. Gustavo tinha o cobertor até a barriga e o encarava sonolento, mas sorria.

— Está tudo bem? — Téo perguntou.

Gustavo havia fechado os olhos mas franziu as sobrancelhas numa careta momentânea antes de permitir-se um sorriso. Abriu os olhos.

Era quase silêncio anunciativo. Esses momentos em que Gustavo acreditava que Téo fosse capaz de falar de si, dar um pouco de ordem ao caos que era tantas vezes seu comportamento e a necessidade de fugir de algo sem crises, sem cobranças, nada. Toda a paciência de Gustavo porque Téo fosse boa companhia e não lhe exigisse demais e ainda assim estivesse presente com seu sorriso de menino, talvez contra vontade, quase incoerente. As perguntas que não eram feitas e as respostas que não encontravam formulação.

Téo sentia-se perto demais da familiaridade da adolescência. Era outra vez Juan, _el primo_, sobrinho, filho. Torcedor _del Racing Club de Avellaneda_ que moleque inventava com os amigos briga de estádio no fim do jogo e precisava correr da polícia pelos trilhos do trem.

Era estranho num país que era também um pouco dele, apesar de. O idioma herdado. Sentir-se estrangeiro quando não compreendia todas as referências, as brincadeiras; a infância que teve no interior de São Paulo, longe dali.

Era o primo mais velho que a tia esperava responsável mas seguia na rua depois das dez da noite arrastando consigo o primo Pablo de onze anos.

Porque até o começo da faculdade em São Paulo era ainda Juan, até que.

Então retomar o nome meio antiquado herdado do avô paterno. Que _Téo_ fosse identidade construída por oposição: por saber quem não queria ser.

Não mais.

Mas voltava. Era Juan outra vez.

— Está? — a resposta feita pergunta.

Como se dissesse que sim, está tudo bem. De minha parte está tudo bem. Chegamos, você tem sua investigação, eu tenho sua companhia e uma cidade nova, um país novo para conhecer.

Está tudo bem.

Téo era enfim o adulto responsável que desde os catorze anos de idade esperavam dele; depois de uma faculdade de direito completada com má vontade, desistido da carreira e dos anos que o haviam levado até ali. Conhecer Elisa, começar de novo.

Téo Miranda, detetive particular.

Téo sabia — sabia? — o que Gustavo esperava como resposta mas as palavras não vinham. Só um gesto breve de cabeça e um sorriso sutil, quase invisível na penumbra. Quisesse pedir desculpas pelo mau humor ou.

— Meu primo sabe.

— Melhor.

— Não estou acostumado com isso.

— Tudo bem, Téo.

— Eu saí da casa dos meus pais aos catorze anos.

— Eu sei.

— E depois quando saí de Buenos Aires fui direto pra São Paulo, fiz cursinho, entrei na faculdade.

— Você já me contou isso, Téo. Tudo bem.

— Não é só isso.

Gustavo esticou o braço por sobre o colchão e colocou a mão no ombro de Téo, que ainda a posição meio torta na borda da cama de baixo. Téo inclinou-se para trás; apoiou as costas na lateral da cama e tomou a mão de Gustavo, trouxe-a próxima ao rosto, beijou-lhe os dedos, fechou os olhos.

— Toma um banho e vem deitar — Gustavo disse. — A gente conversa. Tudo bem.

Téo concordou com a cabeça. Buscou na mala a roupa de dormir e o estojo com a escova de dentes. O cansaço da viagem cobrava o que lhe era devido e Téo sentia os olhos pesados, a cabeça dolorida. Ou porque estivesse desacostumado com tanto mate e depois o vinho e.

(Não é só isso.)

Era descobrir-se adolescente em meio à euforia do futebol e às brigas de torcida e o ego mui masculino dos colegas e amigos as brincadeiras agressivas de machinho latino-americano e ser também parte daquilo; como não? Que nunca lhe houvessem ensinado outra forma. A faculdade de direito como continuação do mesmo universo — trocados alguns símbolos, alguns vícios; detalhes. Que tivesse feito os amigos errados ao longo do caminho. Apaixonado-se pelas pessoas erradas.

Até que enfim Elisa, a agência de investigação.

Um norte.

Mas é que não saberia

não sabia

não conseguia

colocar tudo isso assim em palavras ordená-las uma depois da outra e.

Voltaria ao quarto depois do banho para encontrar Gustavo dormindo, ressonando de leve com a boca meio aberta.

## 31.

### El Bolsón, Rio Negro

— _De Brasil? Con esta pinta?_

Téo sorriu, sem jeito. Afirmou com a cabeça devagar e devolveu o mate a Violeta sem tirar os olhos de Orlando, que mantinha ainda a expressão incrédula. Pensou em completar com o nome mui brasileiro da cidade em que havia nascido, mas concluiu que eles sequer entenderiam que se tratava de nome de lugar.

— _Pero sos totalmente porteño_ — Orlando insistiu.

O narrador na televisão levantou a voz mais uma vez e Téo desviou a vista. Outra chance de gol perdida. Parecia absurdo que o Racing estivesse no topo da tabela com aquela atuação engasgada. Orlando riu outra vez. Violeta, calada, passou-lhe o mate e voltou a prestar atenção nas crianças que brincavam do lado de fora. O sol havia desaparecido por trás das montanhas altas mas o dia seguia claro, num lusco-fusco cinzento.

— _A parte que es hincha de Racing_ — o artesão disse a Violeta.

— _Y se llama Juan_ — ela completou.

— Teodoro — Téo disse. — Juan Teodoro. _Mis amigos en Brasil me llaman Téo_.

Porque começava a incomodá-lo: o nome. Que já fosse um exagero aquele tanto de nome, aquele tanto de personalidade. Era tempo demais longe de casa. Estava cansado de ser _Juan_.

Violeta e Orlando olharam-se, como se para julgar o argumento apresentado. Téo mirou outra vez a televisão pequena montada no topo da parede do bar. Entre eles sobre a mesa a garrafa térmica e os pratos vazios empilhados. Téo explicou que a mãe era argentina e o pai brasileiro, e que havia enfim completado o colégio em Avellaneda, passado boa parte da adolescência virando Buenos Aires do avesso.

— _Dale, dale_ — Orlando disse. — _Sos el peor brasilero que ya conocí._

— _Gracias?_

Riram os outros dois. Era quase improvável que estivesse se divertindo depois das últimas semanas — o último mês. Porque era: sim — quase um mês desde que o avião havia pousado em Buenos Aires.

Estavam em um restaurante simples na ponta do povoado, no rumo do sul. Haviam parado para comer depois da caminhada ao Pitriquitrón. Luiz iria com a filha para casa; Violeta havia buscado as crianças e seguiam ali desde então.

Orlando tinha certeza: sim, conhecia Antonio. Havia conversado com ele. Um sujeito sorridente cheio de histórias mirabolantes sobre a cordilheira que logo se fazia calado quando o assunto era a própria vida. Botasse na fotografia uns cabelos mais brancos, a armação dos óculos menos grossas, mais modernas; não havia mudado quase nada.

— _Creo que lo vi más de una vez en los últimos años; pero no vive acá en el pueblo._

Que era dizer: voltava ali, eventualmente, quando lhe convinha. Um dos rapazes que trabalhava no restaurante havia tido a mesma reação: o homem da fotografia não lhe era estranho.

— _Tiene una camioneta, no?_ — havia perguntado.

Téo só fez esperar que o outro pudesse responder sozinho à própria pergunta. O rapaz entrou pela porta da cozinha e Téo ouviu que perguntava a alguém sobre o nome do homem da caminhonete verde. _Ni idea_, teria vindo a resposta.

O jogo do Racing na televisão seguia zero e zero. Pensou em escrever ao primo Pablo e dizer que pelo jeito bastava ele acompanhar uma partida que ela se alongava no empate. Era o tipo de superstição que o primo teria: não podia ir ao estádio com camisa nova recém-comprada; não podia reclamar da diretoria durante o jogo; não podia cortar o cabelo em dia de partida decisiva.

A lista era extensa.

Mas Téo não sabia se Pablo queria ainda falar com dele. Se voltaria a falar com ele. Se fazia sentido escrever-lhe e comentar o futebol como se

nada.

Orlando quis saber da viagem até ali e Téo lhe explicou o percurso desde Córdoba, a volta por Tucumán e a subida montanha acima até que alcançasse a _Ruta 40_ em Catamarca. Contou das pistas encontradas pelo caminho, as informações que acumuladas não lhe haviam ajudado a descobrir o paradeiro provável de seu tio. Se tanto, conhecia um pouco mais o homem e seus motivos, sua falta de motivos, seus mistérios. Violeta observava a conversa, interessada, ocupando-se do mate e vez ou outra conferindo que as crianças seguiam distraídas em suas brincadeiras.

— _Y que tal el viaje?_ — Orlando perguntou.

Téo olhou Violeta como se buscando ajuda, mas ela só devolveu o olhar curioso do outro artesão. Téo voltou a atenção ao jogo, esperando talvez que o assunto tomasse outro rumo. A caminhada havia sido longa e árdua, e sentia ainda o esforço nas pernas, o calor do sol ardendo na pele.

Quando baixou o olhar, ainda esperavam resposta. Violeta riu.

— _Bueno?_

— _No sé. Qué quieren saber?_

— _Que tal el desierto de Catamarca?_ — ela perguntou.

— _Ustedes lo conocen mejor que yo._

O deserto era principalmente a ausência de Gustavo

e Téo não queria mais pensar em Gustavo.

— _Qué sé yo, es seco y hay mucho viento._

— _Sos un poeta, Juan_ — Violeta brincou.

Ele riu. Como ia explicar; Orlando e Violeta viviam para viajar, e não paravam mais de dois ou três anos num mesmo lugar sem que lhes tomasse uma inquietação, a necessidade de outra mudança, de outro cenário, de outras pessoas. A Téo nada daquilo fazia sentido. Não porque não pudesse apreciar a beleza da paisagem — mais porque nele a inquietação o levava ao silêncio de seu apartamento ou o rumor da avenida conhecida; o conforto de um filme repetido.

A cordilheira era imensa.

O assunto na mesa desviou-se outra vez e Téo se distraiu com a movimentação no restaurante: àquela hora havia mais gente do lugar, trabalhadores no fim do expediente. Devesse talvez voltar em outro momento que não depois de uma caminhada de mais de seis horas mas — quão absurdo podia ser que quando finalmente se aproximava do tio tivesse tanta vontade de desistir e ir para casa.

Violeta contava de uma aventura no norte do país quando entrou uma mulher de uns cinquenta ou sessenta anos; quase todas as mesas interromperam suas conversas para cumprimentá-la, e também Orlando lhe acenou.

— _Es la dueña_ — explicou a Téo.

Fosse ainda dizer outra coisa mas o rapaz com quem haviam conversado se aproximou e gesticulou na direção deles ao falar com a mulher:

— _Isa, el flaco preguntó del de la camioneta verde; vos te acordás cómo se llamaba?_

Téo a olhou, oferecendo-lhe também um sorriso amigável, desconcertado com a súbita intervenção — já havia decidido que voltaria à investigação no dia seguinte. A mulher era alta e gorducha; tinha os cabelos de um loiro cinzento amarrados atrás da cabeça em um coque elaborado.

— _Camioneta verde?_ — ela não pareceu convencida de que a informação bastasse.

— _Mostrale la foto_ — ele disse a Téo.

Téo buscou na mochila a caderneta de anotações em que havia metido a cópia impressa da fotografia. A mulher os cumprimentou, puxou para perto uma cadeira e sentou-se com eles. Pegou os óculos na bolsa antes de tomar nas mãos o pedaço de papel.

Teria murmurado algo — um grunhido. Aborrecida talvez que a imagem fosse ruim e a fotografia demasiado antiga: um homem magro de cara redonda e a pele escura de um tom avermelhado. Tinha os cabelos muito pretos, ondulados, crescidos até a base da orelha; o bigode muito bem desenhado. Uns óculos de armação grossa escondiam um pouco sua expressão. Estava postado em frente a um portão de madeira pintado de azul, as costas apoiadas nele, os braços cruzados. Era impossível saber se sorria: fosse por causa dos óculos ou porque parecesse demasiado entretido em seus próprios pensamentos.

Era quase todo o primo Pablo, à parte o tom de pele e o bigode anacrônico.

A mulher devolveu a fotografia a Téo.

— _Puede que sí, no sé. De la F250 vieja?_ — perguntou ao funcionário.

Ele deu de ombros, incerto; depois afirmou com a cabeça.

— _Hace unos meses no lo veo_ — ela disse a Téo. — _Creo se llama Tulio o... Toño?_

— Toño? Antonio?

— _Puede ser? Hace fletes por la región._

— _Fletes?_

— _Transporte de todo tipo de cosas. Es buena gente. Pero no para nunca._

## 7.

### Buenos Aires, BsAs

Roberto não se lembrava de avô ou avó daquele lado da família. Primo, tio, tia, nada. O pai era filho único e a família vivia espalhada em outras províncias, desde sempre.

Não havia sido mal pai. Não era pessoa ruim ou agressiva ou o que mais se quisesse supor do estereótipo negativo de homem bruto do interior, ex-caminhoneiro, motorista de ônibus. Roberto se lembrava. Uma lembrança em pedaços, reconstruída com as migalhas de comentários ouvidos da mãe, do irmão. Era ainda muito pequeno, ocupado com inventar brinquedos com o pouco que tinham. Antonio voltava para casa sorridente, falava sobre o dia, contava histórias de gente que havia transportado, resmungava sobre a falta de educação dos outros. A mãe e o pai gostavam-se, entendiam-se. A mãe — e Téo a conhecia quase tão bem quanto a própria mãe; eram gêmeas idênticas em aparência e temperamento, em inteligência e teimosia — trabalhava também, e conversavam muito durante o jantar, discutiam as notícias e outros temas que a Roberto criança pareciam desinteressantes.

A memória era longínqua e meio apagada, mas era quase toda tranquilidade, se não algum desentendimento vago, um resmungo, um revirar os olhos e sair de perto. _Acho que foi esse o choque maior_, Roberto lhe teria dito, sentados os dois no banco da praça em frente à escola em que o primo dava aulas. Era o horário de almoço e naquela tarde Roberto não trabalhava.

Téo havia deixado um mapa turístico nas mãos de Gustavo e lhe dito que fosse caminhar, visitar San Telmo e a Recoleta, tomar sorvete e todas essas coisas que fazem os turistas em Buenos Aires. Um pouco a necessidade de criar distância, fazer espaço. Dizer que _não vou te aborrecer com coisas de trabalho_ e fazia mais sentido aproveitar o tempo para conhecer a cidade.

Gustavo sabia; entendia. Era melhor não esperar que a conversa suspensa da noite anterior fosse resolvida tão logo no dia seguinte.

— _Él tenía un amigo en Tucumán._ — Roberto disse.

Havia conversado com ele alguns anos antes e ouvido que tampouco sabia por onde andava seu pai. Que o contato havia se perdido muitos anos antes, pouco depois que Antonio se casou e foi morar na capital.

Talvez ele estivesse mentindo — foi o tempo que plantou a dúvida, fez crescer a desconfiança, imaginar que talvez encontrar o pai fosse mais fácil do que pensava; questão de esticar o braço na direção certa.

— _Pasa que no puedo dejar mi vida atrás y salir a buscar el pasado._

E era esse o trabalho de Téo: procurar o passado. Roberto sabia — tinha certeza — que o pai não estava em Catamarca, onde estariam os contatos mais próximos da família. Uma parte vivia ainda na província — em Santa María ou Belén ou nos arredores, aos pés da puna. Mas ninguém sabia dele, ou ao menos não aqueles que Roberto havia conseguido encontrar.

— _Mi mamá sabía del amigo camionero este, pero nada más. Sabía que existía. Podés imaginar que para ella mi papá se murió._

Havia ido embora sem aviso, mas

planejava?

Esperou que recebesse o salário do mês

resolveu as pendências e a conta no mercado

comprou pão.

A aliança de casamento sobre a mesa junto do maço de dinheiro poderia sugerir uma amante, mas a mãe de Roberto sabia: era pedido de desculpas. Era avisar que a decisão era definitiva, e que não o procurasse.

— _Y no te parece que ella ya sabía?_

— _Eso no. No tenía ni idea._

Era estranho que conversassem sobre isso depois de tanto tempo, apesar de todas as vezes que havia voltado para visitar. Se tanto havia ouvido a tia comentar as semelhanças entre o pai de Téo e o pai de seus filhos — não bastasse que a irmã fosse gêmea precisava também ter se apaixonado pelo mesmo tipo de homem. Mas era mesmo como se estivesse morto, e fosse caso esquecido, ainda que na época não houvessem se passado nem dez anos.

— _Y vos?_

Roberto tinha os pensamentos distantes quando lhe chegou a pergunta de Téo; deteve o olhar adiante. Os alunos da escola circulavam por ali, com suas mochilas e cadernos e gritarias. Talvez nem sentisse o cheiro de escapamento de carro velho que tomava as ruas da cidade — Téo incomodava-se apesar da estranha nostalgia que aquilo lhe trazia. Como se também o ruído dos ônibus fosse diferente de São Paulo e estar ali era mais um deslocamento no tempo do que no espaço.

— _Ahora o antes?_

— _Ahora. Que pensás?_

A cidade que era parte suspensa no tempo enquanto parte caminhava adiante. A cidade que em parte não havia mudado _y veinte años no es nada_ como repetiria para sempre Carlos Gardel. Roberto tirou os óculos e passou a mão no rosto, apertou os olhos com os dedos, ajeitou os cabelos meio crescidos que lhe davam o ar usual de professor de artes desajeitado. Tinha manchas de tinta nas mãos e na manga da camisa; uma marca de caneta esferográfica na cara, meio oculta pela barba.

A ideia de seguir a Tucumán para conversar com amigo distante do tio não parecia a Téo muito promissor. Tucumán era longe, o amigo já havia dito que não sabia onde estava Antonio; a família parecia pouco interessada.

Catamarca?

As províncias argentinas era mais nomes do que lugares.

— _No sé._

A resposta;

resposta?

Roberto levantou-se e disse que fossem comer alguma coisa antes de voltar para casa. Téo seguiu. Caminharam em silêncio por algumas quadras até que o primo continuasse a falar. Seu pai havia sido caminhoneiro, não é? Estacionado em definitivo na capital fez-se motorista de ônibus. Fosse afinal o que sabia fazer: estar em movimento. Talvez seguir em círculos um caminho repetido não fosse suficiente. Chamava-lhe a estrada. Os desertos.

— _Sos un romántico, Beto._

— _Que querés? Puede que me equivoco. Que tenía una amante, otra familia. Deudas._

Sabia era que a mãe engoliu o rancor, engoliu a frustração, engoliu a tristeza. Se os filhos perguntavam do pai ela dizia o que sabia, e mais nada. Ele foi embora. _Por quê?_ Porque. _Não sei_. Teria talvez dito que não, não era culpa deles, não era culpa de ninguém. Era de se imaginar que Pablo, criança furiosa, não teria acreditado.

Roberto não esperava que o amigo de Tucumán magicamente contasse do paradeiro do pai. Ou mesmo que soubesse. Mais que pudesse saber algo, um caminho, possibilidades. Que por ali estivesse a melhor chance de encontrar um norte. A vida na estrada lhe parecia a única resposta possível.

Por isso havia pedido ajuda ao primo detetive.

— _Tucumán, entonces_.

— _El amigo este tiene familia en Cruz del Eje. Es una ciudad chica al norte de Córdoba. Se llama Javier._

Fazia tempo que o contato havia se perdido, e a cidade da família parecia melhor como ponto de partida: Roberto pelo menos tinha ainda um endereço.

## 19.

### San Miguel de Tucumán, Tucumán

Chegaram na cidade de San Miguel de Tucumán no final do dia seguinte, depois de mais de seis horas de estrada e sol forte. Gustavo resmungou que estava moído quando enfim encontraram hospedagem e deixaram o carro num estacionamento. Decretou que o carro não saía dali enquanto não fosse para tomar estrada outra vez, e preferia andar ou tomar o transporte público por quanto tempo precisassem esperar.

— Eu avisei — Téo teria dito, em tom de brincadeira.

A falta de resposta foi a medida que precisava para estabelecer que ainda algo seguia instável, para além dos breves tremores que Téo havia vez ou outra observado no movimento das mãos de Gustavo, e à parte a pequena trégua que se havia instaurado na passagem pelas serras cordobesas.

Era outra vez cidade grande e a confusão do trânsito, as ruas antigas demasiado estreitas, as pessoas que passavam apressadas sem olhar para os lados.

Solange havia dito que tentaria contato com o irmão, e Javier já sabia de que se tratava quando respondeu a mensagem de Téo. Disse que estaria na capital no sábado, dali dois dias; tinha algumas diligências para resolver mas podiam conversar, claro

como não?

No quarto Téo escreveu a Elisa enquanto Gustavo sentado no chão se ocupava com um pequeno guia que encontrara na recepção.

“Ele vai acabar desistindo,” Téo escreveu. “Era melhor que desistisse.”

Preferia escolher um dos livros que a mãe lhe havia emprestado e deixar passar o tempo até a noite, deixar passar as horas no dia seguinte e na manhã depois disso, até que chegasse o momento de encontrar Javier e retomar a investigação.

Mas fingissem que estava tudo bem: pairava ainda entre eles algo —

as conversas adiadas

a noite do jantar na casa de seus pais.

Gustavo, teimoso: inventou roteiro e caminhada nos arredores da cidade para ocupar o dia que tinham livre e Téo deixou-se arrastar com o livro na mochila, aproveitando os momentos de pausa para ler — os óculos de leitura metidos na cara como quem ergue escudo e declara indisponibilidade. A conversa miúda sobre a vista e sobre o caminho, sobre o idioma e os sotaques, sobre teatro e música. Também o que faziam melhor: companhia um ao outro, entre silêncios e sorrisos.

— Você acha que ele vai aparecer? — Gustavo perguntou.

Esperavam num banco sob a sombra de uma árvore na praça Independencia. O sol estava alto e fazia um calor abafado. Téo olhou as horas. Estava acostumado a esperar: tomar posição num banco de praça ou lanchonete e fingir interesse num livro enquanto esperava alguém aparecer, algo acontecer. Pensou em explicar isso a Gustavo mas

já não haviam antes conversado sobre aquilo?

— Antes de escurecer devo estar de volta na hospedaria — Téo disse. — A gente se encontra lá.

— Certo. — Deu dois tapinhas na perna de Téo, devolveu os óculos escuros ao rosto e levantou-se. — Boa sorte.

Afastou-se.

Téo manteve o olhar adiante, perdido num dos edifícios de governo que rodeavam a praça. Sentiu no peito o alívio por estar enfim sozinho, de volta ao seu universo.

A espera.

Saberia reconhecer Javier por fotografias que a irmã lhe havia mostrado e explicara a ele onde estaria, disse que _não se preocupe eu encontro você_, que era como afirmar para si que ainda sabia o que estava fazendo. O mais longe que havia ido em investigação: a meio caminho da fronteira norte da Argentina. _El norte_ famigerado. Tanta a estrada e a cada povoado passado imaginava que talvez ali se escondia o tio, e todo passo adiante era também um passo mais que se afastava de descobrir a verdade.

Elisa lhe havia escrito: Alexandre estava pesquisando com empresas de transporte e logística na região para descobrir se encontrava algo. Era quem os ajudava quando precisavam cavar informações internet adentro — ou quando lhes falhava a impressora.

De todas as teorias possíveis era ainda a estrada a mais convincente. Amante, jogo, dívidas?; parecia improvável. Outra família? Mais um desses clichês ruins. Roberto havia conferido com a empresa na qual o pai havia trabalhado em Buenos Aires. Encontrou um antigo colega e ainda outro que desde aquele tempo mantinha ali cargo administrativo. Sabiam o mesmo que todos: Antonio pediu as contas e sumiu. Que era tipo falante, alegre — às vezes meio desligado; perdia-se em pensamentos com frequência. Gostava de cantar e tinha bela voz. Não se metia em política: desconfiava igual da esquerda e da direita e, um deles confirmara, sequer entendia a diferença entre uma e outra. Falava da família

e era uma só família

contava dos filhos pequenos ou de uma ida ao zoológico.

Falava também dos causos da estrada, de quando era caminhoneiro e cruzava a puna uma vez por semana. Roberto teria se surpreendido que se lembrassem dele, depois de tantos anos; que apesar da imagem difusa ainda mantivessem na memória as conversas e os resmungos, algumas histórias.

Téo olhou ao redor da praça. Já Gustavo havia sumido da vista.

Elisa havia escrito, também: “Você não é o único cabeça dura dessa relação”.

(Era melhor que desistisse.)

Reconheceu o sujeito que se aproximava: figura gorducha de cabelos grisalhos, um bigode espesso, a pele curtida de bronzeado antigo. Usava bermuda laranja e a camiseta branca estampada lhe ficava demasiado justa. Téo pôs-se de pé e acenou, e o homem respondeu com um olhar curioso.

— Javier — chamou, aproximando-se.

— _El sobrino del Antonio, eh?_ — o homem disse, mais em afirmação do que dúvida.

Ofereceu a mão num cumprimento. Téo lhe apertou a mão e tentou um sorriso.

— _Soy Juan_.

Javier parecia saído de uma série de televisão dos anos 1980. Tinha uns olhos miúdos cercados pelas rugas do tempo e do sol e umas sobrancelhas grossas ainda muito mais negras do que os cabelos. Uns óculos de leitura pendurados na gola da camiseta. Era fácil imaginar ele e o tio juntos, melhores amigos, parando o comboio no último restaurante ao final de um povoado de estrada para almoçar antes de se meter pelo deserto aos pés dos Andes.

Explicou que precisava buscar uma encomenda com um amigo e Téo disse que o acompanhava. Seguiram sob o sol por uma das ruas estreitas que bordeavam a praça.

— _Así que sigue desaparecido el Antonio_ — Javier falou, e havia em sua voz algo de satisfação, feito sorrisse por dentro.

A tonada cordobesa se misturava em sua fala com o sotaque do norte: as sílabas todas bem marcadas, como se em marcha militar. Téo aos poucos se acostumava a que o reconhecessem como portenho em vez de lhe oferecerem expressão confusa, como se o sotaque pertencesse a lugar nenhum. Quanto mais longe da Argentina que conhecia, mais argentino se tornava.

— _A usted le parece que ya debería haber aparecido?_

Javier ofereceu-lhe um sorriso enigmático.

— _Tuteame, chico_ — disse, por dispensar a formalidade que mesmo a Téo soava forçada. — _La verdad es que tampoco sé que pasó a él. Conmigo no mantuvo contacto. Pero uno puede imaginar, no?_

## 32.

### Lago Puelo, Chubut

Desceram do ônibus depois de atravessar o povoado, já próximos ao lago. Violeta desceu na frente para ajudar as crianças — as suas e as de uma amiga — e Téo seguiu atrás, sentindo-se ainda atordoado com a agitação das pequenas durante o percurso. A filha de Violeta tinha seis anos, e o menino quatro. As outras duas meninas tinham nove e quatro.

A mais velha fazia-se já guia turística e adiantava-se na frente, explicando às menores o que podiam e o que não podiam fazer. Violeta tomou o braço de Téo e sincronizou o passo com o dele. Caminharam atrás, sem pressa. Ela usava os cabelos presos num coque bagunçado, desfeitas as tranças; as calças largas coloridas e regata amarela. O sol estava alto mas soprava ainda uma brisa fria.

— _Vos tenés hijos?_ — ela perguntou.

— _Yo?, no._ — Téo a resposta meio atrapalhada, num susto.

Estivesse outra vez repassando na cabeça o dia anterior: havia recorrido o povoado perguntando sobre _el Toño de la camioneta verde_, o Toño que fazia fretes. Outras pessoas haviam dito que sim, se lembravam dele — ou haviam ouvido falar. A fotografia parecia então familiar, claro, pode ser que seja ele mesmo. Ninguém sabia muito além de que era de contar histórias, nem todas muito críveis.

— _Por?_ — Violeta quis saber.

Tinha o corpo próximo e a cabeça inclinada, encostada em seu braço; media os passos com cuidado para compensar as pernas mais curtas. Téo olhou adiante as crianças conversando, provocando-se, rindo animadas com o passeio depois da escola.

— _No sé?_ — a resposta feita pergunta. — _Nunca me ocurrió._

Violeta ergueu a cabeça para encará-lo; teria talvez a altura de Elisa, um pouco mais. Téo lhe passava por pelo menos vinte centímetros.

Era mais dessas afirmações genéricas; por dizer alguma coisa. Nunca entendia a intenção por trás das perguntas de Violeta, ou mais ainda se havia intenção; que ela queria saber? Ou: por que queria saber? Tão distraído com a investigação e decidir se continuava ou desistia e.

Quando finalmente uma pista, tão mais concreta.

Uma caminhonete verde — uma ocupação.

Porque ninguém sabia onde ele morava, ou se tinha residência fixa. Téo chegou a conversar com uma mulher que havia feito com ele o transporte de alguns móveis, mas o contato que ela tinha era número inexistente. Outros haviam dito era mesmo difícil encontrar o homem. Quando estava no povoado tinha sempre trabalho, mas depois ia embora, sumia por um tempo, ninguém mais encontrava.

Tomaram um caminho de terra até alcançar uma pequena praia de areia mais fina, cercada pela vegetação. Era parte do Parque Nacional Lago Puelo, Violeta lhe havia explicado. O lago esticava-se adiante, muito azul, aos pés de montanhas altas com os topos nevados. As crianças já estavam prontas para entrar na água quando Téo tirou dos ombros a mochila e a bolsa do mate que Violeta lhe havia dado para carregar.

— _Vamos?_ — Violeta perguntou, gesticulando com a cabeça em direção ao lago.

— _Ni muerto_ — Téo riu.

Violeta riu também. Tirou as sandálias e buscou na mochila um pano colorido para estender na areia. Sentou-se e trouxe para si a bolsa com o mate e a garrafa térmica.

— _Sentate_ — ela disse.

Téo obedeceu. Tirou o tênis e encolheu as pernas sobre o pano. Buscou na mochila o boné para se proteger do sol forte que se fazia mais agressivo tanto mais o vento disfarçava o calor.

Violeta o havia procurado na noite anterior para convidá-lo a acompanhá-la: levaria as crianças depois da escola para uma tarde no lago. Que ele poderia perguntar no povoado sobre o tio, e ela saberia indicar os lugares em que mais provável alguém soubesse algo. Que a companhia seria bem-vinda e não custava também se distrair.

Téo não entendia que tanto alguém poderia querer sua companhia — se mais estava quase sempre calado, distraído, aborrecido consigo mesmo.

Mas talvez fosse só isso: a presença silenciosa.

Gustavo estaria já de volta em São Paulo, no trânsito de São Paulo, ocupado no escritório com seus contratos e processos e o que mais. Téo pensou em pedir a Elisa que descobrisse como estava mas.

Violeta passou-lhe o mate. Tinha um sorriso cansado. Desde a conversa no restaurante dois dias antes que parecia mais pensativa. Abriu a mochila e apoiou sobre o pano uma sacola plástica com pães e um pote com queijo, tomate e cenouras. Chamou a menina mais velha e disse que avisasse aos outros para buscar o lanche dali a pouco.

— _La historia esta_ — ela disse, enquanto preparava entre eles a comida das crianças. — _Me hiciste pensar._

— _Qué historia?_

— _De tu tío. La pesquisa._

Téo fez só erguer as sobrancelhas. Apoiou o mate ao lado da garrafa térmica e olhou Violeta. Era isso que Elisa queria dizer quando insistia que ele era bom investigador? As pessoas falavam com ele — tanto mais se sequer fizesse perguntas. Falavam porque ele não parecia interessado? _É essa cara de sonso_, Elisa teria dito, em tom quase de brincadeira; _faz parecer que é incapaz de julgar os outros._ Fosse o hábito de detetive privado e passar o dia acompanhando a vida alheia sem se fazer notar. Era tão mais fácil desaparecer atrás do cenário.

— _Hace años los chicos no tienen contacto con el padre._

E apontou com o queixo na direção das crianças. A menina de Violeta parecia conversar muito a sério com a amiga mais velha enquanto a dupla menor corria pela parte rasa do lago, espirrando água.

— _Y donde está él?_

Ela ergueu os ombros num gesto lento.

— _Ni idea._

Era também artesão, ela contou; haviam viajado juntos por muito tempo, desde antes que chegassem à Patagônia. O menino havia nascido em Bariloche pouco tempo depois que se separaram; já o pai vivia entre a cidade e outros povoados menores, sumindo às vezes por semanas. Numa dessas ela não quis esperar para saber se voltaria. Conseguiu carona para seguir ao sul e foi embora.

Não se fez incomunicável: tinha ainda o mesmo número de telefone. Ele não a procurou e ela tampouco se esforçou para saber dele. A filha mais velha não se lembrava mais do pai, já mais de três anos passados, e o pequeno sequer o havia conhecido, de fato. Quando a menina perguntava, dizia que não sabia onde ele estava.

— _Te parece que en treinta años van a contratar un detective para encontrarlo?_

Era para ser brincadeira. Téo nunca sabia o que dizer quando as pessoas se faziam assim meio sentimentais. Violeta riu. Fechou dois sanduíches, tornou a botar água quente no mate e tomou um gole. As crianças vinham correndo e ela deu os sanduíches aos dois menores, disse que fossem sentar na beira do lago para não encher a comida de areia. Terminou de montar os outros dois sanduíches no mesmo ritmo tranquilo apesar da impaciência das meninas esperando em pé na frente dela.

— _Listo. Esperá, llevá agua a tu hermano_ — Violeta disse, botando um sanduíche em cada uma das mãos estendidas e entregando uma garrafa plástica de água à filha mais velha.

Esperou que se afastassem e encheu o mate com água outra vez; passou-o a Téo e ocupou-se de preparar um sanduíche para si.

— _Qué sé yo_ — falou, enfim, com um suspiro. — _Quizás. Igual es un pelotudo._

Téo riu. Contou do primo Pablo — ou, enfim, a parte da história que não doía contar. Que a investigação havia sido um pedido do primo mais novo, enquanto o mais velho havia sido claro em seu posicionamento: não o importava o pai, vivo ou morto, e não queria saber onde estava. Também Roberto não esperava um reencontro alegre; era mais curiosidade. Tinha já suas filhas e a mesma idade que tinha o pai quando os havia abandonado. Queria talvez entender o que havia acontecido.

Como se fosse possível entender o que havia acontecido.

— _Y vos tenés contacto con tu papá?_

Téo afirmou com a cabeça. Sempre o pai presente, mesmo à distância; mesmo quando passava meses sem falar com ele.

— _Tu papá es el brasilero, no?_

— _Demasiado._

— _Vos no tanto, eh?_

— _Te parece?_

Era argentino que não conhecia a Argentina; brasileiro desterrado torcendo para a seleção errada. O nome duplo feito duas personalidades era a clave para saber quem era, onde estava, quem devia ser. Sabia-se, sim, mais parecido com a mãe, pelo menos nos interesses. Temia — e isso quase nunca pensava — o quanto mais havia nele da teimosia e obstinação do pai.

Tão cedo que havia saído de casa.

O primo Pablo feito figurinha repetida da imagem do pai que ele detestava.

Violeta ofereceu a Téo o sanduíche montado. Ele insistiu que não tinha fome mas ela já abria outro pão e acomodava nele um pouco de queijo, tomate fatiado e cenoura, fazendo-se desentendida. Adiante na beira do lago as crianças menores corriam outra vez. As meninas mais velhas estavam sentadas num tronco caído, ainda o resto do sanduíche nas mãos, enfileirando pedrinhas que haviam encontrado.

— _Igual_ — Violeta recomeçou. Mordeu o sanduíche e tirou do rosto o cabelo que lhe escapava do coque com o vento insistente. Os olhos cinzentos faziam-se mais claros com a luz do sol, quase azuis. — _Quizás yo deba ya buscar por donde anda el pelotudo este_ — e gesticulou na direção das crianças. Falava outra vez do pai de seus filhos. — _No sé. Mantenerme informada, viste? Si después preguntan, voy a tener la respuesta._

Téo lhe sorriu. Sentia-se tão errado, súbito tornado parte da vida daquela gente tão diferente dele mesmo. O sanduíche de queijo que Violeta havia deixado sobre sua perna. As crianças que o chamavam _tío Juan_ feito estivesse por perto desde sempre — se menos de uma semana havia chegado ao povoado, e menos tempo ainda que havia encontrado Violeta no fim da feira de domingo. Se tudo que fez foi perguntar sobre um homem numa fotografia.

Sequer havia conseguido articular seu interesse por aquele impulso nômade que ele não entendia. Devesse perguntá-la _por que_ ou _desde quando_ e _como_ etc. Podia imaginá-la numa infância de cidade grande brigando com os irmãos por causa de algum brinquedo; o lanche da escola embalado com cuidado por uma mãe carinhosa.

— _Te aburro?_

— _Qué?, no._ — Téo sacudiu a cabeça.

É só a minha cara, pensou em responder, lembrando outra vez o que lhe diria Elisa. Essa cara de sonso entediado.

— _Está buenísimo acá. Está buena la compañia_ — completou.

Sorriu outra vez: o mais convincente que conseguia. Nem tanto que preferisse estar em outro lugar; não sabia mais onde queria estar.

Gustavo que não havia dado sinal de vida e claro, Téo, melhor assim; do jeito que você queria. Era mais o ego que doía — diria a si mesmo. O ego se recupera. Provou enfim o sanduíche e Violeta lhe devolveu o sorriso, pôs a comida e o mate de lado, moveu-se para mais perto e passou o braço no dele, encostando a cabeça em seu ombro.

## 8.

### Buenos Aires, BsAs

Puerto Madero era lugar familiar, repetido. A quantidade de turistas que mesmo em outubro caminhavam por ali com a câmera fotográfica; Gustavo na luz laranja do fim de tarde com os óculos escuros na cabeça, a camisa colorida com as mangas dobradas. Um sorriso sutil apesar do silêncio e tudo que não havia sido dito ao longo do dia.

Haviam se encontrado na praça San Martín. Conversaram sobre a cidade e o que Gustavo havia visto e as fotos que havia tirado. Téo teria comentado sobre o plano de começar a investigação em Córdoba e o convite de sua mãe para o jantar aquela noite. Dali alcançaram o canal e Gustavo fez que se distraía com a vista. Mudou de assunto. Também ele cansado de caminhar cauteloso entre as incertezas de Téo. Preferisse talvez o silêncio e a companhia: era onde melhor se entendiam. Ou porque houvesse desistido de entender.

Pararam próximos à _puente de la mujer_ e ao museu fragata, onde também outros turistas aproveitavam para tirar fotografias.

Gustavo buscou a câmera no bolso e sentiu o momento reprisado: um passeio que houvessem feito um ou dois meses antes, no parque Ibirapuera. Havia tentado tirar uma fotografia dos dois, juntos, e Téo súbito fechou a cara, aborreceu-se. Não deu explicação convincente; atrapalhou-se nas palavras e inventou que não gostava de sair em foto.

— Tira uma foto minha — pediu, e ofereceu a câmera a Téo.

Téo cruzou os braços. Aproximou-se de Gustavo e apoiou-se no guarda-corpo que os separava do canal. O gesto de cabeça era para que Gustavo mesmo tirasse uma foto dos dois: a foto que não havia acontecido antes.

Gustavo riu. Esticou o braço e mirou a câmera.

— Pelo menos descruza os braços — disse, cutucando-o de leve com o cotovelo.

Téo obedeceu, mas manteve-se sério. Estivesse fazendo o melhor que conseguia. Gustavo tirou a foto e conferiu a imagem no visor.

— Parece criança obrigada a sair na foto com a tia.

— Não gosto da minha cara nas fotos.

— É a mesma cara que você tem sempre.

— Então não gosto da minha cara.

— Eu não estou reclamando.

Téo revirou os olhos. Gustavo sabia que era a forma que ele tinha de receber elogio e fazer-se desentendido. Parecia às vezes improvável que Téo pudesse ser tão independente e tão imaturo. Tão adulto e tão menino.

— Você quer voltar no meu primo antes de ir pra casa de meus pais?

Gustavo custou a entender a pergunta. Tinha a vista perdida adiante nas formas da ponte, nos casais que caminhavam juntos, nos grupos de amigos que posavam para as fotos. O sol demorava-se próximo ao horizonte, preguiçoso. Guardou a câmera no bolso da calça e tirou os óculos escuros da cabeça para metê-los no bolso da camisa.

— Que horas você combinou com tua mãe?

— Oito.

— Te parece que estou apresentável?

Téo riu. Logo fez-se sério; quis dizer que não importava. Ou que preferia sentar num daqueles restaurantes e tomar um vinho até dar a hora de pedir um táxi. Que não era má ideia um pouco de álcool antes de encarar as conversas e perguntas que viriam.

— De qualquer forma me parece melhor tomar um banho antes de jantar com teus pais — Gustavo concluiu. — Passei o dia caminhando nesse sol.

Ocuparam um dos bancos de madeira. Gustavo sentou-se com as pernas cruzadas sobre o banco, feito criança, como costumava fazer. Virou-se na direção de Téo ao seu lado.

— Você fica nervoso sempre que vai encontrar teus pais ou é só porque estou aqui?

Téo voltou a considerar a ideia de tomar vinho num dos restaurantes que faziam vista para o canal. Pensou nas palavras do primo — a insistência do primo. O ridículo da situação.

— Não sei o que pensar — disse, enfim.

— Sobre o quê?

Téo sacudiu os ombros. Sentia que já haviam tido aquela conversa antes; que todas as conversas eram a mesma conversa. Que a conversa não acabava nunca. Custava-lhe se explicar: dizer que não queria se explicar. Gesticulou entre eles como se por dizer “isso” ou “nós”. Sabia o que Gustavo ia dizer.

Gustavo continuou calado.

Que ia dizer? Estavam juntos fazia já alguns meses. Gostavam-se. Téo continuava desaparecendo quando bem entendia; desligava o telefone celular e sumia sem aviso dentro de um cinema ou escapava de Gustavo na hora do almoço. Passava dias sem dar sinal de vida e depois mandava mensagem convidando para um café.

— Eu nunca... — Téo deteve-se. Cruzou e descruzou os braços. — Eu nunca quis um... relacionamento.

Gustavo fechou os olhos por um instante. Respirou fundo. Téo pensou que ele fosse se aborrecer, mas; Gustavo sempre tão paciente.

— E os que teve antes?

— Não dava pra chamar de relacionamento.

— E chamava de quê?

— Não sei. Companhia.

— Companhia?

— Antes de te conhecer — Téo disse, num murmúrio, enfim. Mirava adiante; um salto sem volta, como quem mergulha na água gelada. — Era isso. Companhia.

Mais um silêncio. Téo abaixou a cabeça, sorriu para si. Nem mesmo ele soubesse o que estava fazendo, antes, todos aqueles anos. Que fosse melhor não pensar nisso.

— Sexo? — Gustavo perguntou.

— Sexo — e sacudiu a cabeça, aborrecido — não me importo com sexo.

Gustavo riu. Téo o mirou de canto de olho, um pouco confuso, criança insegura que houvesse dito o que não devia.

— É o que você diz — Gustavo murmurou.

— Que isso quer dizer?

— Não quer dizer nada, Téo. Só estou tentando entender.

Quanto fosse que havia para se entender. Explicar o que nem mesmo ele se permitia racionalizar. Porque sim, gostava de estar sozinho, mas às vezes fazia falta companhia. Soava talvez menos ridículo quando o guardava para si.

Olhou adiante o canal; a luz alaranjada da tarde refletida na água. A presença de Gustavo sentado feito fosse menino em roda da escola. Era hábito do teatro, Gustavo havia dito uma vez: sentar-se no chão. Parecia sempre mais confortável.

— Era fácil encontrar companhia — Téo continuou. — Esses empresários, executivos, advogados. Uns tipos de passagem em viagem de negócios, como a gente encontra na avenida Paulista; na Faria Lima.

Deixou escapar um riso nervoso. Virou e Gustavo o encarava. Dessa vez manteve o olhar embora a vontade fosse desaparecer.

— Casados, às vezes — e ergueu a mão esquerda, apontando o dedo anular. — Sexo era só pretexto. Pra mim; enfim. Tanto faz. Depois eles iam embora, ou a cidade engolia, e era isso. Era assim.

Era o que Téo esperava dele, no início. A lanchonete na avenida Faria Lima e a aliança que ele ainda usava, apesar de. O sotaque carioca e o cartão de visitas: o número de telefone com prefixo vinte e um. Mais um advogado de passagem pela cidade de São Paulo. Assim havia começado. Por isso havia começado.

— Mais fácil.

— Não é só isso — Téo continuou.

Claro: algo faltava. Mais fácil, sim. A desnecessidade de intimidade; contar sempre a mesma história, ouvir os mesmos discursos de trabalho e dinheiro e chefe e mercado financeiro. Esquecer-se naquele papo mesquinho

_casados, às vezes_

na falta de interesse em encontrar-se outra vez

essa liberdade.

(Esquecer-se.)

— E agora? — Gustavo perguntou.

— Agora?

— Não pode ser isso? Companhia?

E gesticulou entre eles; mímica do gesto desajeitado que Téo havia feito antes. Téo quis dizer que não era a mesma coisa, mas calou-se. Havia esgotado toda capacidade de articulação sobre o tema e sentia que se explicava mal. Que voltavam ao princípio do problema; que sequer sabia dizer qual era o problema.

(Podia ser suficiente.)

Sentia-se meio ridículo. Roberto tinha razão.

Gustavo conferiu o relógio. Téo sempre esperava que ele perdesse a paciência e retrucasse, mas nunca. A invertida ficava escrita nos olhos e Gustavo mudava de assunto.

Levantou-se.

— Vamos achar um táxi.

## 20.

### San Miguel de Tucumán, Tucumán

O lugar lembrava os bares em que Téo quando adolescente juntava-se aos amigos para assistir ao futebol, em Buenos Aires. A decoração parecia ter permanecido a mesma desde pelo menos três décadas antes, assim como também os garçons e os clientes; Javier era figurinha previsível ali dentro e Téo;

Téo sentia-se em casa.

A narração do jogo na televisão chegava aos ouvidos num murmúrio. Javier cumprimentou os garçons como se fosse ali onde passasse todo seu tempo nos dias longe da estrada e Téo permitiu-se por um instante prestar atenção na partida que se desenrolava: reconheceu o uniforme do Independiente, time rival do Racing. Jogava em casa contra Godoy Cruz.

— _Te gusta el fútbol, chico?_

Téo sorriu. A resposta usual pareceu inadequada

_no es el fútbol;_

_es el Racing_

então só fez afirmar com a cabeça, indeciso, como quem confessa um segredo. Parecia ali menos terrível: afirmar que sim, _el fútbol_, sem que fosse necessário explicar que não acompanhava os jogos quase nunca; que no Brasil não torcia para time nenhum e que na copa do mundo acabava sem perceber torcendo sempre para a Argentina. Mas ali

(estava em casa).

— _Y Beto, como está? Era Beto, no? El hijo del Antonio. Tu primo?_

— _Sí, Beto. Me pidió ayuda. Es un poco mi trabajo._

— _Encontrar gente desaparecida?_

— _Bueno._ — E voltou a olhar a televisão. Sentir-se tão em casa tão adolescente e ainda Téo Miranda, detetive particular. — _Algo así_ — disse.

Javier chamou um dos garçons e pediu uma coca-cola. Téo pediu o mesmo. O homem trouxe os refrigerantes e conversaram brevemente sobre o campeonato e o Boca Juniors, o empate entre o Racing e o Independiente na rodada anterior. Téo distraiu-se outra vez com o jogo na televisão enquanto os dois amigos seguiram discutindo política. Ouviu que o narrador anunciava a partida do Racing contra San Martín àquela noite.

O garçom enfim se afastaria e Javier voltou a atenção à mesa. Havia jogado o corpo para trás e pareceu examinar a figura de Téo, medindo talvez o que poderia ser dito, o que valia a pena ser dito.

— _Tu primo me procuró hace como diez años, creo. Yo le dije lo que sé; lo que sabía_.

E que nada havia mudado: nenhuma informação nova, nenhum contato inesperado. Antonio era parte de um passado distante. Um amigo leal, companheiro de estrada, homem em quem se podia sempre confiar. Mantiveram contato depois que Antonio se casou, por um tempo, mas a vida da estrada e a vida da cidade eram pouco compatíveis.

A Téo começava a incomodar que os relatos todos mencionassem Antonio sempre como figura estável e íntegra —

um homem que abandona a família de repente, sem motivo, nada.

— _Pero hace unos años me ocurrió una cosa_ — Javier disse, então; aproximou-se da mesa e apoiou os cotovelos sobre ela. Tinha uma das mãos erguidas e o indicador estendido. — _Bueno. Dos cosas._

E levantou também o dedo médio, por indicar as contas com a mão. Tornou a fechar a mão e deteve-se, pensativo. Disse, ainda: não queria que Téo tomasse ao tio pelo tipo de pessoa que não era. Que Antonio não era desses. Mas era uma pista

_una idea, no más_

e afinal Téo havia viajado até ali.

— _Se llama María_ — disse.

Os ventiladores de madeira ali dentro giravam preguiçosos espalhando ar quente e Téo súbito sentiu mais pesado o calor. Tomou um gole do refrigerante e manteve-se calado, atento; via a movimentação do jogo com o canto do olho e pensou em seu primo Pablo, na indignação do primo Pablo e a conversa que tiveram antes que saísse de Buenos Aires. Todo aquele ódio e a expressão ofendida quando.

(que não tomassem Antonio pelo tipo de pessoa que não era).

Sentiu que a garganta apertava e se esforçou para prestar atenção no homem sentado a sua frente.

Javier respirou fundo. Estava talvez arrependido de compartilhar o que o amigo lhe houvesse contado em segredo. María havia crescido com Antonio em Belén, na província de Catamarca, cidade em que ele havia vivido dos dez aos dezoito anos; havia sido amiga de infância, grande paixão de sua vida, namorada nos últimos anos do colégio. Até que Antonio, desistido de continuar os estudos, cansado da morosidade de Belén e sonhando com uma vida de cidade grande, tomou o rumo de Tucumán, onde acabaria na mesma empresa de logística para a qual Javier havia começado a trabalhar — antes ainda que tivessem a licença para dirigir.

A paixão por María teria acabado de forma abrupta: que ele preferisse Tucumán e ela tivesse parentes em San Fernando, capital da província de Catamarca. Javier se lembrava ainda as conversas sobre ela no começo da amizade: era muita briga, muita energia desperdiçada, muita dificuldade para se entender.

— _Eso el Antonio no pudo superar, no tan fácil_ — disse, com um sorriso triste.

Ou: superaria;

porque a estrada, enfim.

Conseguiram juntos a licença para dirigir caminhões e juntos ganharam, aos poucos, a confiança do dono da empresa para os percursos cada vez mais distantes. Em pouco tempo cruzavam os Andes e desciam ao pacífico, faziam a volta por Santiago e metiam-se pelo _Paso del Libertador_ no rumo de Uspallata. Quase sempre em comboio, às vezes sozinhos. Havia outros percursos, mas esse era o de que mais gostavam.

Antonio teria encontrado o que sequer sabia que estava buscando e María;

— _Se hizo una lembranza más o menos triste_ — explicou. — _Pasados unos años, ya no hablábamos mucho de ella._

Calou-se. Havia tomado todo o refrigerante e voltou a atenção ao jogo de futebol. A partida chegava ao fim com vitória do Independiente. Téo pensou outra vez em Pablo; que teria planos para assistir mais tarde à partida do Racing, em casa ou naquele mesmo bar de sempre, com os mesmos amigos de sempre. Pablo que trabalhava com contabilidade o dia inteiro fechado no escritório e transformava-se quando o futebol, _la academia_, a gritaria do estádio ou a comemoração no boteco com os amigos. Fazia-se para as filhas o pai presente que não havia tido, conformado e determinado — e agarrava-se ao futebol no tempo que tinha livre;

um time que lhe era todo inconstância

tão estranho impulso por se entregar ao imprevisível.

Custava ainda entender aonde Javier queria chegar com a história de María — se Antonio a esquecera e a história terminava com os dois amigos seguindo o horizonte para além da cordilheira.

— _Y Anita?_ — Téo perguntou.

Anita era sua tia. Javier riu.

— _Mirá que yo me casé primero, con una chica de La Rioja. Estuve casado por, qué sé yo, trés años._

Porque lhe parecesse importante que Antonio se apaixonasse pela moça da capital tão pouco tempo depois que o casamento do melhor amigo se desfizera. Conheceu-a em Tucumán: ela havia viajado para um evento da universidade e — quão inesperado poderia ser que ficassem juntos? Antonio sequer havia terminado o colégio e ela estudava alguma carreira importante, falava de ser professora ou pesquisadora ou.

Téo riu. O tanto que a tia era parecida com sua mãe — para além do óbvio da aparência. Julieta havia deixado pela metade uma faculdade de filosofia quando enfiou-se Brasil adentro e foi parar no interior de São Paulo, enganchou-se com um aprendiz de eletricista metido a engenheiro, também ele mal o ensino médio completado.

Javier olhou o gelo que se desfazia no copo e tomou um gole do resto aguado. Não tinha pressa. Havia passado em dois lugares antes de levar Téo até o restaurante: buscou uma encomenda com um amigo, visitou outro para entregar um dinheiro. Mirava o copo quase vazio com a cabeça distante, e deixou que passasse um momento antes de seguir com a história.

Porque então tudo havia mudado: Antonio saiu da empresa, mudou-se para Buenos Aires; fez-se motorista de ônibus.

Teve filhos.

Então dez anos depois —

Javier repetiu o gesto que havia feito antes: dedo indicador em riste. Voltava ao começo da conversa. Téo ergueu as sobrancelhas em gesto de expectativa. O ar ali parecia cada vez mais abafado. Um dos garçons havia mudado a televisão de canal mas tão somente para encontrar o próximo jogo do dia: Unión contra All Boys. Téo viu na tela que já passavam de quatro da tarde.

— _Eso ahora me contó otro amigo_ — Javier explicou, antes de retomar a narrativa. — _Pasa que María estuvo en Buenos Aires unos meses antes que desapareció el Antonio_.

O tal amigo em comum — havia sido caminhoneiro, também, e passava sempre por Buenos Aires — costumava se encontrar com Antonio quando estava na capital. Enrique, chamava-se. Anos antes havia abandonado a estrada e assentado numa cidadezinha turística na província de La Rioja, na borda da puna. Levava turistas em circuitos _off-road_ pelos Andes.

María havia ido visitar uma prima, alguém da família, enfim. Haviam se encontrado, ela e Antonio;

e que estranho esse pedaço de passado se materializando na frente da gente.

— _Y te parece que el Antonio se fue detrás de ella_ — Téo disse.

Javier parecia ainda mais caricatura de si mesmo: o suor fazia brilhar a pele avermelhada e a camiseta colava ainda mais ao corpo. Estivessem ali trinta anos antes seria o mesmo homem: teria menos cabelos brancos, apenas.

— _Eso es el trabajo tuyo, no? Yo no sé que pasó._

— _Y la otra cosa?_

Javier abriu a boca para um _ah!_ divertido. Cresceu-lhe o sorriso. A outra coisa era menos fato; muito mais abstração. Era um romântico, o Antonio, teria dito. Como se dissesse que, fosse dita a verdade, não podia entendê-lo.

Que numa dessas, antes ainda que conhecesse a Anita, havia se apaixonado por uma artesã a quem deu carona uma vez, com quem depois manteve contato. Antonio falava dela, às vezes, como se ela estivesse sempre presente — embora a visse uma vez a cada tantos meses. Quando tinha folga prolongada, sumia atrás dela. Dizia que era uma amiga: que ela tinha a vida dela e ele tinha a dele.

Mas era pista sem saída: ele não sabia quem era a mulher. Antonio a chamava _la chica_. Que a houvesse mencionado em outros momentos também depois de casado, e por isso a lembrança. Javier não sabia de onde era e sequer sabia se era argentina. Era mais uma ideia do que uma pessoa: um ideal de liberdade impossível.

— _El oficio este, viste?_ — disse. — _Somos gente libre. Es parte de lo que llama a nosotros: la busca por la libertad. Hay que seguir adelante, sin parar, nunca._

## 33.

### El Bolsón, Rio Negro

O dono da hospedagem lhe emprestou o laptop para que pudesse fazer a chamada. Quase uma semana que alongava a estadia e um pouco começavam a tratá-lo como gente da casa; chamavam para almoçar com eles e compartilhavam o mate quando o encontravam sentado na varanda, segurando o telefone celular com o olhar perdido adiante para além das montanhas.

— Trabalhando hoje, _chica_? — Téo perguntou quando surgiu na tela a imagem de Elisa, e ao fundo a janela do escritório, inconfundível.

— Meu sócio me abandonou — ela brincou. — Dizem que foi parar na Patagônia.

— Pelo jeito é o vento que traz as tranqueiras todas pra cá.

Elisa arrumou o cabelo e reclinou o corpo na cadeira, sorrindo em silêncio por um instante, contente por rever o amigo.

— Feliz aniversário — Téo disse. — Como estão as coisas por aí?

— Um pouco paradas. Amanhã talvez apareça um cliente. E por aí?

Téo deixou escapar uma careta. Havia antes escrito para contar sobre a história da caminhonete verde e as pistas esparsas que talvez não significassem coisa nenhuma. Explicar que pensava em desistir; quanto tempo precisava esperar para que o tio aparecesse de repente? — ou seguir adiante e arriscar perdê-lo justo quando o fizesse. Talvez fosse suficiente dizer a Roberto que _seu pai está por aqui entre Rio Negro e Chubut, aos pés dos Andes, numa caminhonete verde_; em incansável movimento.

— Já tem criança me chamando de tio Juan, acho que é hora de voltar pra casa.

— Tio Juan! — Elisa riu. — Pois se depender de mim tome um pouco mais de tempo. Aqui está tudo bem.

— Não sei. Estou cansado de viagem.

— Falei com Gustavo esses dias.

Téo sentiu que o sangue lhe fugia da cabeça — o coração deu um salto. Olhou na tela a própria imagem que miúda no canto do monitor era mais uma mancha e a lembrança de que já não enxergava quase nada àquela distância sem óculos. Quis disfarçar, erguer as sobrancelhas, tentar um sorriso. Elisa media nele a reação provocada.

— Não conversamos muito; encontrei com ele por acaso naquele restaurante aqui perto mas ele já estava indo embora.

— Sei.

— Aconteceu alguma coisa que você não me contou, não é?

Téo abriu a boca para dizer que não, claro que não, e _o que foi que ele te disse?_ As palavras não saíam. Não queria mentir mas tampouco queria dizer a verdade e explicar a verdade e que a verdade precisasse fazer sentido. Dizer talvez que é mais simples do que você pensa ou

muito mais ridículo do que você seria capaz de imaginar.

— Ele tinha que voltar pra trabalhar — Téo respondeu.

— Foi o que ele me disse.

— Pois.

Elisa soltou um murmúrio desconfiado. Buscou sobre a mesa o telefone celular e mexeu nele por um instante.

— Palavras suas: “Gustavo foi embora; agora a queda” — ela leu na tela. — Vinte e quatro de outubro de dois mil e onze. Pouco mais de dez dias atrás. Me pareceu um pouco dramático.

— Devia estar bêbado.

Já tinha quase uma semana desde a ausência de Gustavo quando havia escrito a Elisa. E depois não disse nada que não fosse sobre o tio e as descobertas recortadas que o mantinham naquele povoado, amarrado à RN40, esperando que aparecesse no horizonte a caminhonete verde. Antes tivesse coragem de mandar tudo ao inferno — decepcionar enfim ambos os primos e confirmar a própria incompetência. Ouviu na sala de estar da hospedagem o rumor de novos turistas e o dono que explicava sobre o povoado e onde comer e o que fazer.

— Eram dez da manhã — ela conferiu.

— A gente pode conversar sobre isso quando eu voltar? Já tenho coisa suficiente me atrapalhando a cabeça.

— Claro, Téo.

Elisa havia feito uma careta aborrecida, revirado os olhos; mas sorria, ainda. Contou sobre o último caso e as primeiras aventuras da secretária como investigadora de campo, e sobre como haviam se divertido fazendo-se mãe e filha enquanto acompanhavam de longe o homem e a amante em um passeio no zoológico.

Téo ouviu na sala vozes de crianças e reconheceu a voz da filha de Violeta. Elisa ainda falava quando a porta do escritório se abriu e por ela apareceu a cabeça da menina, curiosa. Sorriu para Téo antes de se virar na direção da sala e sair correndo.

Violeta apareceu em seguida e Téo pediu a Elisa que esperasse um momento. Tirou um dos fones e viu que a artesã olhou incerta a tela do computador antes de voltar a Téo e esboçar um sorriso de desculpas.

— _Perdón, no sabía que estabas ocupado._

— _Es una reunión del trabajo_ — Téo explicou, porque lhe parecesse naquele momento a resposta mais fácil. — _Qué pasa?_

— _Está bien, charlamos después. Te espero en la plaza?_

Téo confirmou com a cabeça e ela saiu, fechando a porta com cuidado.

— Reunião de trabalho? — a voz confusa de Elisa lhe chegou pelo fone.

Então o gesto brusco de girar de volta à mesa e o fone ainda pendurado se soltou da orelha, caiu por entre as pernas e se enroscou nas rodinhas da cadeira. Téo ouviu que Violeta falava com as crianças e se despedia do dono da hospedagem. Abaixou-se para buscar os fones, metê-los outra vez nos ouvidos; deixou escapar um _puta madre_ frustrado antes de apoiar os cotovelos sobre a mesa e devolver a atenção à tela.

— Ainda tem isso — falou, e apertou os olhos com os dedos.

— Quem era?

— Violeta. É uma amiga, acho. Acho que... — e fez uma careta, como se duvidando das próprias palavras. — Não sei. Às vezes parece que.

— Está apaixonada?

— Eu não disse isso.

— Mas pensou.

— Não sei o que pensar. Talvez ela só seja... simpática.

Elisa soltou uma gargalhada e Téo cobriu o rosto com as mãos, fazendo-se envergonhado. A ideia lhe parecia ainda mais ridícula quando dita em voz alta.

— E você não disse que tem... outros interesses?

— Não sei como dizer isso.

— Diga que tem um namorado.

Téo devolveu uma careta indignada e Elisa sacudiu a cabeça — deixou-se rir mais uma vez porque era bem o tipo de situação em que Téo se permitiria entrar, desavisado, enquanto se distraía com outros temas. Ele contou como a havia conhecido, o tanto que ela o havia ajudado e da tarde em Lago Puelo, dos silêncios e olhares e mudar de assunto e falar das crianças e da água fria.

— Vai me dizer agora que nunca aconteceu de uma mulher se interessar por você?

— Sei lá, Elisa. Não fico prestando atenção nessas coisas.

Tornou a esfregar os olhos. Riram, os dois. Téo ainda as mãos sobre o rosto, sentindo-se todo inadequado no improvável da situação. Preferia acreditar que não era nada — que interpretava mal a boa vontade de Violeta para ajudá-lo e lhe fazer companhia. Elisa insistiu que não podia ser difícil inventar ex-namorado, elogiar qualquer homem passando na rua como quem comenta o clima. Quão terrível podia ser contar a uma amiga nova que era gay? De que tanto falavam se ele nunca falava de si?

— Não complique as coisas, Téo.

Gostava de Violeta; não queria magoá-la porque era incapaz de comunicar-se, fazer-se entender, ser direto. Complicava? Bastasse o que havia acontecido entre ele e Gustavo e mais de duas semanas ainda não sabia direito o que havia acontecido

o que estava pensando

se lhe devia desculpas ou;

era Gustavo quem deveria se desculpar?

Conversaram ainda alguns minutos até que Elisa precisasse atender ao telefone. Despediram-se apressados e Téo prometeu que cuidaria de lhe escrever mais, e que logo tomaria decisão sobre a continuidade da investigação.

Passaria em seu quarto para buscar a jaqueta — o sol já estava baixo e a noite trazia sempre uma brisa gelada — antes de sair da hospedagem em direção à praça. Viu de longe que Violeta corria pelo gramado junto dos filhos e por um momento teve inveja dessa capacidade de chamar de casa qualquer pedaço de terra em província estrangeira.

## 9.

### Buenos Aires, BsAs

Téo percebeu o aborrecimento quando Gustavo se levantou com um sorriso e saiu para a pequena varanda do apartamento. Seu pai havia ido à cozinha buscar mais vinho e a mãe distraía-se na estante de livros depois de comentar sobre algo que havia lido recentemente.

— _Llevalo para el viaje_ — ela disse a Téo, e virou-se com um livro na mão.

Seguiu-se um silêncio breve; Téo mirava na direção da varanda e Gustavo, para além da porta de vidro, observava embaixo a rua estreita apoiado no guarda-corpo ainda com a taça de vinho na mão.

— _Tu papá es más vivo que pensás, Juan._

Téo a encarou.

— Do que você está falando?

Julieta sacudiu a cabeça. Voltou ao sofá e deixou o livro na mesa de centro. Pensar: poderia ser pior. Que ela houvesse desistido um pouco do filho no momento em que ele catorze anos e o mandara para viver com a irmã em outro país. Sabia que ele não voltava mais; não para a vida de cidade pequena do interior de São Paulo — vida que também ela nunca teria imaginado para si. Ou: eram demasiados parecidos, pai e filho. Todos aqueles silêncios e tanta a dificuldade para falar sobre si.

Téo e a música e o cinema; o pai que mudava de hobby a cada lua cheia mas se agarrava a cada um deles como se fosse o único.

Joaquim voltou com a garrafa de vinho e o abridor. Se percebeu a conversa suspensa, fez-se tonto; sentou e apoiou a garrafa no meio das pernas para abri-la.

— Cadê teu amigo?

— Olhando a rua — Téo disse, e gesticulou com a cabeça a varanda às costas do sofá. — Deve ter cansado das suas perguntas jurídicas.

— Pra você eu sei que não posso perguntar essas coisas — veio a resposta, seguida pelo ruído da rolha que se despegava da garrafa.

— Não sou advogado.

— É o que você sempre diz. Me dê seu copo.

Mas Téo ainda tinha a taça cheia. Desde o início da noite que bebia devagar; observando de canto de olho o quanto Gustavo estava tomando, por saber o quão rápido o carioca ficava bêbado.

Desde o início da noite que Téo havia percebido o desconforto de Gustavo pelo ridículo de esconder a verdade e não saber o que dizer, esquivar-se das perguntas as mais simples e contar histórias pela metade. Desde o início da noite as conversas sobre trabalho e as perguntas do pai que curioso queria entender o que fazia um advogado no direito contratual e qual o tipo de cliente e o quanto do dia-a-dia do escritório era procurar buracos na lei para fazer passarem os deslizes.

Gustavo certo preferia conversar sobre outra coisa mas era enfim sua área e sorria fácil com as piadas sem muita graça que Joaquim contava. Não por isso havia se irritado e saído para o ar da rua na varanda. Julieta havia perguntado onde se conheceram e Téo se aborreceu, atrapalhou-se na resposta.

— Você estava investigando alguém perto de onde eu tinha aquela entrevista de emprego — Gustavo havia dito, mais a Téo do que aos outros. — Eu me intrometi no meio da sua leitura duma revista de cinema.

Que havia para se aborrecer? Téo havia concordado com a cabeça, como quem retoma memória perdida. Claro que se lembrava. Lembrava também como Gustavo estava vestido, e o corte de cabelo recente que deixava na nuca a linha bem definida da navalha, uma variação sutil no tom de pele por onde antes o cabelo. Lembrava a conversa e a referência a Philip Marlowe. A aliança no dedo. O gesto firme de estender a mão com o cartão de visitas entre os dedos e

_vamos tomar uma cerveja qualquer dia_

tão burocrático desde sempre fazendo espaços medindo distâncias testando reações.

Julieta se levantou outra vez. Seguiu à varanda e passou pela fresta da porta para juntar-se a Gustavo. Na sala chegavam mais os ruídos da rua e Téo não ouviria de que conversavam. Respirou fundo, tomou mais um gole de vinho.

— E teu tio, hein — o pai disse, jogando o corpo para trás e acomodando a cabeça numa almofada. — Que deu em Beto agora?

Téo deu de ombros. Joaquim se atrapalhava na pronúncia do português, tão acostumado ao outro idioma. Nem tanto por ter fluência no espanhol: dez anos morando em Buenos Aires e falava ainda um portunhol esforçado.

— Não deve ser de agora. Desde o começo do ano que Beto anda meio nostálgico.

— Mas agora te fez vir até aqui pra viajar país adentro. Já falou com Pablo?

— Ainda não.

— Vai falar?

A taça de vinho apoiada na mesa de centro. Preferia adiar a conversa com o primo mais velho tanto quanto fosse possível; conhecia bem o temperamento de Pablo e sabia já o que ia ouvir. Pablo lhe havia escrito antes da viagem para dizer o que pensava. Havia pouco mais que se falar.

— Se vou atrás de Antonio, Pablo fica puto comigo. Se desisto, é Beto que fica decepcionado. Que vou fazer?

— Pelo jeito vai viajar a Córdoba.

Téo abriu a boca para retrucar. Mirou mais uma vez na direção da varanda. A mãe e Gustavo lado a lado ambos apoiados com os braços no guarda-corpo, de costas para o apartamento, olhando embaixo o movimento da rua. Gustavo sorria para ela. Téo queria ir embora; abandonar Buenos Aires e voltar ao escritório e esquecer tudo aquilo. Joaquim fez que ia se virar para ver o que Téo estava olhando, mas deteve-se.

— Não é tão simples assim. — Téo disse. — Eles são praticamente meus irmãos. São meus irmãos pequenos.

— Eu sei.

— E o que você faria?

— Provavelmente o mesmo que você. Pablo é meio exagerado.

— Mas não é o Racing perdendo uma partida. É a vida inteira de ódio ao próprio pai.

Joaquim fez um gesto com a mão livre, como se dispensando o argumento. Tomou mais um gole de vinho e fez uma careta. Perdeu-se talvez em pensamentos até que olhou adiante e deparou o filho esperando uma resposta. Era estranha a familiaridade: que tivessem tão pouco em comum e tanto tempo sem convívio e ainda assim conversassem tão fácil.

Ao menos enquanto não se esgotasse a paciência de um deles.

— Se Pablo souber o que aconteceu ele ainda pode escolher o que fazer com essa informação — disse, enfim. — E se não quiser saber, que se faça de tonto; coisa que aliás ele faz muito bem. Ninguém está obrigando ele a abraçar o pai reencontrado. E convenhamos — deixou a taça vazia sobre a mesa de centro; virou-se para olhar a varanda antes de voltar a atenção ao filho — a chance de você encontrar Antonio a essa altura do campeonato é quase inexistente. Se é que ainda está vivo.

— Agradeço a confiança no meu trabalho.

— Deixe disso, Teodoro — e deu um tapa na própria perna, aborrecido. — Você sabe bem do que estou falando. O que tiver pra encontrar você vai encontrar. Mas não dá pra tirar Antonio da cartola.

Era o mais sensato que podia esperar de seu pai. Demasiado sensato; Téo concordava com ele. Pablo tinha seus dramas e manias que iam para além da paixão pelo futebol e _el Racing_. Que ia fazer? Que tomasse a estrada e fizesse o possível. Pablo entenderia. Que lhe custasse tempo, mas entenderia.

Também encontrar o tio passados mais de trinta anos era improvável. Deixaria no final ambos os primos decepcionados.

— Na pior das hipóteses — Joaquim serviu-se de mais vinho — o norte tem uns lugares muito bonitos pra visitar. Precisa ver o roteiro que sua mãe montou.

Téo riu. Que o pai fosse capaz de ser tão perspicaz e ao mesmo tempo tão rápido perder o foco da conversa. Pensou em levantar-se e descobrir sobre o que falavam na varanda, mas só fez esfregar os olhos e bagunçar os cabelos para depois arrumá-los outra vez.

— Devem estar falando de você — o pai disse, num riso contido.

Feito pudesse ler seus pensamentos.

— Tenho certeza que devem ter coisa mais interessante pra conversar — retrucou, irritado.

Viu que o pai erguia uma sobrancelha e voltava a atenção ao vinho. Melhor era inventar desculpa e ir embora. Antes que Gustavo ficasse sonolento e parasse de articular coisa com coisa. Antes que o pai desse de tagarelar sobre o hobby novo. Levantou-se e seguiu à cozinha. Fosse ocupar-se com a louça para lavar.

## 21.

### Tafí del Valle, Tucumán

Teria sido a primeira vez que tão maior o desentendimento. Que Gustavo antes ficasse frustrado ou aborrecido mas deixava crescer o espaço entre eles para depois voltar no escritório uns dias depois e convidar para um almoço. Não ali:

— Que tanto te custava uma merda de uma mensagem?

De Téo talvez a falta de hábito ou mais ainda a necessidade de provar a si mesmo que tinha razão e

(não vai durar).

Quando Javier foi embora começava a escurecer. Téo ficou no restaurante, pediu uma cerveja e uma empanada, deixou-se distrair com as análises pós-jogo na televisão, as fofocas do campeonato. Passavam das nove quando começou a partida do Racing e dali o tempo voou fácil na morosidade do zero a zero que se manteve até o apito final. Téo pensou em escrever ao primo Pablo. Havia algo ainda a dizer? Tirou do bolso o telefone celular e encontrou foram as mensagens de Gustavo.

— De que te serve essa porra de chip argentino se você não usa o telefone quando precisa usar?

Era a manhã seguinte: Téo havia chegado na noite anterior para encontrar Gustavo sentado numa das camas de solteiro do quarto, as costas na parede, o olhar perdido adiante. Meteu-se sob o lençol no momento em que Téo fechou a porta atrás de si. Teria grunhido alguma coisa, talvez um _boa noite_.

— Terminamos a conversa já era tarde; fiquei um pouco mais — Téo conseguiu articular.

Preferia discutir a continuação da viagem; queria sair do quarto, buscar algo para comer e meter-se na estrada, o quanto antes. Javier lhe havia conseguido o contato de María: ela havia voltado a viver em Belén, e era preciso iniciar a subida no rumo de Catamarca, cruzar a divisa da província e tomar a _Ruta 40_ em direção ao sul.

— Não me importa o que você estava fazendo. Não estou aqui pra te impedir de fazer merda nenhuma. Mas não custa uma porra duma mensagem pra dizer que vai demorar.

A voz de Gustavo saía por entre os dentes, num esforço para manter baixo o tom de voz. Téo teria aberto a boca para responder, mas desistiu em seguida. Botou nos ombros a mochila pequena e tomou nas mãos o resto da bagagem. Gesticulou em direção à porta e seria talvez sua forma de pedir trégua, mais uma vez. Como iam fazer crescer o espaço entre eles se precisariam ainda algumas horas compartilhando o mesmo carro, serpenteando serra acima em marcha baixa. Antes tivesse dito que _melhor você volte a Buenos Aires aproveite o resto de sua folga me deixe na rodoviária e eu me viro_.

As palavras não lhe saíram.

(Que tanto custava uma mensagem?)

Medindo no silêncio da estrada tortuosa a melhor forma de fazer com que se acalmassem os ânimos; que Gustavo se livrasse da expressão aborrecida que o acompanharia por todo o caminho. Dizer que _não sei fazer isso não sei dar satisfações pergunte a Elisa que trabalhamos juntos somos amigos tem vinte anos_. Era tão fácil abandonar-se aos hábitos da solidão e decidir que — por que não? — convinha assistir a um jogo de futebol tão em casa se sentia tão confortável que as coisas fizessem sentido;

o empate contra o San Martín era também como a frustração repetida de todos os outros empates a que havia assistido antes.

O sol estava alto quando alcançaram o povoado de Tafí del Valle. Gustavo pediu que fizessem uma pausa quando chegaram ao que parecia ser o centro. Precisava descansar de tantas as curvas impossíveis e precipícios imprevistos que haviam marcado a subida. O vento fresco quando desceram do carro parecia incompatível com o sol forte e o cinza árido das ruas de terra e pedregulho. Por todo lado eram as montanhas, feito estivessem cobertas dum veludo sujo.

O movimento nas ruas era mais intenso do que haviam visto em outros povoados. Entraram em uma lanchonete pequena.

Gustavo ainda a cara fechada. Havia pendurado os óculos escuros na gola da camiseta e limpado o suor com o tecido da manga. O rosto estava corado com o sol dos últimos dias; também Téo: tão mais branco certo que também muito mais vermelho.

— Foi empate — Téo disse, por fim, depois que a garçonete se afastou. Tinha o olhar fixo nas próprias mãos, observando talvez a diferença no bronzeado do braço direito e esquerdo.

— Empate?

— O jogo do Racing. Zero a zero.

A irritação de Gustavo transformou-se numa careta incrédula.

— Era isso que você ficou fazendo? — perguntou.

Téo confirmou com a cabeça. Era criança confessando molecagem; sua forma de pedir desculpas. Passou a mão nos cabelos. Odiava que se sentisse em dívida; que mais que nunca se perguntasse o que estavam fazendo juntos;

que Gustavo estava fazendo ali

que havia visto nele

por quê?

Gustavo estendeu o braço por sobre a mesa; a mão aberta com a palma para cima. Sabia o que Téo ia fazer: olhar em volta, verificar se alguém os observava, abaixar a cabeça, confuso.

— Ninguém aqui te conhece — Gustavo disse. — Ninguém aqui se importa.

Téo tomou a mão de Gustavo, sem muita convicção; deixou que se cruzassem os olhares. Gustavo sorriu. Era sorriso cansado, teimoso. Preferisse encostar a cabeça em algum canto e tirar um cochilo. Assim estiveram por um instante: encarando-se em silêncio.

— Eu... não sou boa companhia — Téo disse.

Recolheu a mão de volta para si e a escondeu sobre o colo, sob a mesa. Gustavo moveu o corpo para trás e apoiou as costas na cadeira. Manteve o sorriso; a irritação se misturava talvez ao cansaço.

— Quem tem que decidir isso sou eu.

Téo sacudiu a cabeça.

— Eu me distraio; com futebol, com música, comigo mesmo. Eu não... — deteve-se. A garçonete voltou com a água e o refrigerante, e Téo esperou que ela se afastasse para continuar: — Eu não sirvo pra ser namorado de ninguém.

O sorriso de Gustavo fez-se expressão de confusão; as sobrancelhas franzidas e a boca meio aberta como quem procura no vento algum resto de palavra para começar um raciocínio.

— Você acha que é por isso que estou aqui?

A pergunta retórica: Gustavo o encarava, esperava resposta. Esperava confirmação. Como quem testa o canal de áudio ou se certifica de que ambos falam um mesmo idioma. _Aqui onde?_ Téo pensou. Aqui em plena Argentina no meio do nada, no meio do caminho. Aqui ao meu lado; aqui, hoje.

_Aqui_.

Ou: o que esperavam os outros, antes? Sexo, companhia; sem amanhã, sem semana que vem, sem mensagem de bom dia. Nunca negociar horários, justificar atrasos. Havia provado algo a si mesmo? — quão proposital teria sido distrair-se com futebol

tão inesperado futebol

voltar à hospedagem quase meia-noite sem ter coragem de responder mensagem nenhuma, desistido de escrever ao primo, mastigando na cabeça as histórias de Javier

(que não tomassem Antonio por).

E que restava ali de seu tio senão o ato de abandonar a família; feitas em pedaços suas angústias suas motivações que quer que fosse o havia levado a desistir

resolver que

(não sou boa companhia).

— Você acha que eu ainda estaria aqui se não gostasse de você pelo que você é?

— Nem eu sei o que eu sou, Gustavo.

— Quer saber o que eu vejo?

Téo negou com a cabeça e Gustavo não conseguiu conter o riso. Inclinou mais uma vez o corpo para frente e apoiou os cotovelos e os braços sobre a mesa. Seguiam intocadas entre eles as garrafas da água e do refrigerante. Uma cestinha de plástico com sachês de mostarda e ketchup.

— Um menino que cresceu frágil e desajeitado numa cidadezinha de interior com um nome que ninguém sabia pronunciar direito.

— Gustavo, por favor.

— Que adolescente foi parar em Buenos Aires e pra ser aceito se meteu com um bando de machinho obcecado com futebol e com provar que é homem arrumando briga com torcedor do outro time, correndo mais rápido que a polícia. Que passou anos apaixonado pelo melhor amigo —

— Gustavo.

— Passou anos apaixonado pelo melhor amigo aterrorizado com a ideia de que alguém fosse descobrir a verdade. Que cresceu se sentindo uma aberração e que adulto inventou uma autoestima instável baseada em insistir pra si mesmo que a própria sexualidade não era importante, não fazia diferença.

Téo havia cruzado os braços; olhava em volta, inquieto, buscando no cenário algo em que pousar os olhos. Feito estivesse pronto para desistir da conversa, levantar e ir embora. Viu que a garçonete trazia enfim as empanadas. Agradeceu com um gesto breve de cabeça e esperou que ela voltasse ao fundo do restaurante.

Gustavo serviu o refrigerante no copo, tomou um gole. Téo percebeu, mais uma vez: o tremor no movimento, as mãos instáveis.

— Estou errado, Juan? — perguntou, pronunciando o nome devagar.

— Não me chama de Juan.

— Estou?

Téo abaixou a cabeça. O nome na voz de Gustavo soava todo errado. Pensou na conversa com o primo Pablo e a provocação porque _una amistad para toda la vida_. Que voltas havia feito, pensaria. A autoestima inventada para que pudesse se recuperar de uma queda. Um pretexto.

— Ele sempre soube.

— Seu amigo? E foi te contar isso quando? Adulto, não sei quantos anos depois? Quando a escola já era uma lembrança distante, quando já estava casado e com filho? Eu conheço esse ambiente, Téo. Eu cresci nele.

— E nunca se sentiu uma aberração?

— Todos os dias.

— Pois parabéns por ser mais corajoso do que eu.

Tomou para si a empanada sem desencostar da cadeira, ainda um braço por sobre o peito. Já o corpo meio de lado, quase por se ausentar o quanto fosse possível. Gustavo suspirou, frustrado. Deixou também que o olhar vagasse; eram poucas as mesas ocupadas, e a garçonete numa mesa ao fundo parecia entretida com uma conversa no celular.

## 34.

### El Bolsón, Rio Negro

A placa rústica de madeira tinha “Comarca andina del paralelo 42°” escrito em letras brancas; o segundo ‘l’ de ‘paralelo’ havia sido propositalmente desgastado ou recortado e só se podia vê-lo bem ali de perto, parados que estavam na lateral da estrada, esperando sob um céu nublado e cinzento. Era a saída do povoado em direção ao sul. Do outro lado da pista, em uma placa quase idêntica, lia-se “El Bolsón les da la bienvenida”.

O filho mais novo de Violeta estava sentado no chão de terra apoiado em uma mochila grande, brincando descalço com um dinossauro de plástico sobre o colo. A menina ao seu lado empilhava pedrinhas pequenas na areia cinzenta.

— _No me puedo creer que me convenciste a esto_ — Téo disse, em tom de brincadeira, olhando mais uma vez a própria mochila grande apoiada aos pés da placa de madeira.

Violeta riu. Vindo do povoado aproximava-se outro carro e ela deu um passo ao lado, ergueu a mão direita com o polegar em riste. O carro não parou. Ela olhou Téo e abriu os braços. Usava na cabeça o lenço marrom que ele a havia visto usar na feira de domingo. Os cabelos presos em uma trança apressada.

— _Te hace falta un poco de aventura, no?_

Téo havia dito que podia tomar o ônibus e assim atrapalhava menos; que com ele era um a mais exigindo espaço. Também porque pedir carona na estrada não era bem sua ideia de aventura. Violeta havia insistido e ele estava ali; a mochila pequena nas costas, o boné na cabeça, todo caricatura do turista desterrado.

— _Hacer dedo en la cuarenta es parte del que te hace argentino, Juan._

Outro carro que passava direto. Violeta não parecia preocupada. Estavam ali tinha mais ou menos meia hora; era cedo e o sol seguia escondido atrás das nuvens, pairando próximo às montanhas ao leste.

— _Soy brasilero_ — ele disse.

— _El peor brasilero._

Ela esticou o braço e tomou-lhe a mão, feito fossem namorados.

— _A ver tu mejor cara de pareja honesta_ — ela brincou.

Téo pensou em Elisa: que não complicasse as coisas. Deve ter feito uma careta. Violeta soltou sua mão e não pode esconder a expressão preocupada.

— _Qué?_ — Téo perguntou.

— _Sos muy difícil de leer._

— _Por qué?_

Ela sacudiu a cabeça, sorrindo outra vez. Verificou que os filhos continuavam tranquilos entre as mochilas antes de voltar a atenção à pista. Téo pensou que devia talvez falar de Gustavo — contar o que havia acontecido. Ou formular qualquer frase genérica e inventar “ex-namorado”. Que ficassem claras as intenções. Vinha um caminhão-baú pequeno e Violeta adiantou-se com o polegar erguido. O motorista devolveu o gesto invertido como quem se desculpa por não poder parar.

— _No sé_ — ela enfim responderia. — _Todavía me parece que._

— _Estoy_ —

— _Cansado, sí. Ya sé._

Ela riu. Perguntou se ele já havia ouvido falar de amizade de mochileiro. Que ela podia entender que ele se fechasse e não quisesse falar de si porque não a considerasse próxima o suficiente ou qualquer fosse o conceito burguês de amizade a que ele estava amarrado. Téo riu da expressão, mas ela manteve nele o olhar determinado. Disse que era um exagero: estavam os dois longe de _casa_; estavam os dois na estrada, no rumo do _fin del mundo_.

— _De donde sos?_ — Téo perguntou.

— _De Rosario._

— _Creciste allá?_

Violeta cruzou os braços, sorridente; tinha na cara uma expressão vitoriosa, como quem termina de comprovar um argumento complicado. Téo riu; percebeu a acusação silenciosa. Teria dito que fazia também o mesmo com Elisa, que era amiga de muitos anos, mas só fez abaixar a cabeça como quem confessa o delito. Tirou o boné da cabeça e arrumou os cabelos.

Que ela concluísse sua teoria.

Amizade de mochileiro, sim? Que era quando a certeza do tempo curto compartilhado fazia dispensar os rodeios. Uma amizade inteira, em minutos.

Téo ia dizer que _desculpe o pensamento demasiado burguês mas_;

pareceu-lhe desnecessária a agressividade.

— _No soy mochilero_ — disse, como se bastasse a constatação.

— _Bueno_ — Violeta sorriu outra vez. Gesticulou com a cabeça para apontar o espaço ao redor. — _Tenés ahí dos mochilas y estás haciendo dedo conmigo en la ruta cuarenta. No sé si se puede ser más mochilero que esto._

Téo olhou outra vez a mochila jogada aos pés da placa; os filhos de Violeta em volta das mochilas dela feito também eles fossem parte da bagagem. Sorriu, mais para si do que para ela — pelo óbvio da argumentação. Ou porque devesse abraçar o quanto possível aquela lógica se tivesse qualquer intenção de entendê-la.

Outro carro se aproximava e Violeta voltou-se mais uma vez para a pista com o dedo levantado. Também Téo: postou-se ao lado dela e repetiu o gesto de erguer o polegar. O motorista chegou a gesticular para explicar que o carro estava cheio antes de passar direto.

Cresceria entre eles um silêncio tranquilo. Violeta divertia-se que Téo enfim se empenhasse em acompanhá-la no gesto aos motoristas que passavam, a maioria sem sequer olhá-los. Alguns poucos justificavam-se ou gesticulavam uma negativa.

O destino era Esquel: o próximo povoado no rumo do sul pouco mais de duas horas de carro dali. Era também o último lugar mais ou menos habitado nos arredores da RN40 antes de uma infinidade de micro-lugares, fazendas e vento — ao menos até que se cruzasse para a província de Santa Cruz. Haveria ali no fim de semana um evento de música e esporte e Violeta queria aproveitar o movimento de turistas para trabalhar. Havia dito a Téo que fossem juntos: ela conhecia algumas pessoas e quem sabe também por lá Antonio passeasse na caminhonete verde.

Parecia melhor do que seguir plantado em El Bolsón esperando que o tio lhe caísse de uma árvore — tampouco teria muito que fazer sem a companhia insistente de Violeta.

O sol arriscou mostrar as caras antes de se esconder outra vez atrás das nuvens. Soprava ainda uma brisa fria. O movimento de automóveis parecia diminuir a cada instante passado e Téo se perguntou por quanto tempo duraria a paciência de Violeta com o dedo erguido na beira da rodovia.

Quando retomaram a conversa ela contou que havia saído de Rosario aos vinte e dois anos, cansada de saltar de trabalho em trabalho e incapaz de manter emprego por mais de quatro ou cinco meses. Vendeu algumas coisas, comprou passagem para o Uruguai, conheceu gente pelo caminho. Seguiu o fluxo do turismo: trabalhava nas hospedagens e aprendia com outros que faziam o mesmo.

Por também dizer que vê: somos amigos. Eis minha história.

— _Lo que quiero decir_ — Violeta voltaria ao tema de antes como se jamais nenhuma interrupção. — _Si te pregunto las cosas, es porque me preocupo._

Téo um gesto vago da cabeça; quis dizer que sim, aceitava a premissa. Que ela tinha razão e ele era bicho de cidade grande desconfiado de tudo e todos e buscando nos outros intenções as mais escusas. Ou que se inquietava quando lhe faziam perguntas porque era sempre explicar-se justificar-se

as demandas todas

as exigências que ele não era capaz de cumprir

o ruído ensurdecedor do vento na janela do carro

o deserto interminável de Catamarca.

Violeta o encarava.

— _Lo hiciste otra vez, Juan_ — ela disse.

— _Qué?_

— _Te perdiste en tu cabeza._

Téo apertou os olhos e tentou um sorriso.

— _Te puedo pedir una cosa?_ — ele perguntou.

— _Por supuesto._

— _Llamame Téo. No me gusta Juan._

Ela devolveu uma careta confusa mas sorriu em seguida, fez que sim com a cabeça e esticou o braço para lhe tomar a mão.

— _Entonces por qué te presentás como Juan?_

— _No sé_ — e deu de ombros.

Era: hábito? Havia sido desde sempre um estranhamento. Voltar a ser o adolescente que não queria ser; que havia decidido deixar de ser. Autoestima inventada — Gustavo havia dito. Tinha razão? Que o estranhamento fosse suportável enquanto ficasse pouco tempo, mas.

Um mês.

Gustavo tinha razão.

— _No sé_ — repetiu.

— _Bueno, Téo_.

Violeta apertou-lhe a mão. Quisesse dizer: estou aqui.

Téo quis responder. Explicar a memória confusa que ele empurrava para trás cada vez que ela ameaçava tomar conta. Dizer que não havia começado a viagem sozinho, mas havia feito algumas escolhas ruins pelo caminho. Que se perguntava o quanto também ao tio não havia ocorrido algo assim: um punhado de escolhas ruins. Dessas curvas pelas quais se passa adiante sem saber que depois é impossível voltar.

Vinha pela pista um jipe pequeno e Violeta repetiu o pedido de carona. Téo a imitou: o gesto já automático, desesperançado. O carro diminuiu a velocidade; passou por eles. Parou alguns metros adiante. As crianças se levantaram num pulo sincronizado e Violeta correu até o carro estacionado.

## 22.

### Santa María, Catamarca

Gustavo entendia. Queria dizer _eu entendo_. Está tudo bem. Fabiano havia sido seu namorado companheiro melhor amigo por quinze anos; fugido de casa aos dezessete porque os pais não aceitavam o _filho gay_. Entendia?; se ele desde sempre a relação de amizade com a mãe e um pai meio distante que nunca pareceu se importar com coisa nenhuma — e fez foi reclamar que Fabiano era _só um moleque_ e você devia procurar homem da sua idade.

Entendia?

Deixaram passar algumas horas no povoado de Tafí del Valle; caminharam pelo centro depois de comer, e Gustavo distraiu-se conversando num inglês meio quebrado com um grupo de turistas que tomava cerveja na praça. Téo aproveitou o espaço criado para escrever a Elisa e contar sobre Javier; dizer a Roberto que tinha um novo contato e chegaria em Catamarca ainda naquele dia.

Uma mensagem de Alexandre lhe informava que Antonio havia estado em La Rioja, pouco tempo depois de abandonar Buenos Aires. “O nome dele consta numa lista de colaboradores de uma agência de turismo num povoado chamado Villa Unión.” E a mensagem seguinte: “Pode ser coincidência de nome. Mas encontrei uma fotografia antiga e talvez seja ele mesmo. Vou continuar procurando.”

Pensou no tal Enrique, amigo em comum de Antonio e Javier, ex-caminhoneiro que se havia feito guia turístico aos pés da puna de La Rioja. Dele não tinha contato, mas Javier tinha certeza que continuava com a agência de turismo, embora não pudesse se lembrar o nome da cidade. _Creo es cerca de Chilecito_, havia dito.

Téo respondeu a Alexandre contando o que sabia. Era uma segunda pista; antes descobrisse o que María tinha a dizer.

No meio da tarde retomaram o caminho pelo deserto até alcançar a província de Catamarca. As montanhas os acompanhavam; sempre no horizonte, por todos os lados, fugidias. Parecia improvável que se pudesse encontrar alguém — quem quer que fosse — naquele lugar. Não havia onde se esconder.

Não havia onde procurar.

Pararam em Santa María no final do dia. O sol ainda flutuava por sobre as montanhas, insistente.

Era cidadezinha pacata de menos movimento turístico, ponto de passagem aos aventureiros da _Ruta 40_; lugar para abastecer o carro, fazer compras no mercado, abrigar-se da poeira.

O hostel havia sido recomendado por um dos estrangeiros com quem Gustavo havia conversado em Tafí del Valle. O recepcionista era um menino pálido e sorridente que os recebeu primeiro em inglês, depois em português e por fim — reagindo talvez à careta confusa de Téo — soltou um _bienvenidos_, abrindo os braços, como quem se desculpa pelo exagero.

— Fala português? — Gustavo perguntou.

— Sim, sim — arriscou. — Eu falo português muito bom.

O sotaque era terrível mas enorme o sorriso de moleque que aceitava o desafio. Téo apoiou-se de lado no canto do balcão. Viu que sobre a mesa da recepção alguém havia fixado junto ao modem a bandeirinha listrada com as cores do arco-íris. Sentia-se meio errado naquele lugar: o sorriso do recepcionista e as paredes coloridas. os quadros coloridos, a sala ao lado da recepção tão destoante: uns móveis antigos e dois jovens com pinta de mochileiros, cada um em seu laptop, jogados no sofá com a expressão sonolenta.

— Vocês têm vaga? — Gustavo perguntou.

— Dois pessoas, sim? Tem sim. Cama casal _o_ dois camas?

— Casal.

— _A ver..._ — E digitou algo no computador. — Tem com _baño_ privado _o compartido_.

— Pode ser o privado.

— _Dale_.

Golpeou a tecla _enter_ algumas vezes e virou-se para agarrar uma das chaves penduradas num painel da parede. Saiu à sala e gesticulou para que o seguissem. Téo demorou-se, sentindo-se ainda inadequado; quando alcançou o quarto no final do corredor já o recepcionista estava voltando.

Gustavo havia deixado a mala e a mochila no chão e se atirado de atravessado na cama, as costas no colchão e o braço cobrindo os olhos, sem descalçar o tênis. Téo entrou, fechou a porta atrás de si. O quarto era estreito e a cama estava encostada numa das paredes. Uma porta ao lado de uma estante de madeira dava passagem ao pequeno banheiro. Era o primeiro lugar em que perguntavam se queriam o quarto com cama de casal ou duas de solteiro; havia sido automático que lhes oferecessem antes o quarto com duas camas, e a Téo parecia suficiente.

— Era isso que te faltava? Uma cama grande?

Téo não calculou que lhe saísse a pergunta tão agressiva. Tirou do caminho a mala de Gustavo e a deixou junto das mochilas, ao pé da estante.

— Claramente — veio a resposta, sarcástica.

— Ou foi só pra ver se eu ia sair correndo?

Gustavo riu, de leve, sem se mover; ainda os olhos cobertos, o outro braço apoiado na barriga, metido por sob a camiseta enquanto os dedos passeavam distraídos pela cicatriz que lhe atravessava metade do abdômen. Téo entrou no banheiro para jogar água no rosto. Quando voltou viu que Gustavo tinha os olhos abertos e mirava o teto; procurasse no vazio algum conforto depois de tanta estrada, tanto o ruído do vento zunindo pela janela.

— Passei? — perguntou. — Fui aprovado?

— Parabéns — Gustavo murmurou.

O silêncio em suspenso duraria talvez um minuto inteiro; ou dois. Gustavo teimoso com o olhar fixo em qualquer ponto entre a lâmpada acesa e o encontro da parede com o teto. O rosto brilhava com o suor da viagem. Os óculos escuros inseparáveis ainda pendurados na gola da camiseta. Téo sabia que devia dizer algo

pedir desculpas;

sabia?

Foi Gustavo quem falou primeiro, baixando finalmente o olhar:

— Desculpa.

Téo manteve-se imóvel. Deixou crescer o silêncio, tão maior depois de horas dentro do carro com os ruídos da estrada.

— O que eu falei mais cedo — Gustavo disse. — Eu queria ajudar. Não ajudei.

_É o cansaço_, quis dizer. Não disse. Abriu a boca como quem vai continuar falando mas só fez suspirar e enfim sentar na lateral da cama para tirar os sapatos. Téo observou o movimento conhecido: como Gustavo desamarrava os cadarços devagar e descalçava um pé de cada vez, para depois deixar os tênis muito bem alinhados no canto da cama, os cadarços enfiados por dentro.

Viu também que ele hesitava: o tremor nas mãos — instabilidade momentânea. O gesto repetido de passar o polegar no anular da mão direita, buscando uma aliança que havia muito tempo não estava ali.

— Eu só queria dizer que — Gustavo continuou, e interrompeu-se como se para retomar o fôlego. — Só queria dizer que eu entendo.

Téo deu um passo para trás e apoiou o corpo no batente da porta do banheiro. Pensar que _não não não_ não era isso que devia ser aquela viagem, aquela investigação; não era para Gustavo estar ali, que precisasse dividir a atenção entre a busca pelo tio e os pedaços do relacionamento; que precisasse dar sentido ao relacionamento, chamar de relacionamento, fazer as pazes com o passado. Houvesse antes confiado no próprio julgamento dito que

não!

— E talvez eu tenha exagerado hoje de manhã — Gustavo continuou. — Mas é que.

Téo conteve o impulso de olhar o relógio. Devesse impedir que a conversa sequer começasse. Mirou no chão os próprios pés, o piso de ladrilho. Gustavo cruzou as pernas sobre a cama e apoiou as costas na parede.

— Aquela noite voltando da casa dos seus pais. Não sei; tenho estado meio ansioso. Meio inquieto. Fazia tempo que eu não.

Mais uma vez palavra engolida. Téo sentiu pesar o cansaço da viagem; tinha fome e queria sair dali, buscar uma lanchonete ou um mercado, um banco de praça iluminado para se ocupar com um livro e esquecer o dia. Adiantou-se para sentar ao pé da cama. Tirou o tênis e deixou-se cair de costas, recolhendo as pernas para cima do colchão; a cabeça ao lado do joelho de Gustavo. Fechou os olhos.

Sentiu a mão de Gustavo lhe acariciando a testa e os cabelos.

— A última crise que tive foi antes de sair do Rio — Gustavo disse.

Téo teve vontade de se afastar, mas permaneceu imóvel; os olhos fechados. Pensasse em qualquer partida entre Racing e Independiente quando ainda estava no colégio, no subúrbio de Buenos Aires; quando saía do estádio e ia arrumar briga com os torcedores do time adversário. Atirando latinha vazia por sobre a multidão. Tanta coragem inventada; tinha os amigos ao lado e sabia para onde correr.

Para onde ia correr?

María havia dado a Antonio um lugar para onde correr? Que também ela fosse mais uma ideia do que uma pessoa: uma vaga noção de liberdade para além da mesmice enlouquecedora da pampa argentina.

— Você não precisa — Téo disse, ainda os olhos fechados. — Não precisa se explicar.

Ou dizer: não sou a pessoa para ouvir. Não sou quem vai dizer que tudo bem, que está tudo bem; não agora. Não era isso que.

Gustavo teria soltado um riso nervoso abafado antes de continuar, como se falasse consigo; feito fosse possível articular a memória e as imagens e o ângulo exato da iluminação noturna os ruídos distantes da avenida uma lembrança que era tanto mais movimento e gestos e gritos e solidão.

Téo apertou os olhos. Conhecia a história e conhecia a conversa e só conseguia pensar que _não não não_ por favor;

por que não queria falar de crises, falar de Fabiano, falar da noite em que Gustavo voltava para casa com o namorado e foram abordados na rua vazia por um grupo de _playboys_ armados com facas e pedaços de pau; não bastasse a cicatriz onipresente para lembrar que Gustavo havia caído antes, semiconsciente, incapaz de proteger o namorado que era menor, mais fraco. Téo não queria falar de Fabiano e não queria ouvir de Fabiano; sentia-se todo errado competindo com o ex-namorado morto

sentia que não merecia todo aquele esforço

que não deveriam estar juntos

que nunca seria o suficiente.

## 10.

### Buenos Aires, BsAs

O sorriso de Gustavo desfez-se no momento em que os pais de Téo fecharam a porta do apartamento. Vestiu a jaqueta e tomou o rumo das escadas para evitar o elevador antigo de porta de ferro de correr que o havia deixado aturdido na subida. Téo foi atrás; aproveitando talvez o silêncio do hall para organizar os pensamentos.

Quisesse fazer uma brincadeira e _meu pai te irritou tanto assim com as conversas de pescaria_. Ou imaginar que algo a mãe lhe havia dito para aborrecê-lo ainda mais que.

As curvas da escadaria faziam girar as paredes. Havia bebido, quê; três ou quatro taças de vinho. Gustavo parecia menos afetado ou fingia muito bem o equilíbrio a cada degrau com a mão firme no corrimão. Alcançaram o térreo e passada a porta da frente Gustavo enfim parou, sentindo o vento frio no rosto; por deixar que Téo tomasse a dianteira e guiasse o caminho até a avenida ou onde fosse melhor para encontrar um táxi.

Téo tomou a esquerda e Gustavo seguiu. A rua mesmo era quase tão estreita quanto as calçadas. Àquela hora havia ainda o murmúrio das avenidas próximas, mas à parte um ou outro apressado encarando adiante os próprios passos não se via alma alguma.

— Não foi tão ruim assim — Téo disse, parando por um instante antes de cruzar a transversal.

Gustavo riu — um grunhido. Havia metido as mãos nos bolsos da jaqueta e encolhia-se embora incomodasse pouco o vento fresco daquele fim de primavera. O corpo estava quente o suficiente. Estivesse também ele organizando as ideias e a cabeça bagunçada pelo álcool. Téo pensou; pensaria: _foi péssimo, não?_

Tão pouco via Gustavo irritado sentia que era melhor dizer outra coisa.

— A última vez que estive aqui meu pai tinha cismado com essas coisas de rádio e eletrônica e dizia que era um sonho de menino aprender tudo aquilo — continuou. — Noutra que ia aprender a tocar flauta. Minha mãe fica doida.

— Não é um táxi ali?

— Aquele não. Vamos até a avenida.

Seguiram adiante. Gustavo manteve-se um passo atrás até metade do quarteirão; Téo deixou-se alcançar, diminuiu a velocidade e recebeu de Gustavo um olhar inquieto.

— Estou cansado, Téo. Por favor.

— Minha mãe te falou alguma coisa?

Gustavo fez uma careta. Teria tentado rir, mas engoliu a reação num suspiro e acenou com a cabeça para que continuassem caminhando. Incomodado talvez pela rua vazia àquela hora da noite. Tomaram outra vez o rumo da avenida, dessa vez lado a lado no passo lento de Téo.

— Pergunto porque.

— Eu sei bem por que você está perguntando.

Téo hesitou mas num gesto brusco Gustavo lhe tomou o braço para que continuasse andando.

— Eu conheço a cidade — Téo disse, livrando-se da mão de Gustavo. — Relaxa.

— Eu conhecia o Rio de Janeiro também.

— Do que você está —

— Téo, pelo amor de deus.

Gustavo tinha os olhos cansados, brilhando com a iluminação da rua — mas havia ali também um desespero que Téo não conhecia. Téo quis falar; abriu a boca e mudou de ideia, confuso. Acelerou o passo e seguiram em silêncio até alcançar a avenida, atravessar para o outro lado e tomar a esquerda. Fez parar um táxi e Gustavo se jogou no banco traseiro como quem escapa de perseguição.

Téo deu o endereço ao taxista e explicou qualquer coisa sobre o caminho. Olhou Gustavo ao seu lado e mediu as palavras, pesou as palavras, desistiu das palavras. Irritou-se. Que devesse a ele algum tipo de conforto mas.

— A gente já andou à noite em São Paulo — disse — não entendo o que.

— Agora não, Téo.

— Estava tudo bem. Tem dez anos meus pais moram aqui. Você acha que eu ia te.

— Agora não.

E fechou os olhos; esfregou o rosto antes de tornar a cruzar os braços. Deixasse outra vez o álcool ocupar as ideias e distrair os pensamentos. Respirando pesado como quem se esforça por controlar as batidas do coração. Téo chegou a abrir a boca para insistir que estava tudo bem, que a rua ficava vazia àquela hora e era normal, era assim mesmo;

estava tudo bem.

Desconfiou que só faria deixar Gustavo ainda mais aborrecido. Que o problema talvez não fosse só andar na rua depois da meia-noite. Apoiou incerto a mão sobre a perna de Gustavo. Tudo o que não sabia dizer.

Depois chegar na casa de Roberto e passar pela porta, acender a luz da sala. Gustavo ficou ainda parado na entrada, deixando que os olhos se acostumassem à luminosidade e ao silêncio da casa. O gato veio pelo corredor e parou por ali, encarando-os.

Téo quis dizer que _venha, vamos dormir_ mas foi só o gesto da mão nas costas de Gustavo e Gustavo esquivou-se, num susto. Ergueu os braços na frente do corpo como para se explicar e Téo percebeu que ele tremia.

— Você tem suas coisas, eu tenho as minhas — Gustavo falou, e a voz lhe saiu engasgada.

A expressão havia recuperado algo da fúria anterior. Encarou Téo por um instante medindo talvez o quanto tinha de disposição para conversar.

— Não foi por andar na rua à noite... não... não sei o que foi.

Seguiu para a cozinha e Téo foi atrás. Buscou um copo e a água na geladeira. Serviu-se, ainda trêmulo, e tomou um gole demorado. Téo pensou que era por isso não falavam nunca sobre Fabiano; nem tanto porque Gustavo evitasse o tema. Téo era quem se sentia desconfortável e não sabia o que fazer quando súbito Gustavo se transformava num menino frágil e inseguro. E mais que isso o quanto lhe gritava sua própria incapacidade de lidar com os problemas mais minúsculos —

a dificuldade de lidar com intimidade e cobranças familiares e tudo mais que lhe escapasse ao conforto de seu universo tão cuidadosamente construído;

enquanto Gustavo nem dois anos desde a tragédia com o namorado e seguia adiante

teimoso, corajoso.

— A gente pode...

Téo hesitou quando Gustavo ergueu o olhos para ele. Depois gesticulou na direção da sala e completou:

— A gente pode assistir a um filme.

Gustavo deixou escapar um riso nervoso, quase como um soluço. Súbito um sorriso meio débil, embora os olhos brilhassem com um tanto de lágrima contida. Esfregou os olhos com a manga da jaqueta.

— É sério — Téo insistiu. — Beto tem a coleção completa do Bogart; quer ver Casablanca? A gente bota o volume baixo. Eles não vão se incomodar.

— Casablanca.

— Não dá pra errar com Casablanca.

O riso por entre os dentes lhe saiu mais fácil dessa vez. Gustavo concordou com um gesto da cabeça. Tornou a encher o copo com água. Tremia, ainda. Um pouco menos. Apontou a sala com o queixo e Téo sorriu.

## 23.

### RN40, Catamarca

Era caminho sem volta. Que Gustavo tentasse se explicar e estava ansioso ou inquieto ou. Que o assombrassem fantasmas. Téo sabia: sentia a mudança; sentia no outro a paciência encurtada e tudo parecia passo em falso, um tropeço no rumo do vazio. As inseguranças transformadas num gesto meio trêmulo ao destrancar o carro e a dificuldade para encontrar o buraco da chave na ignição. Os silêncios tomavam lugar dos sorrisos e Téo queria dizer que voltasse logo a Buenos Aires, que sua folga terminaria de qualquer forma dali uma semana e a investigação seguiria adiante Argentina adentro.

Dizer

_não sirvo pra ser namorado de ninguém_

buscar a rodoviária de Santa María e desaparecer dentro de um ônibus

(para onde ia correr?)

Se era Gustavo o advogado que queria ser ator; era quem sabia se adaptar e mudar de estratégia e fazer que estava tudo bem. Téo, não. Havia terminado a faculdade de direito por pura teimosia e seu trabalho era quase sempre se esconder atrás de um livro, fazer-se desinteressado, fingir que não era com ele.

— Que te parece? — Gustavo enfim perguntou.

As árvores mais altas ficavam para trás com a última casa de cada povoado. Havia perdido a conta de quantos rios haviam cruzado atravessados por cima da rodovia, transformando o trecho num areal que cobria a pista — não fosse as placas, sequer saberiam que se tratava de rio. Por todos os lados aos pés das montanhas adivinhava-se na planície o deserto.

— O deserto?

— Seu tio.

Gustavo tinha expressão séria: os olhos escondidos por trás dos óculos escuros. Téo não teve coragem de ligar a música e o vento ruidoso parecia capaz de levantar o carro no primeiro deslize. A RN40 estendia-se adiante quase sempre em linha reta, infinita.

— Não sei.

— Te parece que essa mulher vai saber alguma coisa?

— Não sei se ela vai querer me contar o que sabe.

Por isso também não lhe havia escrito; preferia aparecer de surpresa, dar-lhe pouco tempo para inventar desculpas. Tinha uma vaga ideia de onde ela trabalhava, pelo pouco que Javier parecia saber. Belén era cidade pequena o suficiente para que a encontrasse antes, e faria enfim o que fazia melhor: seguir, observar, esperar.

— Qual seu plano?

Téo desviou a vista da rodovia adiante para observar a figura do motorista: Gustavo contra o sol oblíquo da manhã, ambas as mãos firmes no volante feito fosse preciso força para manter o carro na pista e impedir que saísse arrastado em direção ao terreno acidentado do deserto. A camisa de manga longa dobrada até pouco antes do cotovelo. Diminuiu a velocidade para atravessar mais um desses areais sob os quais a rodovia desaparecia, feita breve deserto, emergindo teimosa em seguida.

Também os amontoados de casas que surgiam de repente bordeando o percurso, sempre cercadas pelos ciprestes enfileirados que protegiam do vento: tão logo surgiam, ficavam para trás. Gustavo diminuía a velocidade, passavam talvez uma caminhonete ou uma moto e era como se houvessem atravessado vila fantasma.

Gustavo virou-se por um instante para olhar Téo, sobrancelhas erguidas por cima dos óculos escuros; como quem repete a pergunta, insiste por uma resposta.

— Falar com ela — Téo resmungou, contrariado.

— E?

Gustavo ainda o mirava de canto de olho. Téo sacudiu os ombros. Abriu o porta-luvas e moveu ali dentro o manual do carro, alguns folhetos da locadora; por algo com que ocupar as mãos. Tornou a fechar o compartimento e cruzou os braços. Olhou à direita a cadeia de montanhas que os acompanhava: as montanhas cheias de pontas que parecia se moviam com o carro; desenhava-se atrás ainda outro cordão montanhoso que se movia mais devagar, feito estivessem dançando.

— Não sei. Vou saber depois de falar com ela.

— Mas acha que ela pode não querer te contar o que.

— Não sei — e a voz saiu mais alta, atravessada pelo vento.

— São mais de trinta anos.

— E daí?

— Que ela ia querer esconder depois de trinta anos?

— Não sei.

A paisagem repetida; o número nos marcadores de quilometragem mal pareciam mudar: quatro mil cento e setenta e nove, quatro mil cento e setenta e oito, quatro mil cento e. O carro rodando em círculos

a conversa a investigação o relacionamento rodando em círculos.

Não havia para onde correr.

— Te pergunto isso porque.

— Que te faz pensar que eu sei o que estou fazendo?

Quanto mais precisava para deixar claro que não queria conversar? Se toda conversa era início de outra discussão aborrecida e remoer o passado e as crises e _um pouco ansioso_ porque a rua vazia na madrugada de Buenos Aires. Dizer que não queria mais pensar nos motivos do tio e no que fazia um homem adulto sair de casa e não voltar nunca mais, incapaz de se comunicar, fazer-se entender, deixar um bilhete

(que tanto custava uma mensagem?)

Ouviu que Gustavo deixava escapar um grunhido. Quanto mais? Téo moveu-se no banco, ajustou o cinto de segurança. Era uma espécie de claustrofobia: aquela vontade de sumir, deixar-se levar pelo vento; sentir-se tão terrivelmente amarrado

que o cinto de segurança fosse capaz de sufocá-lo.

— É o seu trabalho, não é?

Procurar gente desaparecida.

Meter-se pelo deserto em busca de pegadas.

Fez-se outra vez um silêncio, histérico, marcado pelas inconstâncias do asfalto maltratado e o vento

sempre o vento.

— Só estou tentando entender o que.

— Não tem nada pra entender.

— Só estou tentando conversar.

— Não tem nada pra conversar!

— A gente não precisa falar sobre.

— Para o carro — Téo interrompeu.

— Como?

— Encosta o carro, por favor.

— Está louco? Quer que eu pare de falar eu paro de falar; não vou parar a merda do carro.

Téo desviou o olhar. Faltava-lhe o espaço que impossível a noventa quilômetros por hora a dois mil metros de altitude dentro duma caixa metálica de três ou quatro metros quadrados deslizando deserto adentro; pensar talvez que a estrada não lhe servia e faltava-lhe mais que nada seu apartamento, os filmes repetidos e a música de sempre e o percurso de todos os dias e dizer que nos vemos amanhã hoje quero ficar sozinho.

Quatro mil cento e sessenta e quatro.

— Que você quer? — Gustavo insistiu.

— Que você pare o.

— Quer que eu fique quieto?

— Quero que.

— Que eu seja a porra do motorista?

— Foi você quem se ofereceu pra.

— Que eu finja que não te conheço?

Haviam passado mais um desses vestígios de povoado: os ciprestes que surgiam súbito tão verdes na paisagem árida depois de uma entrada de pedregulho. Quatro mil cento e sessenta e.

— Para o carro, Gustavo.

Havia solitária uma parada de ônibus: uma casinha de concreto pintada de vermelho logo depois de outra saída que parecia serpentear pela lateral da estrada rumo a lugar nenhum. Gustavo passou por ela, diminuiu a velocidade e desviou o carro para fora do asfalto, antes da mureta metálica que margeava uma curva aberta à direita. Adiante pela esquerda havia ainda uma ou duas casas, meia dúzia de ciprestes miúdos.

O céu tão azul duas ou três nuvens no horizonte.

O barulho do vento.

(Tempestade.)

Gustavo tirou os óculos escuros da cara e os atirou no painel.

— Você vai descer? É isso?

Quisesse talvez dizer que _só preciso um pouco de silêncio_ e.

— Eu prefiro fazer isso sozinho.

— O que você prefere fazer sozinho?

— Isso.

— Isso — Gustavo repetiu.

Eram os restos de paciência que escapavam pela fresta aberta da janela. Gustavo respirou fundo, fechou os olhos. Então súbito fez escapar a frustração contida num golpe violento contra o volante, com ambas as mãos. Os óculos escuros deslizaram na direção de Téo.

— Desce — Gustavo disse.

Cruzaram-se os olhares. Téo hesitou, encarando-o por um instante. Não saberia dizer quanto tempo; instante eterno. Quis ler a expressão de Gustavo mas não soube dizer se era raiva ou tristeza ou.

— Desce, Téo.

Outro instante. Gustavo desligou o motor e abriu a porta, desceu, bateu a porta com força. Fez a volta até o porta-malas, tirou dali a mochila de Téo e a jogou no chão de terra. Téo havia enfim descido também e só fez observar enquanto Gustavo abriu a porta do banco de trás para tirar dali a mochila menor e uma sacola com água e parte do lanche que haviam comprado antes de sair de Santa María.

O vento bagunçava os cabelos de Téo e ruidoso sacudia a sacola plástica feito quisesse tomá-la para si.

— Se é isso que você quer, quem sou eu pra.

E um gesto vago; aproximou-se de Téo mas só para fechar a porta do passageiro que havia ficado aberta. Apontou com a cabeça a parada de ônibus uns metros atrás. Nas construções que se adivinhavam antes da curva havia também uma caminhonete azul estacionada. Um homem havia parado ao lado dela e observava a cena, curioso talvez para saber se precisavam de ajuda.

Gustavo logo faria a volta pela frente do carro para assumir outra vez o banco do motorista. Téo olhou as mochilas no chão e ouviu a porta batendo, o ruído do motor, o som das rodas girando por sobre os pedregulhos do acostamento. O vento seco gritava e o sol forte ardia na pele. Ergueu a cabeça a tempo de ver o carro acelerando na curva, para depois desaparecer mais adiante atrás de uma variação do relevo.

## 35.

### Esquel, Chubut

Téo guardou os óculos escuros no bolso da mochila; já o sol metia-se por trás da cordilheira e pelo rio junto da estrada soprava uma brisa gelada. Vestiu a jaqueta e tornou a olhar adiante: para além da ponte seguia a estrada pela qual haviam chegado. De onde estava, ao seu lado, esticava-se uma rua de terra que mais para frente se curvava à direita, de volta ao povoado.

Por todos os lados, onipresentes, as montanhas. O sol iluminava ainda a face de uma delas, a leste, feita muralha dourada. Fosse talvez a vegetação mais esparsa no topo dos montes ou essa poeira cinzenta que cobria tudo ao redor: fazia parecer que estavam envoltos por camadas de teia de aranha, abandonados, esquecidos. Ruínas de um império derrubado.

A Patagônia tinha algo de estranhamente ancestral, gastado pelo tempo e pelo vento — não a paisagem estática e imponente daquele deserto aos pés da puna. Esquel, mais ainda: o céu do fim da tarde uma mistura de nuvens altas e baixas, deixando entrever nos espaços entre elas um azul vivo. O povoado era um pedaço de terra plana de avenidas largas: à parte a movimentação do festival no centro era tudo de um cinza sem graça, repetido e silencioso.

Pareciam dois universos: o cinza aborrecido das ruas e as cores da paisagem acima, a movimentação agressiva das nuvens.

Elisa havia escrito no dia anterior: “me ligue.” Talvez houvesse finalmente conversado com Gustavo. Téo não respondeu.

— Téo!

Violeta parou na esquina antes da pequena área gramada em que Téo havia encontrado um par de bancos de madeira e a vista da entrada do povoado. Téo virou o corpo e acenou. Ela se aproximou correndo, jogou-se ao lado dele no banco. Apoiou a cabeça em seu ombro e lhe tomou o braço.

— _Qué hacés? Te busqué por todo lado._

— _Mirando la tarde._

Havia passado o fim de semana empenhado na investigação. Por dizer a si mesmo que tentaria mais uma vez, só uma. Não arriscaria aquele salto no vazio em que se transformava a RN40 dali adiante, mais ao sul. Violeta o havia apresentado ao dono de uma oficina mecânica próxima à entrada — ele tinha também um restaurante onde muitos dos caminhoneiros costumavam se reunir.

O homem havia reconhecido a figura da fotografia: sim, claro, Antonio Gonzalez. _El tucumano porteño de Catamarca_, disse, rindo. _Toño del flete_. Sabia que não tinha residência fixa em nenhum lugar — tinha talvez algumas residências, alguns lugares para encostar a cabeça num travesseiro e descansar. Mas não tinha casa nem família nem destino definitivo. Chegava a cruzar até Santa Cruz e ficar meses do outro lado; outra vezes seguia ao norte, para além de Bariloche; às vezes também seguia ao Chile e demorava-se pelos lados do Pacífico. Ia aonde o levasse o trabalho e ficava quando convinha. Depois, voltava.

_Se deja llevar_, o homem explicou, satisfeito — com algo de inveja nos olhos. Era o tanto que sabia. Disse a Téo que falasse com a dona da pensão onde Antonio costumava ficar, mas também ela não sabia como entrar em contato. Tinha o mesmo número de telefone que Téo já havia tentado antes.

— _Se fue hace como una semana_ — a mulher havia dito. — _Al sur, creo._

Dali onde estava, todo lado era sul.

Violeta o havia acompanhado nos horários de menos movimento de turistas. Deixava uma amiga cuidando da mesa e das crianças e aparecia inesperada com mais alguém que houvesse conhecido Antonio. As histórias se repetiam. Téo havia andado o povoado inteiro feito fosse possível topar numa esquina com a caminhonete verde.

Já ia embora a tarde de domingo. As ruas estavam esvaziadas com o fim do evento no início da tarde e Téo não sabia explicar o desespero silencioso que lhe tomava o peito. Sentia-o reprisado: a mesmo ímpeto de fugir que havia sentido algumas semanas antes.

Precisava sair dali antes que enlouquecesse.

— _Vas a desistir?_ — Violeta perguntou, quase num sussurro, sem tirar os olhos da montanha iluminada no horizonte.

Téo respirou fundo. Que mais ia fazer? Também ele precisava retomar sua vida: explicar-se a Roberto e voltar a São Paulo, recolher os pedaços de relacionamento que ainda restassem pelo caminho, decidir o que fazer com eles.

Falar com Pablo, talvez.

Se fosse ainda possível falar com Pablo.

— _Si querés estar solo te dejo_ — ela ofereceu.

— _Está bien. Estuve suficiente solo ya._

Virou-se de lado. Violeta havia erguido a cabeça e o mirava com um sorriso incerto. Os olhos faziam-se um cinza azulado, feito refletissem a cor do céu. Téo já não sabia em que pensava. Tantas as perguntas e as respostas repetidas: mostrar a fotografia e ouvir e fazer-se interessado e caminhar em círculos. Deixar-se arrastar adiante, cada vez mais ao sul. Violeta esperava;

algo.

Téo abaixou a cabeça e deixou escapar um riso envergonhado. Violeta desviou o olhar, sem jeito. Ia dizer algo; abriu a boca, pensou duas vezes, desistiu. Soltou do braço de Téo e uniu as mãos sobre as pernas, e foi a vez de Téo passar o braço por seus ombros num abraço de lado.

— _Me preguntaste que me pasó_ — Téo disse.

Violeta afirmou com a cabeça, olhando ainda os próprios pés.

— _Mi..._ — hesitou; sempre o peso das palavras. — _Mi compañero... me dejó._

Violeta endireitou-se no banco para encará-lo, livrando-se do abraço ao girar o corpo em sua direção. A expressão era algo de surpresa confusa.

— _Tu compañero_ — ela repetiu.

Téo confirmou com a cabeça.

— _Bueno_ — continuou, e soltou um riso nervoso. — _Literalmente me dejó. En la autopista._

— _En serio?_

A resposta só o gesto repetido da cabeça, o sorriso meio constrangido. Violeta lhe tomou a mão fria e a apertou; ainda o encarava, esperando dele o resto da história.

— _Me dejó en la cuarenta entre Santa María y Belén._

— _En el medio de la nada? Qué hiciste?_

— _Esperé el bus._

E ergueu os ombros como se por dizer que não havia muito mais que pudesse ter feito. Violeta sacudiu a cabeça.

— _No; qué pasó? Qué hiciste para que te dejara?_

Téo fez uma careta. Passou a mão nos cabelos e esfregou o princípio de barba que coçava no queixo. Era a pergunta que restava; não? Que você fez, Téo? Olhou Violeta e queria poder dizer que não havia feito nada, que havia sido um mal entendido; que Gustavo havia acordado de mau humor. Que lhe havia ocorrido um lapso de claustrofobia momentânea e a única saída havia sido pedir para parar o carro e.

— _Qué sé yo_ — disse, enfim. — _Lo merecí, creo._

— _Fuiste un pelotudo._

— _Un grandísimo pelotudo, sí_ — Téo riu.

Ela riu também, e Téo tornou a passar o braço em seus ombros; ela retribuiu o abraço, apoiou nele a cabeça. Estiveram calados um instante, observando enquanto a montanha adiante mudava de cor e as nuvens no céu indiferentes continuavam suas batalhas. A lua crescente surgiu brilhante por trás de uma delas e parecia quase absurdo que pudesse estar ali, obstinada por trás da guerra.

Violeta quis saber as horas; precisava buscar as crianças que brincavam com os filhos de outra amiga, fazer o jantar, dormir cedo. Havia conseguido carona de volta a El Bolsón para o dia seguinte, pela manhã, e ia embora antes das seis.

Ficaria atenta, disse. Téo tinha seu telefone, estavam em contato; ela avisaria se aparecesse no povoado o tio desaparecido. Já por toda a região a gente sabia que um sobrinho de Toño o procurava. Era talvez o mais próximo que podia chegar de uma investigação concluída em situação como aquela.

— _Igual vuelvo a mi primo sin respuesta_ — Téo disse.

— _Y si no hay respuesta?_

Téo virou a cabeça para olhá-la. Violeta desviou a vista da lua e sorriu para ele.

— _No sé_ — e ergueu as sobrancelhas como quem deixa no ar uma pergunta. — _Vos bajaste de un auto en la autopista, no? En el desierto._

Por dizer, talvez: uma escolha ruim. Um equívoco.

Assim começava.

Téo pensou em Gustavo: a caminhada apressada até a avenida depois do jantar na casa dos pais; a trégua nas serras de Córdoba e tomar sol na beira do rio; a estrada.

Gustavo de volta em São Paulo.

(E se não há resposta?)

Violeta riu consigo antes de endireitar-se no banco. Levantou-se e abriu os braços para um abraço de despedida. Téo pôs-se de pé e ajustou a jaqueta no corpo. Era estranho despedir-se: estar sozinho outra vez. Ele que tanto buscava a solidão sentia-se de repente cansado do silêncio; cansado da aridez e do vento e da poeira cinzenta que vinha do sul e se acumulava toda naquela curva do percurso.

O silêncio do sul era ensurdecedor.

Violeta percebeu a hesitação e lhe tomou as mãos.

— _Sabés el más lindo de la amistad de mochilero, Téo?_

— _No?_

— _Dura lo tanto exacto que tiene que durar._

Abraçaram-se e despediram-se. Violeta lhe beijou o rosto e se afastou, parando mais uma vez na esquina antes de continuar:

— _Volvé a tu casa. Mientras sepas donde está._

## 24.

### RN40, Catamarca

Era ainda o começo da viagem quando desceu a senhora que ocupava um dos assentos a sua frente na primeira fileira. O ônibus seguia quase vazio. Vez ou outra parava no meio do caminho no meio da pista no meio do nada e aparecia alguém pela lateral da estrada como se saltando por detrás de — quê?

De onde surgiam aquelas pessoas?

Tanto quanto ele teria surgido em uma parada remota antes de uma curva, próxima de um amontoado de casas silenciosas. Quase três horas esperando impaciente sob o sol, metendo-se vez ou outra na parte coberta da pequena construção para se proteger do vento e da poeira.

Téo aproveitou para passar ao banco vazio adiante, junto ao corredor, porque dali via melhor a estrada: a imensidão entre as montanhas e as cores pálidas do deserto. Pudesse fazer caber na cabeça aquele tanto de espaço desocupado: o quanto alcançava a vista e nenhuma alma; humano, bicho, nada.

O motorista tinha uma das bochechas saltadas pelas folhas de coca. Uns óculos escuros enormes e um sorriso amigável. A moça da fileira ao lado esticou-se para uma foto e o motorista lhe disse que sentasse ali no degrau que as fotos lhe sairiam melhores; botou sobre o chão sujo uma bolsa de couro vazia que lhe podia servir de assento. Ela aceitou.

Téo desconfiou que fosse brasileira; turista, mochileira. Quem mais falava era o motorista, que de repente encontrava companhia para a viagem longa e a paisagem monótona. Falava: quase impossível compreendê-lo com a boca cheia de coca e as palavras engolidas do sotaque do norte. Era também provável que nem a moça entendesse o que dizia: concordava com a cabeça, tirava mais uma foto, fazia uma pergunta genérica.

Téo pensou em seu tio: o caminhão atravessando a puna de Catamarca no rumo do Chile. Que houvesse em algum momento encontrado uma turista sozinha para lhe escutar as aventuras. Vê: deixei a família na capital e precisei tomar o rumo do norte, voltar à casa, buscar um amor perdido. _Mas a família?_ Estão melhor sem mim, não há dúvida. Que tipo de pai ou marido eu poderia ser;

eu

capaz de abandoná-los assim

que pai é esse que pensa em abandonar os filhos?

O motorista foi murmurando comentários sobre os lugares, os povoadinhos, a necessidade de chegar no horário e ter que diminuir a velocidade para não chegar muito cedo no povoado seguinte. Contou que tinha uma namorada em Salta e outra em Santa María, e tinha também uma em Belén mas ela havia descoberto sobre a existência das outras. Que tinha sido casado nove anos e que a ex-mulher, a moça que vendia as passagens em Santa María, tinha dele um ódio tremendo. Justificava-se: estava sempre na estrada, não parava mais de uma semana em uma só cidade e sentia-se sozinho, gostava de companhia.

Téo tirou do bolso o telefone celular. A bateria estava no fim e ele não tinha sinal. Nenhuma mensagem. A paisagem desértica os acompanhava, e as montanhas mudavam de cara e de formação geológica, ganhavam outras cores e um aspecto lunar; umas mesetas que formavam uma espécie de sequência de tobogãs. Na lateral da estrada eram os arbustos e algarrobos baixinhos e nada, ninguém. Uma imensidão de espaço vazio e o solo arenoso e cinzento pintado pela vegetação espinhosa.

Era para ser outra coisa

a liberdade da solidão;

meter-se no cinema quando lhe dava na telha, desligar o telefone quando não queria saber de trabalho, esquecer que existia internet quando não tinha paciência para os e-mails da mãe e do primo Pablo.

Elisa o conhecia e o compreendia e o aceitava. Seus silêncios.

Gustavo também? Quando antes lhe havia exigido mais do que

_estou vivo_

_estou ocupado_

_hoje não;_

que reconhecesse sua existência?

Estou aqui.

Era para ser outra coisa; não a irritação com a conversa furada do motorista do ônibus e querer que terminasse logo aquela paisagem árida e aparecesse cidade, mais de meia dúzia de casas e ciprestes, sinal de vida na tela do telefone. Mensagem? Que talvez devesse escrever algo e pedir desculpas mas

se nem sabia como

por que

que tinha feito de errado.

Atordoado com o barulho do vento e a voz de Gustavo repetindo insistentes as mesmas perguntas que já ecoavam dentro de sua cabeça. Todas as conversas que não queria ter.

Era isso que queria?;

descer do carro em plena pista.

Se precisava apenas um pouco de silêncio.

Chegou a desbloquear a tela do telefone e buscar a última mensagem de Gustavo, de quando ainda estavam em Tucumán: “está tudo bem?”

Porque sim: custava-lhe a mensagem. Custava a ideia de anunciar seus passos e suas decisões e seus planos. Se mesmo Elisa depois de tantos anos havia aprendido a lidar com sua inconstância e a confiar nele: fazia seu trabalho dentro do prazo combinado, e bastava.

Guardou o telefone na mochila. Quis se convencer de que sim — estava tudo bem; que Gustavo alcançaria Belén e se encontrariam em Belén e desaparecida a irritação poderiam conversar sobre qualquer assunto desimportante, e seria o tempo que Gustavo tomasse o rumo de volta a Buenos Aires para não perder o voo marcado.

mas ouvia eram as palavras feito fosse um filme ruim

(que você quer?)

o vento fazendo tremer a janela meio aberta do carro

(que eu finja que não te conheço?)

e parecia tão mais complicado desatar aquele nó; tão absurdo que houvesse permitido que o nó sequer fosse atado e tão ridículo que fosse incapaz de conversar e precisasse levantar a voz, parar o carro, descer do carro.

Dizer a si mesmo que doía talvez o ego

(desce, Téo)

mas era melhor

claro que era.

Esgotava-se a energia para pensar no tio e nos motivos do tio. Imaginá-lo como motorista de ônibus aborrecido com o trânsito da cidade grande, os atrasos, os semáforos. Antonio com a bochecha saltada pelas folhas de coca atrás do volante do caminhão cruzando o deserto mais uma vez. Houvesse ido embora para retomar a busca impossível por um vago ideal de liberdade e talvez se arrependesse mas — que se pode fazer?

Adiante o asfalto chamava e as montanhas escondiam ainda outras montanhas, cada vez mais altas.

A montanha alta que se aproximava ao fundo junto de umas nuvens escuras. O motorista indicou que ali atrás estava Belén e quem sabe menos calor, embora o termômetro do ônibus marcasse 36 graus do lado de fora. O ônibus meteu-se por um trecho sinuoso em velocidade reduzida. Téo ouviu que a turista perguntava quanto tempo até a cidade mas não pode entender a resposta do motorista. Fechou os olhos e acomodou-se como podia ao espaço curto do banco. Sentia-se tão cansado precisava talvez apenas isso:

dormir

uma noite inteira

sozinho.

## 36.

### Esquel, Chubut

Tinha sobre a cama a mochila aberta e algumas roupas espalhadas em volta; a câmera fotográfica que havia usado tão pouco e os livros emprestados pela mãe. Sentado no chão com as costas na parede. O telefone na mão e os óculos de leitura escorregando pelo nariz depois de tantas vezes que havia começado a digitar, desistido, desviado a vista adiante.

Havia escrito a Elisa que “estou voltando a Buenos Aires”. Queria escrever ao primo e finalmente relatar a última semana mas tinha na tela mais uma vez a última mensagem de Gustavo que insistente “está tudo bem?” Devesse responder — ali quase três semanas depois e pensava que já era talvez o momento para responder mas;

se não sabia ainda o que dizer.

Três semanas depois sete meses depois — ainda não sabia o que dizer. Porque desde o começo previa o desastre; previa a queda. Desde o começo sete meses antes teve certeza de que

não ia durar

não era possível.

Talvez fosse aquele o tempo necessário para que também Gustavo chegasse à mesma conclusão e estava — mas isso Téo não pensaria — ainda o ego ofendido

(desce, Téo)

o ego dolorido.

Porque tinha razão, mas.

A que custo?

E a que custo seguia adiante embora soubesse que precisava voltar

se voltar era também encarar de frente todas elas as escolhas ruins

os equívocos todos;

e voltar era exatamente o que o tio nunca teve coragem de fazer.

O vento sacudia vez ou outra a janela do quarto e Téo adivinhava o frio que fazia do lado de fora. Pleno novembro, pensaria — a noite gelada. Como se a vida em São Paulo a vida em Buenos Aires a vida toda que conhecia fosse parte de outro universo. Parecia tão fácil deixar-se perder no rumo do fim do mundo.

Respirou fundo. Tirou os óculos da cara e jogou o telefone na cama, junto do resto da bagunça. Levantou-se para continuar a guardar as roupas de volta na mochila maior, as miudezas na mochila pequena. Deixou separada uma camiseta para a viagem. Seguiria à rodoviária na manhã seguinte e dali traçaria o percurso mais conveniente, direto à capital federal. No caminho podia pedir à mãe que lhe providenciasse a passagem de avião de volta a São Paulo, que ela tinha os contatos certos para organizar aquele tipo de coisa.

A Roberto diria o que havia descoberto, deixaria com ele os contatos todos. Era o melhor que podia fazer. Mais de um mês perseguindo um fantasma. Roberto queria saber onde estava o pai; ei-lo: Antonio rodava a Patagônia nos arredores da _Ruta 40_, sozinho, sem parar, numa caminhonete verde.

Ouviu apitar o telefone. Leu a mensagem de Elisa: “O que aconteceu na estrada?” — insistente. Havia conversado com Gustavo. Téo interrompeu a arrumação com um suspiro aborrecido. Jogou-se deitado na cama por cima da desordem para digitar apressado a resposta devida: “O que ele te contou é o que aconteceu. Eu sou egoísta e eu me distraio e eu não sirvo pra essas coisas. Você sabia disso, eu sabia disso, ele sabia disso. Fim.”

Atirou o telefone no fundo da mochila pequena, irritado. Ouviu um instante depois que ele apitava mais uma vez e deixou escapar um grunhido. Não tenho mais idade para isso, pensou.

Alguém bateu na porta.

Téo levantou-se. Abriu a porta para a figura conhecida de uma das funcionárias da hospedagem.

— _Perdón. Hay una persona que quiere verlo._

A afirmação quase feita pergunta. Téo pensou primeiro que podia ser Violeta — era ainda o domingo e ela só ia embora no dia seguinte. Depois tomou-lhe um calafrio. Agora?, pensou. Agora que decido ir embora, agora que decido esquecer essa merda de investigação — agora me aparece mais uma pista? Mais um pedaço de informação pela metade? Agradeceu a moça e disse que já estava indo. Voltou para buscar a jaqueta e calçar o tênis. Adiantou-se em seguida pelo corredor até a entrada da hospedagem.

Não era Violeta.

Tampouco Antonio, súbito trazido pelo vento noturno.

Téo reconheceu o rapaz de uma de suas voltas pelo povoado. Não havia conversado com ele. Teria no máximo vinte e cinco anos e era bastante branco, os cabelos dum castanho meio avermelhado.

— _Sos Juan, no?_ — o rapaz perguntou.

— _Juan Teodoro; Téo está bien._

— _Buscás al Antonio._

Era menos uma pergunta e mais uma constatação. Téo franziu a testa; afirmou com a cabeça, devagar. O outro continuava parado próximo à porta de entrada, com as mãos nos bolsos do casaco. Olhou em volta, desconfiado. Téo gesticulou na direção da porta e passou por ele, como para pedir que o seguisse. Sentou-se adiante no meio-fio e esperou que o rapaz fizesse o mesmo.

O céu estava limpo — desaparecidas as nuvens que lutavam furiosas no final do dia, algumas horas antes. A lua crescente pairava solitária a oeste, fazendo desaparecer as estrelas ao seu redor. O vento gelado.

— _Qué es lo que pasa, chico?_ — Téo perguntou.

— _Por qué buscás al Antonio?_

Pergunta devolvida.

— _Lo conocés, entonces?_ — Téo insistiu.

— _Por qué lo buscás?_

— _Soy su sobrino._

— _Toño no tiene hermanos._

— _Soy sobrino de su ex-mujer._

— _De Buenos Aires?_

— _Sí._

— _Tenés un acento raro._

— _Soy brasilero. Che, escuchame: vos qué querés? Quién sos?_

— _No soy de acá_ — ele respondeu, depois de um instante de silêncio. — _Vivo en otro pueblo acá cerca._

— _Bueno._

Téo não sabia se esperava, se insistia com as perguntas, se levantava e voltava ao calor da hospedagem e se ocupava com a mochila. Não havia jantado, mas tampouco tinha fome. Tantas as vezes nos últimos dias havia sido Violeta lembrando-o de comer — como também em São Paulo era Elisa quem o interrompia no meio do dia para levar um sanduíche ou brigar com ele para que se alimentasse ou

Gustavo que inesperado aparecia e _vamos ali naquela padaria tenho meia hora para engolir um misto-quente_.

— _Llegué hace poco_ — o rapaz continuou.

Téo conteve a vontade de revirar os olhos.

— _Me dijieron que estabas buscando al Antonio._

— _Y lo conocés, entonces?_ — Téo repetiu.

— _Por qué lo busca su ex-mujer?_

— _Ella no lo busca._

— _Entonces que querés vos?_

— _Yo, nada_ — Téo sacudiu a cabeça, irritado. — _Mi primo; el hijo más pequeño del Antonio. Él lo busca. Yo le estoy ayudando. Qué te parece? Que yo iba a preguntar en el pueblo a la gente toda si tuviera algo que esconder?_

A resposta lhe saiu agressiva. O rapaz o encarou por um momento com algo de fúria ofendida no olhar. Levantou-se num pulo, meteu as mãos no bolso do casaco e saiu apressado. Téo pôs-se de pé.

— Ei!

Saiu atrás dele, mas o rapaz apertou o passo. Quando percebeu que também Téo fazia o mesmo, acelerou ainda mais; dobrou a esquina, correndo.

Téo correu atrás; gritou para que esperasse, mas o outro só fez correr mais rápido; tomou a direita, algumas quadras adiante. Quando Téo alcançou a esquina, não viu alma viva. A rua estava vazia como se sempre houvesse estado vazia. Como se também ali o vento sul houvesse passado para depositar a poeira cinzenta sobre qualquer pegada que pudesse ter existido.

— Merda — Téo murmurou.

Agora essa.

(_Volvé a tu casa, Téo._)

Perderia ainda algum tempo na esquina, olhando a rua vazia, recuperando o fôlego. Um casal de turistas passou por ele conversando em inglês.

Tirou a jaqueta e sentiu o ar da noite esfriar os braços no contato com o suor. De volta à hospedagem, encontrou a funcionária postada ao lado da porta com a expressão preocupada. Quis saber se estava tudo bem — certo o havia visto saindo em disparada atrás do outro. Téo encurtou a conversa: disse que sim, estava tudo bem e _buena noche_. Seguiu pelo corredor, mas a moça o chamou outra vez.

— _Su amiga le dejó una cosa_ — ela disse.

Estendeu a ele um saco pequeno de papel pardo. Explicou que Violeta havia passado na hospedagem pouco antes do rapaz que o estivera procurando. Téo agradeceu, ainda um pouco atrapalhado, e voltou ao quarto.

Atirou a jaqueta por sobre as mochilas. Jogou-se na cama outra vez e chutou longe os sapatos. Pensou em sair para comprar uma cerveja mas sabia que melhor seria tomar banho e dormir, acordar o mais cedo que conseguisse e desaparecer daquele lugar. Ao inferno com a Patagônia e a estrada. Abriu o pacote e fez deslizar de dentro dele duas pulseiras de macramé como as que Violeta fazia. Eram parecidas: uma delas alternava uma parte em branco e azul celeste e outra em verde e amarelo, em linhas diagonais; a outra tinha as mesmas cores, mudava só o desenho das linhas.

“Una es para vos,” lia-se em um pedaço de papel dobrado. A letra era redondinha, como a letra de uma adolescente. “La otra, quizás, para tu compañero. Lo vas a saber. Buen viaje, mi amigo detective.”

## 11.

### Avellaneda, BsAs

Téo ouviu Pablo gritar _tarde!_ em resposta a uma pergunta que não conseguiu entender. A porta se abriu num movimento brusco e por trás dela estava o primo com um sorriso cansado, meio forçado. Tinha os óculos tortos e os cabelos crescidos o suficiente para que já pudesse prendê-los atrás com um elástico. Téo devolveu o sorriso. Veio outra vez a voz da esposa de dentro do apartamento pedindo que não esquecesse de comprar pão.

Uma menina apareceu atrás dele: era figurinha copiada do pai quando criança. Queixo esfolado, lábio inchado e um bandeide próximo ao olho. Acenou para Téo com um sorriso largo.

— _Juan, Juan! Hola! A donde van?_

Téo ia responder. Dizer que meu deus você não para de crescer e essas coisas que os adultos dizem sempre para as crianças quando não sabem o que mais poderiam dizer. Pablo o interrompeu com um gesto e virou-se para a menina:

— _Vos andá a hacer tus tareas_ — e apontou na direção do corredor à direita.

— _Pero vine a saludar el primo._

— _No vas a saludar a nadie. Marchate._

A menina resmungou alguma coisa antes de se meter pelo corredor e sumir da vista. Pablo esperou que parassem o ruído dos passos. Olhou Téo outra vez.

— _Que tal, primo?_ — disse, burocrático, e cumprimentou Téo com um beijo no rosto antes de fechar a porta atrás de si.

Téo apertou os lábios para esconder a vontade de rir. Gesticulou na direção da escadaria e desceram em silêncio até a porta da rua. Passavam de quatro da tarde e o sábado seguia ensolarado. Pablo esticou as costas ao alcançar a calçada, fazendo escapar um suspiro aborrecido. Apontou na direção da avenida. Pablo morava ainda no mesmo bairro em que Téo havia passado os anos do colégio, em Avellaneda. Não gostava de Buenos Aires, dizia, e fazia-se desentendido quando Beto insistia que era tudo a mesma coisa.

— _Cómo están las chicas?_

Pablo soltou um riso debochado que lhe saiu mais como um grunhido. Pararam para atravessar a rua.

— _Sara está adolescente y Emi cree que está también_ — disse.

— _Está con cuántos años Emi?_

— _Once._

Cruzaram a avenida. Pablo meteu as mãos nos bolsos da bermuda e mais uma vez indicou com a cabeça a direção que iam tomar. Téo já sabia aonde iam — eram três ou quatro os lugares a que ia com o primo, e cada um tinha ocasião adequada.

— _Igual que vos entonces_ — Téo disse.

Pablo riu; a expressão súbito transformada, como se desaparecida a irritação de antes. Ergueu o olhar para o céu claro e o topo das construções, respirando devagar o ar abafado da tarde. Enfim tomou Téo pelo cotovelo para fazê-lo parar. Encararam-se de frente.

— _Sos un hijo de puta, sabés?_

Téo abriu os braços, um pouco como quem pede desculpas.

— _Igual vine a verte._

— _No me vas a convencer de nada._

— _Nunca pude convencerte de nada, Pablito._

Ainda de Pablo o olhar desconfiado. Téo havia se preparado para o pior de seus humores — e conhecia seus piores humores. Sabia também o quanto o primo gostava dele, e que de qualquer forma conversariam; Pablo era difícil mas era também previsível, e o mau humor às vezes sumia com a mesma rapidez com que havia surgido.

Retomaram a caminhada.

Téo sabia que não era a mesma coisa: não era a indignação com um árbitro de futebol ou um dirigente do time. Pablo aborrecia-se fácil com os menores dos inconvenientes, mas aquele não era inconveniente menor. Téo sabia também que Pablo tinha algo de razão; que a empreitada era perda de tempo, que o paradeiro daquele homem não lhes dizia respeito.

— _Viste el último partido?_ — Pablo perguntou.

— _No. Llegué hace dos días._

— _No perdiste mucho. Estaba dormido el ataque._

E deu de recontar a partida, no tom aborrecido que Téo conhecia de quando o time não conseguia vitória. O Racing havia empatado em um a um contra o Independiente, o que não era de todo ruim, mas. À parte que o Boca Juniors continuava no primeiro lugar da tabela quase sem fazer esforço. Alcançaram um café restaurante numa esquina, com mesas alinhadas na parede de fora, na calçada. Entraram e ocuparam uma das mesas do lado de dentro, junto à janela.

— _Te veo cansado, primo_ — Téo disse, depois que pediram a cerveja.

— _Muy perspicaz, Sherlock._

E esticou as costas mais uma vez antes de jogar o corpo para trás e fazer espaço entre a cadeira e a mesa para conseguir cruzar a perna. Disse que um dos colegas se havia demitido e o chefe achou mais conveniente distribuir o trabalho em vez de contratar substituto. A filha mais velha tinha arrumado namorado e queria ficar fora até tarde, e discutia com a mãe, gritava com ele, batia a porta do quarto. A pequena se fazia muito crescida e só queria estar na rua andando de skate com os amigos, arrumava briga na escola feito fosse moleque e era já a segunda vez que tinha voltado com a cara machucada e uma advertência.

— _Igual que vos entonces_ — Téo repetiu.

— _Dale_ — Pablo riu, deixando escapar algo da tensão. — _La idea era que la hija me saliera mejor que el padre, no?_

A garçonete trouxe a cerveja e brindaram calados. Téo olhou ao redor o espaço conhecido. Era sempre Pablo quem mais falava: sobre futebol e sobre trabalho, sobre algum absurdo que havia visto na televisão. O diálogo era mais fácil com Roberto. Com Pablo era principalmente a companhia para o futebol, a euforia compartilhada. Mesmo os irmãos conversavam pouco para além das eventuais obrigações familiares. _Perda de tempo_, Roberto havia resmungado quando Téo disse que ia encontrar o outro primo. Depois conformou-se, disse que o levava de carro até Avellaneda e depois ia com Gustavo ao Malba.

— _Y Raquel?_

— _Si no me tira pronto por la ventana capaz que la tiro yo._

— _Un poco dramático._

Pablo deu de ombros. Disse que queria ver você no meu lugar, com esse trabalho, com essas crianças. Você que continua se fazendo tonto e eternamente solteiro, ou mais esperto que eu e não se deixando agarrar. Téo pensou em Gustavo quando reclamava das conversas esquisitas de homem hétero e teve vontade de rir, mas Pablo estava sério: sempre Pablo levando a vida muito a sério, o casamento as filhas o trabalho o futebol os problemas todos

a curiosidade do irmão mais novo a respeito do paradeiro do pai — muito a sério.

Mais de trinta anos passados e _espero que esté muerto el hijo de puta este_.

Que não fosse possível meio termo entre o peso da responsabilidade e a fuga. Téo pensou em Roberto que se equilibrava o quanto podia entre as carreiras de artista plástico e professor — o sonho e a obrigação. Mas vivia os dois pela metade? Gustavo que havia desistido do teatro para se concentrar na faculdade de direito porque.

Ou sua mãe que saiu do país interrompeu os estudos arrumou casamento casa filho vida de cidade pequena. A tia Anita que havia desistido da carreia acadêmica e fez-se professora por

escolhas?

Havia algo — um raciocínio mal formado, uma lógica incompleta. Téo não sabia por onde começar a organizar as ideias. Seu pai e Roberto que insistiam Pablo é assim _meio exagerado meio dramático_; ou a tia que nem mais dizia nada só virava os olhos e mudava de assunto, para não se comprometer. Porque Pablo fosse incapaz de dar um passo atrás, criar distância entre ele e as crises todas — respirar um pouco antes de mergulhar de cabeça.

Pablo que sequer havia desistido de coisa alguma ao escolher a vida de contador e escritório e filhas. O menino que Téo adolescente arrastava consigo aos jogos de futebol junto dos amigos e que carregava nas costas quando se complicava o tumulto na saída do estádio.

— _Vos?_ — Pablo perguntou, erguendo as sobrancelhas. — _Seguís solo destruyendo matrimonios?_

— _Uno tiene que hacer lo que hace mejor._

Pablo cruzou os braços; de repente a cara fechada. Téo percebeu a tempestade que se anunciava e tomou outro gole da cerveja.

— _Y no tenés trabajo más importante que este ahora?_ — perguntou.

Téo pousou o copo na mesa e desviou o olhar para a rua: a gente que passava devagar no ritmo do sábado e a movimentação dos carros no cruzamento. Por buscar as palavras; não tinha muito que dizer e sabia que jamais convenceria o primo de coisa nenhuma. Roberto havia tentado — e falhado.

Mas Pablo era como um irmão mais novo e um pouco Téo sentia-se responsável. Sentia que lhe devia explicação. Já não bastasse preocupar-se com as filhas e o trabalho e os desentendimentos com a esposa. Ainda isso: o irmão mais novo decidia revirar o passado morto

(mais de trinta anos)

o passado que Pablo tanto se esforçava para manter enterrado.

## 25.

### Belén, Catamarca

Oito da noite e ainda o calor do meio dia. Começava no povoado um murmúrio de crianças saindo à rua para brincar, porque enfim amansava o mormaço do fim de tarde. A praça era mais agradável sem o sol forte. Téo tinha o telefone nas mãos

esperando.

Olhou o céu porque a umidade lhe parecia prenúncio de chuva, mas não viu muitas nuvens. Algumas estrelas. Era estranho, ainda, ver o céu noturno para além daquele alaranjado de São Paulo ou Buenos Aires. Lembrar que havia lua, que as estrelas continuavam ali como nas noites de sua infância no interior paulista.

Porque era; aquele povoado: Belén, província de Catamarca — e lembrava mais de sua cidade natal, tão distante no tempo mais ainda do que no espaço. A praça, a igreja, as noites quentes e a gente que passava pelo centro sem muita pressa na volta do trabalho. Os adolescentes em grupos pequenos amontoados nos bancos. Parecia absurdo que estivesse ali, e mais ainda que se sentisse tão em casa naquele lugar desconhecido aos pés dos Andes.

Que a qualquer momento depararia rosto vagamente conhecido; um dos meninos com quem jogava bola no campinho do bairro, um dos professores da escola, a moça que trabalhava na padaria.

Reconheceu foi a mulher que se aproximava. Em dois dias havia descoberto que morava sozinha e trabalhava em um consultório médico como recepcionista. Numa tarde havia almoçado com duas amigas e os assuntos variavam entre as miudezas do cotidiano, os netos de uma delas, aposentadoria, política e a última novela brasileira.

Quando finalmente Téo telefonou, María já sabia que _alguém a procurava_. Mais fácil que cobrir os próprios rastros era torná-los mais visíveis e fazer-se tonto, fingir que havia perguntado pelo povoado porque não sabia seu telefone e não tinha forma de entrar em contato.

Gustavo, nada: já teria ido embora no momento em que o ônibus alcançou a rodoviária, duas noites antes. Téo não encontrou dele nenhum sinal

e envergonhava-se de que houvesse procurado

(tão frágil o ego feito em pedaços)

perguntado nas hospedagens

no posto de gasolina.

Guardou o celular no bolso

resoluto —

e levantou-se, sentindo o tecido da calça grudando na pele com o suor.

— _Tú eres Juan?_

Téo fez que sim.

— _Soy sobrino del Antonio Gonzalez._

Ela ergueu as sobrancelhas em expressão de surpresa. Ficaram ali, encarando-se sem jeito, em pé no passeio, envoltos pelos gritos das crianças. Ela teria sessenta e tantos anos, talvez; os cabelos pintados num castanho avermelhado, curtos, a franja rente às sobrancelhas. Na luz fraca e amarela da rua não se via muito para além da maquiagem e ela parecia mais nova — Téo intuía a idade mais pela cronologia da história do que pela aparência da mulher.

— _Tomamos un helado?_ — ela sugeriu, indicando uma direção com a cabeça.

Sorvete parecia a opção sensata para o calor noturno e Téo só fez concordar e seguir. Havia contado pelo menos seis sorveterias em volta da praça central e outras tantas nos arredores, o que certo atestava contra a sanidade térmica daquele lugar. Sentia-se ainda atordoado. Talvez fosse a viagem, a estrada, o vento seco do deserto que acabava uma ou duas curvas antes de Belén, afundando o povoado num calor úmido e estático. Estivesse pensando em Gustavo ou

não;

Gustavo a expressão tão confusa o olhar tão —

o carro parado no acostamento da rodovia e o vento golpeando o vidro da janela.

A viagem e as horas e as variações de altitude; a umidade inesperada do povoado: não estava pensando direito. Era investigador particular fora de seu território habitual e de que servia afinal um investigador fora de seu território.

Havia dito a María que _me gustaría hablar con usted sobre un amigo suyo, de Tucumán_ e que preferia encontrá-la, e podia ser onde lhe parecesse mais conveniente.

Caminharam em silêncio até a sorveteria. Téo a observava: parecia súbito distraída, remontando talvez uns pedaços de memória distante.

— _Bueno, dime entonces, que es lo que pasó con Antonio?_

Haviam comprado os sorvetes e se sentado numa das mesas de plástico que ocupavam um espaço pequeno antes da calçada. Téo sentiu naquele momento que se desfazia mais uma ponta da investigação

a esperança de que ela soubesse algo

que ao menos soubesse mais do que ele.

Sentia-se tão cansado — um passo fora da realidade. Tanta aridez e tanta poeira, tudo um fim de mundo e o sinal instável de celular. Tanta a necessidade de silêncio e dois dias depois sentia-se pronto para voltar ao rumor caótico da avenida Paulista.

Era o que seu tio buscava? O silêncio do deserto;

a gritaria do vento.

Um norte.

— _Eso lo vine a preguntarle a usted_ — disse.

Ela riu. Enfiou a colherzinha de plástico no sorvete ainda inteiro e mirou Téo, curiosa, como quem se esforça para captar as intenções nas entrelinhas do discurso e do olhar.

— _En serio_ — Téo insistiu, tentando o melhor de seus sorrisos de menino inocente.

— _Mira, yo no sé de Antonio desde hace... qué sé yo, veinte, treinta años?_

Vinte ou trinta anos. Eram já dez anos de incerteza e Téo procurou naquele rosto sinal de que estivesse mentindo. Por que ela mentiria? Se havia concordado em encontrá-lo ali na praça.

Um estranho fazendo perguntas sobre o passado.

Téo pensaria naquela gente que subia e descia do ônibus na estrada, no meio do nada, feito surgissem da própria poeira, do próprio deserto. O tio perdido no deserto da puna argentina. De onde o primo tinha tirado a ideia de que ele seria capaz de seguir rastro tão apagado?

— _Cuándo fue que habló con él por última vez?_

Ela suspirou. Voltou a atenção ao sorvete de framboesa que derretia no copinho.

— _No sé. Hace mucho tiempo._

Algo ainda não estava dizendo. Mais difícil afirmar que mentia; eram os olhos incapazes de se fixar em qualquer ponto. As mãos inquietas entre a colherinha do sorvete e o guardanapo.

— _Antonio es tu tío? Yo no sabía que él tenía familia en Buenos Aires._

— _Soy sobrino de su mujer. Su ex-mujer._

María riu. _Antonio, casado?_, teria dito. Não precisou abrir a boca: Téo leu em sua expressão, tão óbvia.

— _En Buenos Aires?_

Téo fez que sim com a cabeça. Desaparecia dela o sorriso e vinha uma expressão preocupada; como quem se perde numa matemática complexa para além do tempo presente. Fosse ali uma brecha para descobrir algo que ainda não soubesse ou o motivo do espanto ou da falta de memória e _qué sé yo_ vinte ou trinta anos.

— _Qué era lo que querías? Saber de la niñez de Antonio?_

A infância perdida de seus primos. O ódio de Pablo e a insegurança de Roberto. Da infância de Antonio sabia o suficiente para desconfiar que não precisava saber mais. Família desgarrada espalhada pelos povoados da região, perdida pelas províncias vizinhas.

— _Bueno, no_ — admitiu, baixando o olhar com um sorriso que se pretendia envergonhado.

Queria era ir embora. Voltar para Buenos Aires, tomar o voo para São Paulo. Meter-se no silêncio de seu apartamento. Ocupar-se de investigar gente com quem não tinha nenhuma relação, ajudar a desmanchar casamentos e frustrar adultérios, que era o que sabia fazer.

(Tão cansado.)

Pensaria na exagerada brasileirice do pai entre aquele monte de portenho convicto que era a outra metade da família. Seu pai tão paulista tão homem do interior e o colégio completado tardiamente. Sua mãe e sua tia, gêmeas idênticas demasiado idênticas: que fossem ambas se casar com tipo bronco do interior;

ambos: seu pai, seu tio

(aquele tio que não conhecia)

tão doces tão gentis tão toscos.

Os estereótipos.

— _Bueno?_ — ela insistiu.

Téo olhou o copinho de sorvete, quase vazio. Quis pensar no tio e veio na memória uma fotografia velha de seu pai em frente à casa em que moravam, vestido com um macacão jeans e camisa branca amarrotada.

E isso porque havia pensado outra vez em

Gustavo

ele e a mãe na varanda do apartamento conversando atrás da porta de vidro seu pai resmungando sobre os exageros de Pablo

(_tu papá es más vivo que pensás_.)

María o observava, toda curiosidade, esquecida do sorvete. Não sabia por onde andava Antonio; disso Téo teve certeza. Tampouco era tonta: certo adivinhava entre as palavras dele o motivo pelo qual estava ali.

— _Antonio abandonó su familia hace treinta y dos años. Mi primo más grande tenía como siete años._

Ela calada.

— _Y ahora ellos quieren saber que pasó._

Outro silêncio.

As vozes das crianças correndo na praça.

— _Y tú?_

— _Yo nada. Soy el primo que piensa que sabe encontrar gente desaparecida._

María abaixou a cabeça. Empurrou o sorvete para o meio da mesa e deixou-se estar, distraída, buscando na memória o passado distante, a imagem de Antonio; a adolescência naquela mesma praça, tantos anos antes. Quando finalmente falou era como se falasse consigo:

— _Yo no sabía que Antonio estaba casado._

Pousou as mãos sobre a mesa e olhou os dedos abertos; a Téo pareceu que estava fazendo contas. Restava esperar e deixar que sozinha contasse o que ainda lembrava: a vez que em Buenos Aires para o casamento de uma prima havia encontrado Antonio, por acaso. Que haviam conversado, sentados num banco de praça olhando a revoada dos pombos. Que ele parecia triste e ela perguntou por que enfim aquela vida — que estava fazendo na capital? Mas ele não disse que estava casado, não disse que tinha filhos. Perguntou sobre ela; perguntou sobre a vida em Catamarca e quis saber se ela mantinha contato com esse ou aquele amigo, mas.

— _No dijo que estaba casado_ — ela repetiu.

— _Qué dijo?_ — Téo perguntou, num tom de voz suave que era para não desmanchar a neblina da memória.

Mas não havia dito nada — havia feito perguntas e contado que seguia atrás de um volante; que gostava de estar atrás de um volante. Haviam falado de um amigo em comum, o único com quem ela ainda conversava: um que recém havia inventado uma empresa de turismo aos pés da puna de La Rioja para levar os mais aventureiros em circuitos nos Andes.

— Enrique — Téo disse.

Ela pareceu surpresa. Afirmou com a cabeça.

— _Antonio dijo que lo iba a visitar_ — María disse. — _Yo le dije que sí, que quizás podrían trabajar juntos._

Um lugar para onde correr.

Téo sentiu que o coração dava um pulo: esperança renovada. O resto do sorvete de María era um caldo cor-de-rosa no potinho abandonado.

— _Hace... treinta y dos años_ — ela concluiu, devagar, incerta dos próprios cálculos.

Havia em seus olhos um certo desespero. Quase um pedido de desculpas. Repetiu: não sabia que Antonio estava casado. Não sabia que tinha filhos. Não sabia que um comentário assim atirado no vazio pudesse virar do avesso a cabeça dele. Téo quis saber se ela o havia visto depois disso e ela tornou a se perder na conta dos anos. Enfim disse que sim, uma vez.

Alguns anos depois: cinco, seis? Ela não tinha certeza. Ele a procurou em San Fernando, onde ela vivia. Estava de passagem — um serviço temporário no transporte de carga pesada para algum projeto do governo no norte do país.

O encontro havia sido rápido — ele quis dizer oi e como está e que tem feito e não podia ficar e conversar mas queria vê-la _mais uma vez_. Ela se lembrava das palavras. Antonio tinha uma pressa terrível; uma ânsia para recuperar o tempo perdido ou o pavor de algo que vem atrás.

— _Le pregunté que iba a hacer después_ — disse, com um sorriso débil. — _Dijo que iba al sur._

— _Al sur? La Rioja?_

— _No; al sur de verdad. Le pregunté que iba a hacer después de llegar al sur, y me contestó que volvería al norte._ — E riu. — _No lo tomé en serio. Era el tipo de cosa que decía. El Antonio que yo conocía._

## 37.

### Esquel, Chubut

Téo sentiu o ar frio quando pisou do lado de fora da hospedagem. Tirou os óculos escuros do bolso da jaqueta e pôs no rosto, atordoado com a luz branca da manhã que o atingiu de frente. A viagem seria longa: precisava decidir se queria parar no caminho ou se encarava de uma vez as trinta horas até Buenos Aires.

De qualquer forma tomaria o primeiro ônibus que saísse no rumo do norte. Melhor era estar logo em movimento — afastar-se dali antes que o vento o empurrasse adiante

ou o impedisse de sair.

Avisaria o dono da oficina mecânica que estava indo embora e deixaria contato. Cedo ou tarde, Antonio saberia que o procuravam — que um dos filhos o procurava.

Trinta e dois anos depois.

Cedo ou tarde.

Ajustou a mochila maior nos ombros e pensou na sexta-feira anterior. Violeta com o polegar erguido na beira da rodovia. As crianças descalças na terra, esperando conformadas pela carona que — elas sabiam — sempre vinha. Quão impossível podia ser conciliar aqueles dois universos? A resignação furiosa de Pablo e a necessidade incontrolável de seguir adiante, sem nem vestir os sapatos.

Olhou em volta mais uma vez para ter certeza de que o rapaz da noite anterior não ia aparecer de trás de um carro. Que nenhum outro personagem inesperado fosse se meter naquela história a três páginas do final.

A rua estava vazia.

Queria dizer a si mesmo que a viagem não tinha sido em vão: que havia encontrado pedaços importantes de informação e que talvez fosse a Roberto suficiente: conhecer um pouco mais daquele homem inconstante que era seu pai. Quanto mais se podia saber sobre motivos? Provável nem mesmo Antonio soubesse seus motivos, tanto quanto Téo havia descido do carro no meio do deserto, no meio do nada, porque.

Tanto quanto não conseguia encontrar a coragem para escrever uma mensagem para.

Se não há resposta?

Mais algumas quadras e alcançaria a rodoviária. Eram sete e meia. Sequer havia comido antes de sair da hospedagem. Pouco tempo depois tinha nas mãos uma passagem para Bariloche com o ônibus saindo às nove e vinte. Comprou algo para comer e voltou à entrada do lado de fora. Sentou-se num banco de frente ao canteiro entre a avenida e a ruazinha onde estacionavam os táxis.

Tirou do bolso o telefone celular e digitou devagar a mensagem ao dono da oficina mecânica. Depois pensou em escrever ao primo, explicar a tarefa impossível que seria descobrir o _paradeiro_ de um homem que não parava nunca. Eis enfim um circuito: como convinha à alma de caminhoneiro de Antonio Gonzalez.

Desistiu. Doía a vista escrever demais naquela tela minúscula sem os óculos. Não tinha vontade de procurá-los na mochila. Conversaria com o primo quando chegasse. Buscou o número de sua mãe para uma chamada.

— _Juan!_ — a surpresa quando reconheceu a voz do outro lado da linha. — _Donde estás?_

— Esquel.

— _Esquel? Yo pensé que ibas al norte!_

— _Bueno, sí. Fui. Ahora estoy en Esquel._ Estou voltando.

Ela riu. Téo ouviu que falava em português longe do telefone. Reconheceu a voz do pai perguntando sobre Antonio.

— _Y?_ — outra vez para Téo.

— _Lo encontré pero no lo encontré_. Depois explico, pode ser? Quero saber se pode me reservar a passagem de volta pra São Paulo. Devo chegar em Buenos Aires em dois ou três dias.

— _Claro. No te quedás un rato con nosotros?_

— Não. Tenho que voltar pra casa. _Estoy cansado_.

— _Y no te parece mejor descansar un poco antes de volver?_

— Vou descansar quando estiver sozinho em casa.

Houve do outro lado um breve silêncio. Dois mochileiros haviam sentado no gramado ao lado e improvisavam um café da manhã. Téo inclinou o corpo para trás e mirou em frente as sombras compridas projetadas no asfalto. As montanhas que cercavam o povoado, iluminadas pelo sol oblíquo que se demorava no horizonte.

— _Y tu amigo?_

— Que tem ele?

— _Bueno_ — ela disse; quis continuar, mas se atrapalhou com as palavras.

— Ele já voltou.

— _Ah, pensé que._

— Ele tinha que trabalhar.

E quis brincar que algumas pessoas tinham trabalho de verdade e não podiam largar tudo e passar um mês perseguindo fantasmas aos pés dos Andes — o comentário ficou engasgado e Téo abaixou a cabeça, fechou os olhos. Pudesse ausentar-se de si mesmo nos dois ou três dias seguintes e acordar de repente dentro de seu apartamento, em seu quarto, ouvindo pela janela as buzinas do trânsito da manhã. Gustavo sentado na beirada da cama brigando com o nó da gravata

_tenho que trabalhar_

aquele alívio idiota quando ouvia fechar a porta do apartamento e estava sozinho outra vez.

— _Está todo bien, Juan?_ — ouviu a mãe perguntar.

Outro silêncio. Téo ergueu a vista para a morosidade da manhã; os turistas no gramado e dois taxistas conversando na mureta na lateral do prédio da rodoviária. Os táxis estacionados em fila. Incomodado que a mãe pudesse lê-lo daquela forma à distância, pela voz abafada no telefone celular. Quis dizer que estava cansado, que a investigação havia durado tempo demais. Que a viagem de volta seria longa. Sentiu de repente a cabeça leve, o rosto quente; ardiam os olhos com a poeira e a brisa seca que soprava insistente.

— _No_ — disse, passado um momento. Ouvia do outro lado da chamada um burburinho de vozes, a melodia arrastada de uma bossa-nova. — _El pasaje, puede ser?_

— _Sí, claro, mi amor._

— _Dale. Gracias, mamá. Nos vemos._

Desligou sem esperar resposta. Conteve a vontade de bater com o telefone na cabeça e o guardou de volta no bolso. Tirou os óculos escuros para esfregar o rosto com as mãos, secar as lágrimas que —

claro

alguma alergia

a poeira e o ar demasiado seco.

O ego se recupera, não? Mais uma queda. Era o que havia dito a Elisa, no começo do ano: podia levar mais uma queda, só mais uma. Permitir que se demorasse a presença de Gustavo em sua vida por — não era? — pura teimosia. Deixar que as coisas acontecessem. Arrastado então ao sul do continente e perdido em meio caminho _al fin del mundo_, brigando com o vento. Grande merda de ideia se meter Argentina adentro com uma história mal contada e uma fotografia velha. A investigação terminava em pedaços a relação com o primo mais velho feita em pedaços e Gustavo

(se é isso que você quer quem sou eu pra).

Quando abriu os olhos, teve a impressão de reconhecer do outro lado da avenida o rapaz que o havia procurado na noite anterior. Levantou-se num pulo, mas logo a atenção se desviou para uma caminhonete verde recém estacionada na esquina. O homem ao volante abriu a porta e desceu, num movimento vagaroso. A testa franzida com o sol que lhe atingia na cara. Arrumou os óculos e atravessou a avenida em direção à rodoviária.

## 12.

### Avellaneda, BsAs

Por um instante Téo esperou. Sentia os olhos de Pablo lhe atravessando o peito mas manteve ainda a vista baixa na mesa entre eles; a cerveja e os copos e o porta-guardanapos, as manchas no tampo de acrílico.

— _Mirá_ — disse, enfim, erguendo o olhar para o primo. — _Nadie te está pidiendo que_.

Deteve-se. Ia repetir as palavras de seu pai? Adiantava justificar-se? Enfileirar motivos e possibilidades e lógica. Pablo entendia de números — passava a semana afundado em números. Qual a probabilidade que encontrasse Antonio _àquela altura do campeonato_? Pablo talvez soubesse.

— _Nadie te pide que le de una chance al Antonio_ — continuou. — _Te pido una chance yo. A mí y a tu hermano._

— _Para esto viniste?_

— _No vine a convencerte que quizás tu padre sea un buen hombre. Ya está. Ya lo sabemos todo. Sabemos lo que hizo, sabemos lo que pasó._

— _Vos no sabés nada._

Ainda os braços cruzados. Ainda a cadeira afastada da mesa, a perna cruzada criando distância. Uma fúria no olhar que Téo não conhecia.

— Beto —

— _Beto, Beto!_ — Pablo interrompeu. — _Qué sabe Beto? Beto no se importa con nada que no sea idea abstrata._

Para Roberto era isso, disse: um pai abstrato. A ideia de um pai ausente. Téo viu que algumas pessoas olhavam para eles. Pablo não era tipo para baixar o tom de voz quando estava irritado. E a irritação vinha de longe, amparada no tempo presente pelos desentendimentos com as filhas e a esposa e o aborrecimento no trabalho e o empate do Racing contra o Independiente.

Téo continuou calado. Não sabia a hora certa de falar — sabia quando era melhor ficar quieto. Pablo repetiu o que já havia dito antes, por e-mail e por telefone: não queria encontrar o pai. Não queria saber o que havia acontecido e não queria saber onde estava. Não queria encontrá-lo arrependido, pedindo desculpas; tampouco queria descobrir que estava bem e a vida havia seguido adiante.

Não sou uma pedra, teria dito. Havia baixado um pouco a voz, e havia na mudança um tom acusatório. Téo manteve o olhar embora sentisse que ardia a vista. Quis culpar a poluição; o calor. Pablo descruzou a perna e trouxe a cadeira para mais perto.

— _Me da odio la idea de que esté bien_ — disse, muito sério. — _De que esté. De que exista. Me entendés? Puede que esto me haga una persona mala. Pero no soy una piedra._

Ele sabia, disse — sabia que as pessoas aprendiam e se arrependiam e mudavam de ideia e trinta anos é muito tempo, não é? Quem ia acreditar que aquele moleque encrenqueiro ia se formar e se casar e arrumar trabalho e todos os dias sete da manhã terno e gravata e levar as filhas na escola. Trinta anos era tempo demais. Não queria perdoar. Não queria saber que o pai pudesse ter aprendido. Aceitar que houvesse servido de lição para um homem egoísta aprender sobre a vida.

Antonio não era figura abstrata no passado. A ausência era concreta e na maior parte do tempo ele podia esquecer o tamanho dela

(trinta anos é muito tempo)

mas não esquecia o buraco que ela havia deixado. Não esquecia a falta de norte, as brigas com os colegas, os desentendimentos com a mãe e com o irmão. Olhar para trás e saber o quanto deveria ter feito tudo diferente. O quanto se sentia todo errado, todo tempo. A autoestima feita em pedaços. Porque se sentia ainda inadequado como pai; as filhas lhe batiam a porta na cara e ele tinha certeza da própria incompetência e muito provável o casamento não sobreviva a isso, ele disse, e a voz lhe saía como em golpes num saco de pancadas e a gente no restaurante olhava para eles um pouco constrangida ainda que falasse mais baixo.

— _Yo entiendo lo que decís, primo. Pero_ —

— _No entendés nada, Juan. Vos llegaste después. Te quedaste trés años y pico y te fuiste igual._

Téo sentiu a acusação como se fosse ele transformado no saco de pancadas. Pablo nem olhava para ele. Tinha os olhos fixos em algo sobre a mesa, a vista perdida para além da mesa. Você foi a única coisa que fez algum sentido depois que a casa virou do avesso, Pablo disse, num grunhido agressivo, quase incompreensível.

— _Yo?_

— _Vos, Juan, sí. Vos._

— _Yo era adolescente!_

Téo sentiu um calafrio. Lembrou a primeira vez que convenceu a tia a deixar que levasse Pablo ao estádio com os amigos. Ia o pai de um deles. Téo tinha quinze anos e Pablo dez. Disse que não largaria a mão dele, que o botaria nas costas se houvesse tumulto. Que sairia do estádio no meio do jogo no primeiro sinal de confusão. Aos poucos a tia se acostumaria com a ideia — confiaria em Téo como guardião responsável. Téo que se deixava levar pelas ideias inconsequentes dos amigos e atirava latinha vazia na direção dos torcedores do time adversário. Téo que naquele tempo era apaixonado pelo melhor amigo, claro — aterrorizado com a ideia de que alguém pudesse descobrir a verdade. Que o seguia a todo lado; muitas vezes com Pablo a tiracolo — às vezes literalmente.

Pablo havia aprendido rápido: soltava uma ofensa absurda e corria. Os mais desavisados corriam atrás e davam com Téo e o grupo de amigos. Se a briga ameaçava se complicar, fugiam. Pablo era quem ficava de fora, escalava algum muro e gritava quando via a polícia chegando.

Corriam.

Isso tia Anita não sabia, nunca soube. Roberto não sabia. Sempre havia uma desculpa bem calculada para as vezes em que Téo aparecia com um olho roxo.

Na maior parte das vezes provocavam e corriam.

— _El fútbol_ —

— _El fútbol, sí, dale_ — Téo interrompeu, ainda atordoado com o rumo que havia tomado a conversa. — _Yo, no._

— _Vos, sí. El fútbol y vos; era todo lo mismo. Fuiste mi amigo._

— _Yo era un hijo de puta que._

— _El único hijo de puta que fue mi amigo._

Porque tinha vontade de quebrar as cadeiras, chutar os vasos de plantas, atirar as coisas pela janela. Brigava na escola. A mãe não sabia o que fazer. Não tinha para onde deixar correr aquela energia toda, até que Téo aparecesse.

A expressão de Pablo mudou outra vez, como se lhe cobrisse os olhos uma dessas nuvens pesadas que anunciam tempestade.

— _Entonces no, primo. No entendés nada. Me dijiste que te ibas a quedar y te fuiste igual._

Sem o primo mais velho haviam sido alguns anos até que a mãe o deixasse ir aos jogos outra vez, ou mesmo para que tivesse companhia para ver as partidas na televisão. E Téo havia ficado anos sem voltar; voltaria depois de formado, para uma visita breve. Pablo havia acabado de entrar na faculdade e tinha enfim seus próprios amigos com quem ia ao estádio e com quem via os jogos na televisão.

— _Por qué nunca me contaste eso, Pablo?_

— _Para qué?_

— _Qué sé yo; porque es importante._

Pablo desviou a vista. A fúria se acalmava e a substituía outra vez o cansaço aborrecido de antes.

— _Te lo estoy contando ahora._

Tirou os óculos e os jogou sobre a mesa. A armação estava torta numa das pernas. Téo pegou os óculos e mediu no tampo da mesa o desvio. Dobrou devagar a parte torta até que ficasse ajustada com o outro lado. Deslizou os óculos de volta para perto do primo.

O silêncio que veio depois parecia gritar mais alto que o zunido dos carros no cruzamento. O som de louça e copos e gente conversando — e a gente toda conversando num murmúrio cauteloso. Téo não se lembrava de ter dito ao primo pequeno que ficaria. Lembrava-se apenas da confusão dos meses anteriores e decidir o que fazer depois de terminado o colégio; tanto o desespero e tantas as incertezas como convinha a seus dezoito anos recém completados. Sua mãe queria que ele continuasse estudando, que escolhesse uma carreira. Então o amigo que _vamos a São Paulo_ porque.

Quaisquer fossem seus motivos. Téo o seguiria a qualquer lado.

Não pensava no primo — jamais teria adivinhado que.

Não sou uma pedra, Pablo repetiria, ainda, dessa vez num murmúrio; não como você e Roberto. Não tinha deles a curiosidade intelectual que transformava a história em mera ficção. Tinha só um resto de ódio que preferia continuar guardado. Uma infância toda torta.

Téo quis dizer que _bem-vindo ao clube_; se para ele nem havia um pai ausente a quem direcionar a frustração da adolescência. Quis dizer que se não fosse Antonio, seria outra coisa. Um primo inconsequente que foi embora depois de dizer que ficaria. Uma paixão platônica impossível.

— _Te convenció el Gabo a irte, no?_ — Pablo perguntou.

Téo afirmou com a cabeça.

— _Y pelearon unos años después._

Téo confirmou outra vez, vagaroso. Pensava sempre que era história superada, passado remoto. Mas ainda doía, um pouco. Haviam dividido um apartamento por mais ou menos três anos até que não mais se suportassem por quaisquer fossem os motivos mesquinhos que sempre brigavam. Até que o amigo se mudasse para morar com uma namorada. Então a resolução: esquecer que era Juan e inventar novo personagem: Téo. Afastar-se dos velhos amigos e fazer outros.

Que era também saber que não podia ter ficado em Buenos Aires. Que aquele tempo lhe havia servido para saber quem não queria ser.

— _Una amistad para toda la vida_ — Pablo disse, irônico, repetindo o que ouvia dizer a mãe.

Téo serviu ambos os copos de mais cerveja e tomou um gole — como se ali pudesse estar a coragem que lhe faltava para:

— _Yo estaba enamorado de él._

E ergueu a vista para a reação do primo: os olhos súbito muito abertos e sem os óculos pareciam ainda tão maiores. Pablo ergueu uma das mãos e apontou na direção do primo, em pergunta silenciosa, ou para ter certeza de que havia entendido bem. Téo levantou os ombros e tentou um sorriso. O coração lhe golpeava o peito com violência e fazia que lhe faltasse o ar. _Cuarenta y pico años voy ahora decir que_.

— _Pero vos_ —

E Téo mais uma vez só afirmou com a cabeça, naquele gesto lento que era para esconder a reação do corpo, o impulso de fuga, a taquicardia. Se o primo queria odiá-lo que o odiasse por ser mentiroso — não porque adolescente era incapaz de olhar para além do próprio umbigo.

— _Y él...?_

— _No, no. Yo creo que sabía, pero._

Escapou um riso nervoso. Claro que sabia — o havia dito, depois; anos depois. Téo havia desviado o olhar ao copo de cerveja, arrependido já de ter contado mas.

(Porque é importante.)

Aí outro silêncio.

Interminável.

Também Pablo o olhar inquieto em qualquer outra coisa que não o primo, a expressão vazia, como se fizesse contas na cabeça.

Téo olhou em volta o movimento do restaurante já um pouco maior que quando haviam chegado. Quando voltou a encarar o primo, Pablo o encarava de volta. Os óculos ainda na mesa ao lado da cerveja e os olhos castanhos um vazio indecifrável.

— _Por diós, Pablo, deci algo._

Pablo ergueu um pouco as sobrancelhas.

— _Así que sos_ — e interrompeu-se, mirando o primo com uma careta confusa.

— _Gay?, sí._

— _Por qué me lo contás ahora?_

— _Porque sos mi amigo._

Pablo grunhiu alguma coisa. Pôs os óculos de volta no rosto e cruzou os braços.

— _Beto sabe esto?_

— _Sí._

— _Claro que sí. Sabe todo el Beto._

E calou-se. Voltou a atenção ao copo de cerveja e o esvaziou num só gole, demorado. Téo sentia ainda o sangue pulsando nas orelhas, o coração martelando o peito. Inclinou-se adiante com os braços na mesa e tentou explicar que não fosse Antonio ir embora, também ele jamais teria ido a Avellaneda fazer o colégio, por insistência da mãe, pela teimosia da tia. Não teria conhecido Gabo e os outros meninos e não fosse Gabo não haveria o futebol; não fosse o futebol, não estariam ali, ele e o primo, amigos depois de tudo apesar da diferença de idade que era tão maior quando mais novos — e que afinal não fosse Gabo também não teria ido a São Paulo e deixado atrás o primo pequeno outra vez com seu ódio e seus fantasmas. E às vezes as histórias se amarravam daquele jeito, e era impossível desatá-las; não era questão de passo em falso, tomar a esquerda quando devia ter tomado a direita. Não era o pai ter ficado e tudo se resolveria. Não era possível voltar e escolher outro caminho porque a escolha não estava mais ali. Nunca esteve.

Às vezes as coisas aconteciam da única maneira que poderiam acontecer.

Entende o que estou dizendo, Pablo? Entende que faço essa viagem por seu irmão, porque também ele é meu amigo?; porque cada um lida como pode com as adversidades atiradas no caminho. E talvez, sim: talvez para Beto havia restado transformar o pai ausente numa ideia abstrata de ausência — quem vai dizer qual dos dois estava certo?

Pablo esperou que Téo parasse de falar para se levantar. Tirou do bolso a carteira e contou algumas notas, deixou-as sobre a mesa.

— _Yo no sé de Beto_ — disse.

— _Pablo, por favor, sentate._

— _O de vos_ — completou, ainda, num tom ofendido.

Não esperou resposta. Téo havia se levantado mas Pablo já andava para fora do restaurante. Algumas pessoas os observavam, curiosas, e Téo deixou-se cair de volta na cadeira.

## 26.

### RN40–Villa Unión, La Rioja

— _So what do you think, Juan?_ — o holandês ao seu lado perguntou, com o sorriso enorme e uns dentes muito brancos.

Téo abaixou para o colo as mãos segurando o telefone celular e virou-se para olhar por cima dos óculos os outros dois passageiros no banco de trás do carro. O holandês muito cor-de-rosa e a inglesa muito branca, ambos devidamente protegidos do sol com uma camada tão grossa de protetor solar parecia mais que haviam se esfregado com cera. O motorista era um taxista e eventual guia turístico de Belén, conhecido de María. Ao lado dele no banco da frente ia outra inglesa — uma moça muito alta e muito magra e, para sorte de Téo, bastante calada. Era a única do grupo que sabia algo de espanhol e com ela Téo havia conversado para combinar o transporte e dividir os custos da viagem.

— _Qué piensas_ — a inglesa no banco da frente traduziu, em seu sotaque cuidadoso, sem olhar para trás.

Téo havia entendido a pergunta. Sabia suficiente do inglês para entender o que diziam e repetir com Elisa os diálogos de seus filmes favoritos. Mas acabava ali sua disposição para o idioma, e desde o começo da viagem que havia estado calado, digitando no telefone uma mensagem ao primo para contar o que havia descoberto até ali.

— _Perdón_ — disse. — _Que pienso de qué?_

O motorista riu. O holandês olhou a companheira no banco da frente, esperando a tradução com a boca meio aberta em seu entusiasmo de viajante. Era o mais jovem: tinha talvez vinte e poucos anos, e parecia saído de uma dessas comédias de verão — bermuda cáqui e camisa colorida, câmera pendurada no pescoço e as meias brancas na metade da canela. As inglesas teriam algo em torno de trinta anos, as duas. Pareciam mochileiras de longa data; não tinham muita pressa e não se incomodaram quando o motorista atrasou quase uma hora para buscá-los no lugar combinado.

Téo havia ouvido a inglesa comentar que o contato deles em Villa Unión se chamava Enrique; que os buscaria na praça para levar ao hostel e depois conversar sobre o roteiro. Quantas agências de turismo de aventura poderiam existir no povoado? Era uma coincidência conveniente.

— _The Andes_ — o rapaz insistiu, depois que recebeu da inglesa a tradução. — _You’re from Buenos Aires, right?_

Téo abriu a boca para responder, mas acabou fazendo uma careta. Que tipo de pergunta era aquela? O que ele achava dos Andes? _É uma cadeia de montanhas_, pensou em dizer. Olhou pela janela as montanhas que os acompanhavam desde que haviam saído de Belén. O deserto que era cada vez mais deserto, cada vez mais plano e árido e imenso.

Negou com a cabeça.

— Brasil — disse, apenas.

Uma placa solitária indicou as distâncias até os povoados próximos: _Famatina 29_ e _Chilecito 39_. O céu era o mesmo de quando havia tomado a RN40 a primeira vez: um azul histérico quase sem nuvens, à parte as que flutuavam distantes no horizonte por sobre as montanhas e atrás delas.

O cenário também era o mesmo. O mesmo vento golpeando as janelas meio abertas do carro. O mesmo zunido da estrada e as mesmas montanhas, a mesma terra, os mesmos arbustos, as mesmas cores. Não havia ônibus direto de Belén a Villa Unión ou qualquer outra localidade próxima; a outra opção teria sido voltar a Santa María, seguir à capital da província, depois La Rioja etc. Uma volta sem sentido — já não bastasse a volta sem sentido que era ter chegado a Tucumán para depois seguir ao sul, trocar de província mais uma vez.

Ergueu o telefone para perto do rosto. Talvez assim o holandês entenderia que não tinha vontade de conversar. Já não bastasse que o cenário não mudava e que escutava na memória a voz de Gustavo

(o que você quer?)

a cada vez que erguia a vista para a janela.

Ouviu que o rapaz comentava com a inglesa ao lado sobre sua passagem pelo Brasil; que havia ido a Salvador e Rio de Janeiro e às cataratas do Iguaçu. Téo não queria ouvir sobre o Rio de Janeiro. Escorregou no banco, feito fosse capaz de desaparecer. A mensagem na tela seguia incompleta.

María acreditava que Enrique saberia de Antonio. E havia também a informação de Alexandre: tinha o nome da empresa de turismo e um endereço — o endereço certamente desnecessário; o povoado era uma avenida e meia dúzia de quadras. Era o melhor que Téo podia esperar de uma pista naquele meio do nada, embora não fizesse sentido que Antonio pudesse ter abandonado a família para trabalhar com turismo de aventura. Teria ficado um ano e seguido adiante. Primeiro ao norte; depois, talvez, ao sul.

E ao norte outra vez.

Releu a mensagem e guardou o telefone no bolso — teria sinal para enviá-la ao primo quando passassem por Chilecito. Depois, o motorista havia explicado, começava a parte mais complicada do percurso. Pendurou os óculos de leitura na gola da camiseta e pôs no rosto os óculos escuros, cruzou os braços, fechou os olhos. Que a conversa ao seu lado em língua estrangeira seguisse adiante sem sua participação.

Quando despertou do cochilo, o carro estava estacionado e o grupo abria as portas para descer. Viu primeiro o chão de terra seca, a estrada curvando-se à direita no contorno de um morro. Soprava um vento agressivo, ruidoso.

— _Donde estamos?_ — perguntou ao motorista.

— _Cuesta de Miranda._

O cenário era outro. Haviam parado em um mirante protegido por uma mureta metálica, alargando-se na lateral da pista. Téo abriu a porta e desceu. Olhou por cima do carro a paisagem à esquerda: a imensidão de montanhas que os cercava como um mar furioso, alternando tons de cinza, verde e vermelho.

O grupo de mochileiros se ocupava de tirar fotos e o motorista aproveitou para esticar as pernas. Era o trecho entre Chilecito e Villa Unión, Téo sabia: havia entre os povoados uma cadeia montanhosa e a rodovia a cortava no meio — talvez o motivo pelo qual não havia ônibus tão fácil cobrindo aquele percurso.

Andou contra o vento até próximo à mureta. A câmera fotográfica estava na mochila, dentro do carro, e a câmera do celular transformaria a paisagem em uma pintura a óleo amadora. Sentiu que lhe tomava outra vez um desespero — aquela claustrofobia. O vento gritando nos ouvidos.

Não era para ser assim. Não era para se distrair a cada instante do motivo pelo qual estava fazendo aquela viagem absurda. Não era para pensar em Gustavo e pensar que Gustavo devia estar ali. Que Gustavo sim teria algo a dizer sobre os Andes e a vista e as voltas do holandês pelo Brasil. Gustavo com seus sorrisos e a câmera e _pelo menos descruza os braços_.

Ouviu que a inglesa perguntava em espanhol se ele poderia tirar uma foto deles, mas fez-se desentendido. Desviou o olhar ao chão de terra arenosa e andou de volta ao carro. Deixou passar o resto da viagem de olhos fechados, encolhido junto à janela, repassando na cabeça os diálogos decorados de Casablanca, que era sua maneira de contar até cem para fazer o tempo passar.

Foi fácil demais: Enrique os esperava na praça encostado em uma caminhonete branca, toda adesivada com publicidade da agência de turismo e imagens dos roteiros que ofereciam. Parecia mais jovem que Javier — tinha a mesma altura de Téo e parecia forte, os cabelos curtos bastante grisalhos num corte jovial, mui deliberadamente desordenados. As rugas do rosto umas linhas finas na pele bronzeada manchada pelo sol. Cumprimentou o grupo e distribuiu abraços, e depois perguntou em inglês se afinal seriam quatro em vez de três para o roteiro programado.

— _No, yo no_ — Téo respondeu com um sorriso cansado.

O motorista terminava de tirar a bagagem do porta-malas e Enrique voltou a atenção aos outros três.

Era ainda o começo da tarde e o sol estava alto, mas soprava uma brisa fria, espalhando no ar um cheiro de terra misturado à poeira do asfalto. A praça e a rua estavam quase vazias; parecia que a planície desabitada ao redor os envolvia num silêncio profundo, isolando-os do resto do mundo.

— _Igual me gustaría hablar con vos, después_ — Téo disse, quando Enrique virou para ajudar com as mochilas.

— _Sí, claro_ — ele respondeu. — _Ya tenés donde quedarte?_

Téo negou com a cabeça e Enrique gesticulou na direção dos outros, como para perguntar se ele queria ir com eles.

## 38.

### Esquel, Chubut

Téo o reconheceu: o homem da fotografia. Não mudam tanto os adultos depois dos trinta anos, pensou, enquanto esperava que ele se aproximasse, num passo lento, olhando ao redor como se não soubesse bem o que estava procurando. Tinha a barba feita e os cabelos estavam mais curtos e mais brancos; os olhos mais fundos atrás dos óculos de armação quadrada. Usava camisa de manga curta apesar da brisa gelada da manhã.

Parou ao alcançar o canteiro da entrada de táxis. Viu que Téo o observava, e sorriu.

— Antonio — Téo disse.

Um mês.

Uma volta absurda.

Trinta e dois anos.

O homem demorou para se mover. Que o atingisse de repente o peso de sua presença: o que significava estar ali — ir atrás daquele que o procurava. Atravessou a pista da entrada e parou próximo ao banco, um pouco sem jeito. Queria talvez dizer algo, mas a boca se moveu silenciosa, como se as palavras escapassem.

— _No me conocés_ — Téo falou. — _Soy sobrino de Anita._

E estendeu-lhe a mão. Antonio abaixou a cabeça por um instante breve, sacudindo-a em seguida num gesto agitado, feito fosse necessário confirmar a si mesmo a informação recebida; o nome que vinha de tão longe no passado. Apertou a mão de Téo, tomando-a com ambas as mãos e encarando nos olhos o sobrinho distante.

— Juan — Antonio disse, quando finalmente o soltou. Estava sorrindo outra vez. — Juan Miranda Lanari.

— Lanari Miranda — Téo devolveu o sorriso. — _Soy brasilero. Nosotros lo ponemos al revés._

— _Claro, claro. Me acuerdo de ti_ — e marcou com a mão ao lado do corpo uma altura abaixo da cintura. — _Pequeñito así._

Téo não se lembrava dele. Sabia que havia feito uma ou duas viagens à Argentina quando ainda criança, mas não lembrava nenhuma delas para além da ideia vaga de estar no avião, as sombras da casa, o idioma da mãe. Gesticulou para que sentassem, e Antonio ocupou o espaço ao lado dele no banco.

Assim então aparecem os adultos.

De repente.

Sem aviso.

Antonio quebrou o silêncio: disse que estava trabalhando em Trevelin, um povoado pequeno a poucos quilômetros dali, e tivera um problema com a caminhonete. Havia já uns dias que o freio anunciava o defeito e a caminhonete é de 1976 — explicou. Tão fácil se deixava distrair nos detalhes Téo entendeu um pouco o que diziam sobre ele: o homem cheio de histórias sobre a puna e sobre a estrada.

Precisou uma peça — e disse o nome da peça e para que servia e Téo não tinha interesse em mecânica nem nunca havia tido carteira de motorista, mas na narrativa meio tímida de Antonio era como se a peça maldita se transformasse na heroína de um romance épico — precisou de uma peça e ligou na oficina em Esquel, aquela perto da saída, que eles tinham às vezes as peças as mais inesperadas, e ele não podia esperar pedir peça nova vindo da capital ou onde quer que fosse, que ele tinha pressa e o cliente também. Pediu o menino para buscá-la — disse assim, _el chico_, como se Téo soubesse de quem estava falando; depois explicou que era um rapaz que o ajudava às vezes, que era bom menino apesar de ser meio agitado, às vezes meio paranoico, deve ser que vê muita televisão, assiste a uns filmes estranhos.

O menino, ele continuou, veio de moto, falou com o pessoal da oficina e ouviu falar de você — e olhou Téo mais uma vez, e Téo viu em seus olhos miúdos um pouco do primo Pablo, outro tanto de Roberto, e viu também algo mais que era talvez medo ou.

— _El chico que me encontró ayer_ — Téo disse.

Antonio sorriu mais uma vez, envergonhado porque soubesse como havia corrido a interação. Pediu desculpas e reiterou que o menino é boa gente, não faz por mal. É sempre muito prestativo e acho que vê em mim algo da figura do pai que não teve, veja você, quem ia pensar numa coisa dessas — eu que passo naquele povoado uma vez a cada seis, oito meses.

E abaixou a cabeça outra vez, porque sabia aonde ia a conversa — aonde ele mesmo estava levando a conversa. Quando ergueu a vista adiante para a paisagem e as casas iluminadas pelo sol que se erguia às costas deles, Téo percebeu que seus olhos brilhavam.

— _Puedo..._ — e limpou com os dedos por baixo dos óculos as lágrimas que ameaçavam escapar. Respirou fundo. — _Tienes alguna foto... de los chicos?_

Téo buscou no bolso o telefone celular. Ele mesmo não era de tirar fotos — muito menos com aquele telefone — mas teria ali algumas imagens enviadas pelos primos. Antonio ao seu lado tinha as mãos sobre os joelhos, observando atento enquanto Téo passava pelos arquivos na tela.

— Beto — Téo disse, virando a imagem para que Antonio a pudesse ver.

A foto havia sido tirada em algum passeio da família. Beto tinha as costas para uma mureta baixa de concreto, e apoiava-se nela com os cotovelos. Parecia ter se distraído com algo à esquerda, embora ainda no rosto o sorrido posado. Antonio sorriu também, por um lapso de segundo — fez-se sério no instante seguinte. Téo adivinhava no movimento quase involuntário dos lábios o esforço por dizer algo; a busca pelas palavras que não apareciam.

Tomou de volta o telefone. Apertou a vista até identificar na resolução ruim da tela miúda as cores que procurava: o branco e azul celeste das camisas. Ele e Pablo na fila para entrar no estádio, dois ou três anos antes. Pablo tinha os cabelos mais curtos e estavam os dois de gorro, a camisa do time por cima do agasalho. O primo mostrava _três_ com os dedos mas Téo não sabia se era pelo resultado do jogo anterior ou outra referência obscura que certamente teria feito muito sentido no momento da fotografia.

Mostrou a imagem a Antonio.

— Pablo? — ele perguntou, apontando.

— _Pablo y yo, sí._

— Racing.

— _Eso creo fue mi culpa_ — Téo disse, e o riso ficou engasgado.

Porque ouviria outra vez as palavras de Pablo, as acusações de Pablo. Melhor talvez seria não ter encontrado o tio, voltar com uma pilha de informações desencontradas e um rastro apagado. Que pudesse dizer ao primo mais velho que veja, fiz o que me pediu seu irmão, mas. Parecia que o traía mais uma vez ao permitir que o pai o visse tão sorridente. A tela do telefone apagou-se sozinha e Téo o guardou de volta no bolso. Antonio mirou adiante, respirando compenetrado, os olhos outra vez enchendo-se de água.

— _Por qué..._ — ele disse, e interrompeu-se para tomar fôlego. — _Por qué viniste?_

— _Me lo pedió Beto._

— _Por qué?_

Téo ergueu um pouco os ombros, num gesto lento.

— _Quiere saber que pasó._

— _Ahora?_

— _Ahora o antes o después... que diferencia tiene?_

Antonio tinha a vista ainda fixa nas montanhas para além das construções. Concordou com a cabeça, meio ausente. Ergueu os óculos no rosto e tornou a esfregar os olhos.

— _Tienes razón_ — disse, ainda. — _Pasa que... hace tanto tiempo._

E quanto não teria também se esforçado para esquecer aqueles dez anos vivendo em Buenos Aires — a vida da qual precisou fugir? Téo não sabia como ordenar as perguntas na cabeça; a sequência da história, os buracos na narrativa. Temia que Violeta estivesse certa: não haveria respostas. As histórias todas que Antonio contava — como lhe haviam relatado a gente toda por ali — uns causos mais ou menos críveis sobre a vida fantástica de um homem solto na estrada. Se nem mesmo ele soubesse distinguir fato de ficção?

Tanto tempo, não é? A memória se fazia água, ele disse. Tem que juntar as mãos com cuidado para que não escape e um pouco enfim sempre se perde. Ele se lembrava de pegar carona na estrada e lembrava principalmente o quão leve parecia sua mala — a sensação de que havia esquecido algo importante.

— _Unos meses antes_ — Téo disse. — _Enconstraste María en Buenos Aires._

Antonio demorou a responder. Fez que sim com a cabeça.

— _Dijiste a ella que iba a visitar Enrique._

Era algo — retomar a história; dar ao tio um ponto de partida para que começasse a falar. Antonio outra vez confirmou, e sorriu para Téo quando se virou para encará-lo.

— _Parece que ya lo sabes todo._

— _Quizás. Pero me gustaría entenderlo._

Sentiu que viria resposta defensiva — outra pergunta. Antonio abriu a boca, mas logo desfez a expressão obstinada e baixou o olhar. Porque era

isso, não?

Todo aquele último mês: não era só o homem o que procurava.

Não bastava encontrá-lo.

Antonio pôs a mão em seu ombro.

— _Caminamos?_ — e gesticulou adiante com a cabeça. — _Puedes dejar tus cosas en la camioneta._

Téo olhou as horas. A pressa então dissolvida pela curiosidade e maldita curiosidade que tanto o fazia se meter onde não tinha sido chamado. Seu tio Antonio, aquele desconhecido. A ideia tão abstrata de um pai ausente. A ausência que indiretamente havia sido responsável por sua adolescência nos subúrbios de Buenos Aires e toda uma lista de escolhas erradas que fez pelo caminho.

Que volta absurda precisava ter dado para chegar ali.

— _Dale_ — respondeu. — _Dejame antes cambiar un pasaje._

## 13.

### Buenos Aires, BsAs

Era noite quando saiu do cinema. Buscou no bolso o telefone mas não conseguiu ler mensagem nenhuma; duas do primo Roberto, outra de Gustavo. Algumas chamadas não atendidas. Tampouco conseguiu ver as horas. Sentia os olhos cansados e a cabeça doía. O corpo estava quente mas soprava uma brisa gelada e não havia trazido a jaqueta.

Seguiu à direita, encolhendo-se, deixando-se levar pelo movimento de pedestres na calçada larga da avenida.

Gostava de estar ali. Talvez porque adolescente era dos poucos lugares a que ia sozinho. Era quando se sentia mais inteiro — como se pertencesse a si mesmo. Quando esquecia o colégio, as aulas e as tarefas e os trabalhos atrasados; quando esquecia as paixões impossíveis e o tanto que não se encaixava entre os amigos. Era outra forma de desaparecer.

Porque havia o estádio e o grito da torcida e transformar-se na multidão — mas havia também essa solidão silenciosa entre o burburinho dos carros, as conversas alheias, as luzes dos cinemas e das lojas e dos teatros. Eram, como não?; artifícios. Estratégias para se transformar no que não era. Inventar-se por negação: deixar de ser. Tão pura alegria dissolver-se na multidão.

Solidão era não precisar ser coisa nenhuma: primo, sobrinho, filho, amigo; o primo mais velho que veio para salvar o primo pequeno de si mesmo; o sobrinho responsável.

Uma amizade para toda vida.

Distraiu-se com a vitrine de uma loja de música. Uma televisão grande montada na porta mostrava a apresentação de uma orquestra tocando Vivaldi. Algumas pessoas haviam parado para observar a gravação como quem assiste a uma performance de rua, ao vivo. Também isso ele lembrava: a falta de pressa de quem caminhava por ali à noite. Essa gente que saía do trabalho e não queria voltar para casa.

A figura refletida no vidro da vitrine parecia mais um espectro de um tempo futuro, embora ainda quase tão magro quanto aos dezessete anos; tinha menos cabelo e estava mais branco. Penteou os cabelos para frente com os dedos. Sentiu que piorava a dor de cabeça e lembrou que não havia comido desde o almoço.

Entrou num café de esquina. Pediu água e um sanduíche. Aos poucos se diluíam as imagens do filme e voltavam agressivas as palavras de Pablo. O restaurante em Avellaneda. A cerveja vazia sobre a mesa e a vontade de desaparecer — a gente toda disfarçando os olhares em sua direção. Havia depois caminhado até a ponte e seguido adiante, a pé. Os caminhos todos decorados. Meteu-se no metrô e saiu na avenida Corrientes, pouco antes de escurecer.

Téo quis dizer a si mesmo que era tudo exagero — aquele ódio. A responsabilidade que não lhe cabia. Dali algumas semanas estariam mais uma vez falando sobre futebol, como se nada houvesse acontecido. Pablo é uma criança, Roberto às vezes dizia. Não dá pra ser criança aos trinta e nove anos uma filha de catorze outra de onze.

Em algum momento o telefone tocou.

— _Te busco, primo?_ — veio a voz de Roberto.

— _No, no hace falta._

— _Seguís en Avellaneda?_

— _No, pero está bien. No me esperen para la cena._

— _Donde estás? Te busco, no hay drama._

— _No. Está bien. Vuelvo más tarde. No me esperen._

Afastou o telefone da orelha e ouviu que Roberto perguntava mais alguma coisa; desligou antes que ele terminasse de falar. Antes que perguntasse sobre a conversa com o irmão. A dor de cabeça havia melhorado depois de comer, mas Téo sentia pesado o cansaço do dia. Pediu uma cerveja e deixou passar o tempo observando o movimento.

Não queria mais pensar em Pablo. Que se podia esperar do adolescente que havia sido? Tudo que havia feito tinha sido porque lhe disseram outros: as provocações na saída do estádio, a mudança para São Paulo, a escolha da faculdade. A única coisa que queria era a única coisa que não podia ter.

O que não podia ser.

Pablo havia sido criança perdida no fogo cruzado.

Depois da segunda cerveja saiu outra vez para a rua e caminhou até encontrar outro cinema, outro filme; outra distração — uma desculpa para se abrigar do vento gelado. Não queria voltar e conversar e explicar-se e fingir que estava tudo bem, não se preocupem, Téo Miranda sabe o que está fazendo. Queria sair logo daquela cidade maldita que ele teimava em chamar de sua, à parte que havia vivido ali só três anos e meio — nos subúrbios.

A nostalgia do cativeiro.

Era madrugada quando finalmente voltou à casa. A sala estava escura e o único som ali dentro era o motor da geladeira na cozinha. Passou no banheiro para jogar água na cara e escovar os dentes. Deslizou cauteloso até o quarto de hóspedes e encontrou Gustavo deitado na cama mais alta, meio encolhido próximo à parede, a coberta enroscada nas pernas. O telefone celular ainda junto à mão. Chegava pela janela algo da luz fraca da rua, suficiente para que Téo pudesse tirar os jeans e trocar a camiseta. Tirou o telefone da mão de Gustavo.

— Téo?

— Está tarde.

Gustavo murmurou alguma coisa incompreensível e Téo meteu-se na cama junto dele.

— Você está frio — Gustavo disse, num sussurro.

— Desculpa. Não tomei banho. Vou pra outra cama.

— Não, espera.

Buscou a coberta das pernas e a trouxe para cima de Téo, ainda com os olhos fechados. Acomodou-se para ocupar o espaço que lhe restava, encostando-se em Téo como se para ter certeza de que ele ficaria. No instante seguinte parecia já adormecido. Téo sentia nos ouvidos as batidas do coração. Sabia que não dormiria tão fácil mas restava fechar os olhos, concentrar-se no calor da cama e a respiração serena de Gustavo ao seu lado.

## 27.

### Villa Unión, La Rioja

A placa na entrada dizia “Hostel La Nómada”. A dona era irmã de Enrique; ela havia se mudado para estar perto dele alguns anos antes, depois da morte da mãe, e ele havia ajudado a montar o negócio. A área principal tinha a cozinha integrada com uma sala grande; mesas, sofás e televisão dividiam o espaço com o balcão da recepção, algumas estantes de livros e um emaranhado de quadros e fotografias em todas as paredes. Téo deixou as mochilas no quarto e voltou. Enrique organizava uma pilha de folhetos de publicidade sobre o balcão.

— _Bueno, flaco_ — disse — _cómo te puedo ayudar?_

Téo apontou uma das mesas para que se sentassem. Pousou sobre ela a caderneta de anotações e tirou de lá a fotografia de Antonio posando de má vontade em frente ao portão da casa em que moravam. Enrique buscou no bolso da camisa os óculos, trouxe a imagem para perto e sorriu ao reconhecer o amigo.

Depois ficou sério, de repente. Tirou os óculos.

— _Cómo era tu nombre?_

— Juan.

— _Sos..._ — e apontou a foto, depois Téo, depois a foto outra vez.

— _Sobrino_ — Téo explicou. — _Sobrino de Anita. La esposa de Antonio._

Enrique deixou escapar um suspiro aliviado; deteve-se em seguida, apoiando os cotovelos sobre a mesa e escondendo por um instante o rosto com as mãos. Téo o observou, calado. Era mais fácil quando não precisava fazer perguntas. Os últimos dias o haviam ensinado que não gostava de ser o detetive que fazia perguntas.

Enrique apoiou o queixo nas mãos.

— _Lo buscás_ — disse, então.

Téo confirmou com um gesto da cabeça. O grupo de estrangeiros voltava do quarto e a inglesa que sabia espanhol se aproximou, aproveitando o silêncio para intrometer-se na conversa.

— _Está bien se hacemos el almuerzo primero?_ — perguntou.

Enrique lhe sorriu e disse que não tinha pressa. Explicou que precisava de qualquer forma resolver um assunto importante — e apontou Téo sentado a sua frente. O grupo se reuniu em volta do balcão da cozinha para discutir a comida e Enrique voltou a atenção à mesa. Téo não havia almoçado — não tinha fome. Sabia que precisava comer mas muito fácil se distraía com outros temas: Enrique a expressão entre culpado e curioso e a boca meio escondida atrás das mãos. Organizava talvez a memória na cabeça; os fatos no tempo.

— _Él vino para acá, no?_ — Téo perguntou.

— _Sí._

— _Quizás entonces me podés ayudar a entender que pasó._

— _Te puedo ayudar a entender lo que pasó. Pero no sé si te puedo ayudar a entender por que._

E a boca curvou-se num sorriso meio triste. Téo viu que a moça havia deixado os outros dois na cozinha e sentado no sofá com um livro. Guardou a fotografia de volta dentro da caderneta e a empurrou para o canto da mesa.

Contou então o que sabia. O que lhe haviam dito Javier e María e o pouco que sabia Beto. Enrique ouviu com atenção. Tinha no dedo indicador da mão direita um anel largo e prateado que ele girava devagar, distraído. Esperou que Téo terminasse de falar para enfim afirmar com a cabeça.

— _Yo lo encontré la última vez que estuve en Buenos Aires_ — disse. — _Le conté de la idea esta. Yo y una camioneta, viste._

Não era grande coisa, naquele tempo. O turismo na região era quase inexistente e ele precisava sempre buscar a gente em La Rioja. Fez parcerias com agências grandes e aos poucos foi encontrando público. Muitas vezes se fazia motorista para roteiros mais tradicionais, porque precisava também pagar as contas. Quando foi a Buenos Aires, era para entrar em contato com algumas agências. Por força do hábito procurou Antonio, e conversaram.

Não se lembrava exatamente quanto tempo depois — dois ou três anos — Antonio apareceu ali, sozinho, com a mala pequena e a cara de quem tinha visto assombração. Enrique o recebeu em sua casa. Nos meses seguintes Antonio ajudou com alguns serviços: buscava os turistas na capital da província, fazia transporte entre os povoados. Depois conseguiu trabalho com uma empresa de logística e fez-se caminhoneiro outra vez. Voltou alguns anos depois, de passagem.

— _Me dijo que iba al sur_ — Enrique disse.

— _Y después al norte otra vez?_

Enrique riu. A inglesa pôs-se de pé, chamada pelos companheiros, e andou vagarosa até o balcão da cozinha. Enrique havia jogado o corpo para trás e unido as mãos sobre a barriga, pensativo.

— _Llegar al norte es fácil._

O norte é uma linha imaginária onde acaba este país e começa o outro, disse. Uma linha mais ou menos reta delimitando o alcance da nossa ambição.

O sul, não: o sul não acaba nunca.

— _Así que no te parece que Antonio se fue por María o... la artesana esta de que me contó Javier_ — Téo perguntou.

— _No, no creo._

— _Pero creés que está en el sur._

Enrique afirmou com a cabeça e riu outra vez. Ria mais por costume do que por ver graça na situação. Sua expressão era quase toda sorrisos, mesmo quando se fazia sério. O grupo de estrangeiros havia ocupado uma das mesas próximas e comia em silêncio, feito pudessem sentir na conversa o peso da história, mesmo sem o significado das palavras.

Antonio queria seguir ao sul pela RN40, Enrique disse. A Patagônia era um sonho de menino. Se havia abandonado a família para conhecer a Patagônia? Parecia absurdo dizer aquilo assim, em voz alta. Mas ele também não sabia. Mesmo Antonio se esquivava de falar sobre o tema. Mudava de assunto. Dizia que não podia ter ficado. Às vezes se perdia em tautologias: foi embora porque foi embora; não podia ficar porque foi embora porque não podia ficar porque.

A inglesa tinha o livro de antes aberto ao lado do prato, mas parecia distraída com outra coisa, a vista perdida adiante. Seus companheiros vez ou outra trocavam palavra, em voz baixa, respeitosos. Téo abaixou a cabeça. Sentia de repente um cansaço terrível; os olhos tão pesados. Mais uma vez no meio do nada num desvio da _Ruta 40_ e era preciso continuar: ao sul.

(O sul não acaba nunca.)

Quando investigava suspeitas de infidelidade dava ao cliente um prazo de uma ou duas semanas. Havia chegado em Buenos Aires numa quinta-feira. Era sexta-feira, duas semanas depois. Duas semanas e um dia.

Se nem mesmo o tio sabia por que tinha ido embora.

Ou era pedir que parasse o carro descer em plena pista no meio do deserto, no meio do nada e

por quê?

— _No sé que te dijo Javi, pero lo conozco. Es una ilusión eso de la ruta_ — Enrique disse. — _Como el asfalto nuevo que brilla cuando sale el sol._

— _Y te parece que buscarlo es como buscar los diamantes estes?_ — Téo tombou a cabeça de lado, medindo com cuidado a reação do outro.

Enrique esteve calado por um instante. Um sorriso sutil fazia supor que se divertia com as próprias ideias. Ou repetia na cabeça a pergunta de Téo, pesava-a compenetrado contra o que sabia. Téo viu que a inglesa olhava para eles. Estivesse todo tempo prestando atenção na conversa, treinando o ouvido para o idioma estrangeiro.

— _No_ — Enrique disse, por fim, pronunciando com cuidado cada letra. — _Pero si buscás el que los busca..._ — e ergueu os ombros devagar — _un poco tenés que hacer el mismo camino._

Depois pediu desculpas. Antonio contava muitas histórias, mas todas sempre pela metade. Aquela havia sido a última vez que o havia visto; quando passou para dizer que tinha comprado uma caminhonete, que ia a La Rioja resolver os papeis da compra e depois seguiria ao sul pela RN40. Fazia já uns vinte ou vinte cinco anos — não tinha certeza; havia parado de contar o tempo depois da virada do milênio.

Não sabia por onde andava. Não sabia se estava vivo.

Disse que o mais lógico seria começar em Bariloche: que buscasse com as empresas de transporte e logística. Que algum rastro devia existir, embora tivesse quase certeza que Antonio teria continuado mais ao sul. Os povoados turísticos dali em diante eram os que tinham mais movimento, mais chance de trabalho. Não havia muito depois de Bariloche além do turismo e uns povoados fantasma ou aglomerados de fazendas parados no tempo, distanciando-se uns dos outros cada vez mais.

— _No te gusta la Patagónia?_ — Téo perguntou.

— _No_ — Enrique não precisou pensar para responder. — _Sí es lindo; los lagos y las montañas y el paisaje. Pero son todos iguales y el viento trae un polvo gris que se mete adentro en el alma._

A Patagônia é desses lugares dos quais é impossível escapar — ou que fazem sair correndo para nunca mais voltar. Não há meio termo. É fácil se perder quando a paisagem se repete a cada cinquenta quilômetros rodados, ele disse. Faz parecer que não saímos do lugar e é preciso continuar em frente.

Téo trouxe a caderneta de anotações para perto de si outra vez. Tirou de lá a fotografia do tio: o homem encostado no portão. Quis dizer que entendia o impulso, mas calou-se. Não entendia a inquietação da busca; conhecia o anseio pela solidão. Caminhar sem rumo pela avenida Paulista depois do cinema, à noite; descer ao centro num passo demorado e distrair-se com o movimento da cidade. Quando pela manhã Gustavo saía do apartamento e Téo ouvia distante o soluço do elevador que se punha em movimento.

— _Mi padre murió el año pasado_ — a inglesa disse, aproveitando o silêncio que tomou conta do ambiente.

Téo e Enrique a olharam. Também seus companheiros, embora não compreendessem o que dizia. Foi do coração, ela explicou, em sua voz grave, escolhendo as palavras no melhor que podia fazer de seu espanhol. Pouco tempo depois, encontraram um bilhete dentro de um caderno velho. Era uma carta de despedida à esposa, com data de vinte anos antes. Carta que sua mãe nunca havia recebido. Ele pedia desculpas, mas explicava que precisava ir embora — que estava enlouquecendo; que deixaria toda a família louca; que a família o faria enlouquecer ainda mais rápido.

Mais da metade do texto tratava de questões burocráticas: senhas do banco, instruções sobre o que fazer com a casa de campo. Mas era principalmente uma carta confusa, que tentava em vão descrever um desespero inexplicável.

A carta ficou guardada e ele nunca foi embora.

— _Él no tuvo coraje_ — disse, e sorriu, melancólica. — _Yo pensé eso primero: que no tuvo coraje_ — corrigiu-se. — _Pero quizás es necesario más coraje para quedarse, no?_

## 39.

### Esquel, Chubut

Caminharam até a entrada do povoado e seguiram à direita acompanhando o rio, passando pela praça gramada em que Téo havia antes se despedido de Violeta. Antonio ainda explicava o problema com a caminhonete, a manutenção que precisava fazer, qualquer outra peça que logo teria que substituir. Feito pudesse preencher o espaço com uma pilha de palavras — algumas que a Téo pareciam inventadas — e assim fazer esquecer os últimos trinta e dois anos e que se pode fazer com o passado se o presente tão concreto nos exige a atenção.

Calou-se quando contornavam o quarteirão, tomando outra vez a direita e dando as costas para a rodovia. Abriu e fechou a boca, suspirou, tentou outra vez.

— _Mira, yo no sé que pasó_ — disse, enfim, e suspirou mais uma vez. — _No sé que pasó cuando me enamoré de Anita, y no sé que pasó cuando... me fui._

Porque lhe havia ocorrido que precisava ir embora. Pensava o que havia sido terminar o relacionamento com María: a certeza inicial de que havia tomado uma decisão ruim — que a estrada não era o conforto que buscava e a puna não seria capaz de preencher aquele vazio histérico que lhe gritava no peito. Mas havia sido, não é? O vazio se havia calado. Ele sorria um sorriso triste, e queria talvez convencer a si mesmo da própria narrativa. Anita era tudo que alguém como ele jamais poderia ter sonhado e que absurdo, aquela moça tão inteligente achar graça nas piadas dele caminhoneiro sem instrução e querer vê-lo outra vez divertir-se quando ele explicava sobre mecânica e outras ladainhas logísticas que nem a ele interessavam tanto assim.

Ele não se lembrava daqueles anos — sabia que havia ido a Buenos Aires para vê-la. Que havia ficado alguns meses e feito por ali alguns trabalhos e de repente tinha um emprego como motorista de ônibus e.

Casaram-se. Os anos seguintes foram um sonho esquisito: acostumar-se ao ritmo da cidade ao ritmo de Anita à família de Anita aquela gente tão estranha que discutia literatura à mesa do jantar e sabia os nomes dos antepassados todos, os italianos e os espanhóis. Ele lembrava alguns dos percursos e lembrava a avenida Callao e a praça San Martin.

Lembrava-se de brincar com os filhos nos domingos.

Mas lembrava-se principalmente do terror que o tomava quando pensava que não, não pode ser, algo não está certo. Quando o atacava a certeza de que era capaz de ir embora, e que a estrada curaria as feridas todas, igual havia curado antes.

Sabe o que é olhar a pista que corre por baixo do viaduto — e querer saltar? Mirou Téo de canto de olho, meio tímido, como se pedisse desculpas pela bagunça que estava fazendo com a memória, e você parece um homem que estudou, não é? Filho de Julieta, claro, como não ia ser?

Téo não queria falar de si ou de suas escolhas equivocadas de faculdade porque sim, claro, filho de Julieta; a faculdade era nada mais que a obrigação. Quis dizer que conhecia o impulso de atirar-se do viaduto, como não? — que às vezes o medo de altura era isso, apenas: o medo de se sentir compelido a pular. Onde havia lido isso?

Claro que havia lido em algum lugar

(você parece um homem que estudou)

se ele mesmo preferia sempre o pé no chão desaparecer entre a multidão descer ao centro no fim do dia encontrar aquele o mesmo bar de sempre ou um cinema desses alternativos os filmes estranhos que ninguém ficava sabendo que existiam. Que ele sabia de viadutos e asfalto e.

Continuou calado e deixou que Antonio continuasse. Que repetisse o que havia dito antes a Enrique as tautologias todas e foi embora porque foi embora porque.

_Me entiendes?_

Se era capaz de abandonar a família, como ia ter ficado? Antonio tinha outra vez os olhos úmidos brilhando com a luz forte do sol da manhã. Que pai era esse capaz de abandonar os filhos? Não adiantava ter ficado, entende? Sabia-se capaz de ir embora e como ia viver consigo sabendo-se capaz de.

— _Y ahora?_ — Téo perguntou.

— _Ahora nada. Perdí el tiempo igual que la memoria._

Entre os dedos, feito água.

— _Tienes hijos, Juan?_

— _No._

— _Casado?_

— _No._

— _Más fácil, eh?_

Téo riu. Afirmou com a cabeça, incerto. Algo o incomodava. A curiosidade dissolvida num aborrecimento. Antonio tomou fôlego outra vez:

— _Mira: yo no quise ser el padre que debería haberse ido_.

E deteve o passo; tomou Téo pelo braço para que parasse também, e o encarou de frente. Ali na borda do povoado a rua de terra a poeira misturada a um vago odor de esterco novo. Pela esquerda um portão de madeira cinzenta na entrada de uma chácara; a cerca que acompanhava a estrada seguia junto a uma fileira de ciprestes que eram como gigantes os observando. Um murmúrio de galinhas resmungando na distância. Os olhos de Antonio tão miúdos por trás dos óculos: uma súplica desesperada.

— _Y María?_ — Téo perguntou.

Deu um passo atrás para livrar-se da mão de Antonio em seu braço. O sorriso para disfarçar o incômodo; que precisasse ali libertar aquele homem que mais de trinta anos depois pedia redenção pelos erros do passado.

Antonio abaixou a cabeça e cutucou com a ponta do sapato as pedrinhas na terra seca. Depois pôs a mão nas costas de Téo e gesticulou adiante para que continuassem caminhando.

— _María... me abrió los ojos._

— _Qué dijo?_

— _No, no dijo nada. Pero nos encontramos en Buenos Aires y lo tomé como una señal._

— _Una señal de que tenías que irte?_

— _Sí_ — disse, mas depois sacudiu a cabeça, gesticulou com as mãos na frente do corpo como se daquela forma pudesse agarrar as palavras que todas erradas lhe atrapalhavam os pensamentos. — _No._

— _No?_

— _Me fui a Tucumán porque la amaba. Me fui de Buenos Aires porque amaba a mi familia. Y todavía los amo._

Buscou em Téo qualquer sinal de que a explicação não se perdia no vazio. Téo caminhava com os olhos adiante, as mãos no bolso da jaqueta. Sentia-se de repente inquieto — um mês atrás daquele homem e quando o encontrava a história fazia ainda menos sentido. Quanto mais falava, menos fazia sentido; as palavras se transformavam num devaneio mais ou menos narcisista de um homem inseguro.

— _Me entiendes?_ — Antonio perguntou.

— _No_ — Téo disse, e negou ainda com a cabeça. — _No entiendo._

Antonio parou outra vez. Esfregou as mãos uma na outra, num gesto aflito, e distraiu-se olhando um gato que caminhava cuidadoso por sobre um telhado inclinado.

— _No puedo quedarme_ — disse, os olhos muito abertos, como se confessasse algo importante ao animal do outro lado do portão. — _Qué padre puedo ser? Estuve en Buenos Aires por diez años y todos los días_ — e sacudiu a cabeça, devagar — _todos los días... pensava en irme._

Os olhos outra vez enchiam-se de lágrimas, mas ele rápido tirou os óculos e esfregou o rosto com a manga da camiseta. Soltou um riso abafado. Téo sentia aumentar a inquietação; um aperto no peito, um princípio de náusea. A irritação com aquela ladainha vazia que se pretendia grave e terrível — os exageros todos porque fosse incapaz de saber o que queria da vida. Sentia o sangue pulsando nas orelhas embora a manhã continuasse fria e a caminhada naquele passo lento não fosse suficiente para fazer tirar a jaqueta. O gato saltou do telhado a uma pilha de caixas de madeira amontoadas na lateral da casa.

— _Y ahora?_ — Téo perguntou, quase num sussurro.

— _Tú, Juan, tienes cuántos años?_

Téo puxou o ar com força como para combater a ilusão de que súbito lhe faltava o fôlego. Antes pudesse culpar a altitude; Esquel estava talvez a uns quinhentos metros acima do nível do mar. Não era fome, pensou. Não dessa vez.

— _Cuarenta y cuatro._

— _Así que sabes lo que es dejar que pasen diez años sin darte cuenta de ellos._

— _Sí_ — Téo respondeu, contrariado.

Dez ou vinte anos — as inseguranças da adolescência que se arrastavam à vida adulta e então quarenta e quatro anos construindo desculpas as mais absurdas porque.

Antonio olhou o gato uma última vez e indicou com a cabeça outra rua que seguia à direita, na direção de onde haviam começado a caminhar. Téo retomou o passo ao lado dele, ainda as mãos metidas nos bolsos da jaqueta, em ambas o punho cerrado os dedos abraçando os polegares.

— _Diez, veinte, treinta años_ — Antonio completou, com um movimento dos ombros.

— _Gardel_ — Téo disse, e Antonio o mirou com curiosidade. — _Que es un soplo la vida, que veinte años no es nada._

— _Ah, sí. Carlos Gardel. No sé mucho de los tangos._

Olhou adiante e pôs-se a cantar um _zamba_, a voz mais grave tão inesperadamente afinada _no sé para que volviste si yo empezaba a olvidar_. Téo reconhecia o ritmo, embora não a canção. Se sabia algo da música do norte argentino era o pouco que Roberto o havia feito escutar. E também Roberto

(você parece um homem que estudou)

_mi zamba vivió conmigo_

_parte de mi soledad_

tão portenho tão intelectual tão artista e ideias abstratas

_no se si ya lo sabrás_

_mi vida se fue contigo_

preferia como Téo escutar o jazz de terras distantes

_que mal me hace recordar_.

Claro que não era fome não era a altitude não era o lanche que havia comido na rodoviária; porque a cabeça leve e a garganta apertada e algo que era como um buraco queimando no peito os olhos ardendo com a poeira e a luz do sol que depois da curva os atingia direto na cara. Antonio ao seu lado seguia cantarolando com a boca fechada, e outra vez enxugou os olhos com a manga da camiseta.

Era tudo de uma pequenez tão ridícula e sim: os devaneios narcisistas de um homem inseguro

(não sirvo pra essas coisas)

que houvesse se agarrado não a um ideal abstrato e impossível de liberdade mas sim à ideia vaga de que devia ir embora

(não sou boa companhia)

não podia ficar porque capaz de ir embora

(se é isso que você quer quem sou eu pra)

e então o tempo por entre os dedos, feito água.

— _Y ahora sos feliz?_ — Téo perguntou.

— _No_ — a resposta pronta, como se surpreso pelo absurdo da pergunta. — _Pero igual no hago infeliz a nadie._

## 14.

### Buenos Aires, BsAs

Acordou com a voz sonolenta de Gustavo próxima ao ouvido, dizendo _buen día_ em seu espanhol carioca. Tinha dormido em cima do braço. Girou o corpo como podia no espaço estreito da cama de solteiro compartilhada. Gustavo estava deitado de lado, as costas coladas à parede, ainda os olhos fechados.

— Que horas são? — Téo perguntou.

Os ruídos da cozinha chegavam ali abafados. Gustavo esticou-se para alcançar o braço de Téo que usava o relógio, trouxe-o para perto do rosto e abriu um dos olhos para ler as horas.

— Oito e meia.

Téo sentou-se num pulo e deixou escapar um grunhido aborrecido.

— Era pra gente sair cedo.

— Você chegou tarde.

— E a sua desculpa qual é?

— Teu primo me fez beber com ele.

Téo virou-se para encará-lo e Gustavo riu. Tinha já os olhos meio abertos embora parecesse prestes a voltar a dormir.

— Vai pegar estrada de ressaca.

— Meio litro d’água e um café e já estou bom.

— Ainda dá tempo de desistir e ficar em Buenos Aires.

Gustavo respondeu com um resmungo incompreensível. Sentou-se também, esfregou o rosto. Aproximou-se de Téo para um beijo apressado e deslizou até a ponta da cama.

— Você não se livra de mim assim tão fácil — disse. — Ficar um pouco longe de cidade vai me fazer bem.

Abriu a porta do quarto e saiu. Téo ouviu abrir e fechar a porta do banheiro no corredor. Buscou o telefone celular para ler as mensagens do dia anterior.

Roberto, às 19h34: “Venís para la cena? Gustavo dijo que va a cocinar.”

Outra vez Roberto, 20h16: “Te puedo buscar. Estás en Avellaneda?”

Gustavo, 21h02: “Não esquece de comer.”

Pensou em escrever a Elisa, mas só fez reler o que ela havia escrito dois dias antes: “Você vai sentir a falta dele quando ele for embora. Faça seu trabalho e pare de resmungar.”

Gustavo voltou e concentrado cuidou de arrumar suas coisas, trocar de roupa. Téo ainda o telefone nas mãos e o olhar perdido. A cabeça vazia. Por saber que devia pensar em

algo;

o primo Pablo e

(_no entendés nada_).

— Não sou eu com pressa sentado de cueca olhando a parede, Téo.

Gustavo terminava de abotoar a camisa. Téo se distraiu observando-o: o movimento calculado dos dedos. Não importava a pressa: Gustavo demorava-se igual em cada um dos botões — sempre o mesmo gesto medido. Parecia às vezes que o tempo parava em volta dele até que terminasse de se vestir.

— Como foi com seu primo? — Gustavo perguntou.

— Como eu pensei que seria.

— Difícil?

Téo sacudiu a cabeça. Olhou a mochila aberta e a roupa do dia anterior que havia jogado na cama de baixo. Ainda tinha sono.

— Pelo que entendi de Beto, Pablo é um cara meio complicado.

— Meio complicado sou eu — Téo disse.

Gustavo riu. Sentou-se ao lado dele e passou o braço em suas costas. Téo apoiou a cabeça em seu ombro e fechou os olhos.

— Que aconteceu? — Gustavo perguntou.

— Contei pra ele que — respirou fundo. — Falei sobre aquele meu amigo, no colégio. O motivo idiota pelo qual eu fui pra São Paulo.

— E?

— E nada.

Moveu o corpo para frente como se para levantar, mas só fez se livrar do braço de Gustavo. Continuou sentado.

— Se ele não deixar de falar comigo depois dessa investigação talvez eu saiba o que ele tem a dizer. Talvez eu saiba se ele me odeia porque fui embora, porque menti ou.

— Porque você é gay.

Téo mirou Gustavo de canto de olho. A cabeça de volta na discussão com o primo — o monólogo furioso do primo. Sentia como se os anos todos de companheirismo se diluíssem ali, na ousadia de Téo para uma investigação improvável; uma revelação improvável. Era um exagero. Quantas vezes Pablo havia brigado com ele por motivos os mais diversos, os mais absurdos? Esquecia tudo algumas semanas depois em um e-mail furioso reclamando de outras coisas, perguntando como está o trabalho e você acredita a ousadia daquele árbitro aquela jogada não era cartão amarelo nem na copa do mundo.

— Pois é.

Levantou-se. Pegou a roupa na mochila e disse que ia tomar banho, mas parou na porta do quarto. Gustavo também pôs-se de pé; um dos braços esticados enquanto dobrava a manga da camisa.

— Me faz um favor? — Téo pediu.

— Sim?

— Distrai meu primo pra mim.

— Como assim?

— Não sei; conversa com ele. Pra ele não ter espaço pra perguntar sobre ontem. Não quero falar sobre ontem.

— Claro.

Viu que Téo hesitava com a mão na maçaneta da porta e devagar adiantou-se até ele, puxou-o perto para um abraço. Téo apoiou a cabeça no ombro de Gustavo e deixou-se abraçar, os braços meio soltos ao lado do corpo.

Assim estiveram por um momento, calados.

— Às vezes as coisas dão errado, Téo.

Que era dessas constatações vazias

tão agressivamente óbvias.

Téo respondeu com um grunhido, a cara ainda escondida e a noite mal dormida pesando nos músculos. Estivesse mais disposto, devolveria um comentário irônico.

(Às vezes.)

— A gente conserta depois — Gustavo completou.

Téo se afastou com meio passo para trás. De que estavam falando? Gustavo bem sabia o quanto algumas coisas não se podiam consertar. A cicatriz enorme que ele tinha na barriga e o hábito persistente de girar no dedo anular uma aliança inexistente.

Virou-se e abriu a porta do quarto, mas outra vez se deteve, olhou Gustavo atrás dele.

— Você está melhor?

Gustavo pareceu surpreso com a pergunta. Desviou o olhar para a mala arrumada junto à porta, deu de ombros. Depois fez que sim com a cabeça e disse que ia comer alguma coisa para ver se passava logo a dor de cabeça. Roubou de Téo um beijo e passou por ele, saiu do quarto e se afastou pelo corredor.

Então à locadora de automóveis e depois à casa dos pais; a mãe lhe havia pedido que passasse antes de seguir viagem. Gustavo o deixaria na esquina e _vou dar uma volta no quarteirão ver se encontro onde estacionar, qualquer coisa me espera depois aqui mesmo_.

— Seu amigo não quis subir? — Julieta perguntou.

— Não tem onde parar o carro.

Joaquim apareceu na entrada da cozinha, segurando uma xícara com as mãos cheias de sabão:

— Falou com Pablo?

— Sim.

— Como foi?

— Péssimo.

Joaquim deixou cair os ombros num gesto de frustração teatral. Olhou na direção da esposa, depois do filho outra vez, e achou melhor voltar à cozinha.

Téo ouviu o som de louça e água corrente. O pai murmurando um samba como se nada pudesse lhe estragar o domingo. Julieta havia se afastado até a estante de livros e perguntava sobre o caminho até Córdoba, onde pensavam em parar, quanto a locadora ia cobrar a diária do carro. Téo meteu as mãos nos bolsos, ainda em pé próximo à porta.

— Que era que você queria?

— _Bueno, si te olvidastes el libro._

Pegou dois livros que estavam deitados por sobre os outros no canto da estante. Voltou e os entregou ao filho.

— Dois?

— _Si no te gusta uno, tenés el otro._

— Tenho que ir.

Fez menção de chamar o pai mas Julieta ergueu a mão para que esperasse; aproximou-se dele.

— _Pará un minuto, Juan._

— Gustavo está esperando. Dando voltas no quarteirão.

— _Está bien. Un minuto, no más._

Téo cruzou os braços e a encarou. A expressão que se pretendia séria apressada aborrecida mas que tão inevitável deixava transparecer o desespero. Sentiu que lhe fugia o sangue do rosto; um calafrio. A conversa que ele não queria ter. Não ali, não naquele momento.

— _Me alegra que tengas_ — e distraída passou os dedos na camiseta de Téo como se houvesse ali alguma sujeira um fio de cabelo as palavras que lhe escapavam. — _un amigo como Gustavo._

Téo desviou o olhar, passou a mão nos cabelos; teria ainda aberto a boca feito fosse capaz de articular uma resposta. A reação engasgada. A mãe outra vez o interrompeu com um gesto.

— _Se preocupa por vos. Te quiere bastante._

— Por que você está me dizendo isso? — foi o que Téo conseguiu dizer, ainda a vista inquieta adiante.

— _Por nada_ — ela sorriu. — _Para que lo sepas. Porque es importante._

Téo parou com a vista na varanda, por onde entravam os ruídos da rua. Pudesse acreditar nela; incomodavam os sentimentalismos todos. Gustavo a mensagem às nove da noite e _não esquece de comer_. Não era mais adolescente e parecia tudo um exagero — tanta importância a um relacionamento. Se ele nem sabia como funcionar dentro de um relacionamento. Todos aqueles meses e ainda não sabia.

Não ia aprender nunca.

(Porque é importante.)

Fechou os olhos por um instante.

— _Bueno_ — e tornou a olhá-la; o sorriso esforçado. — Era isso?

— Era isso, Teodoro — ela disse, forçando o pronúncia dura do ‘t’ e do ‘d’ para imitar o sotaque brasileiro. — _Buen viaje._

— Tenho que ir.

Ela riu. Passou por ele e abriu a porta do apartamento.

— _Hacete un favor, Juan: no seas tan dramático_ — ela ainda falou; feito pudesse ler seus pensamentos. — _Te va a salir todo bien._

Téo concordou com a cabeça, mais por hábito que convicção. Chamou o pai e gritou uma despedida, abraçou a mãe e desceu correndo a escadaria com os livros debaixo do braço.

## 40.

### Bahía Blanca, BsAs

Passava de três da manhã mas Téo ainda os olhos muito abertos mirando adiante a escuridão em movimento — ali naquela primeira fileira de assentos no ônibus de dois andares a janela enorme à sua frente. Ligou a luz sobre o banco. Um rapaz de vinte e poucos anos dormia encolhido no assento ao lado.

Buscou na mochila a caixa com os óculos e o livro que mal havia começado. Seguia ainda na página quinze. _La pesquisa_; a capa mostrava a ilustração de uma lâmpada pendendo de um fio branco, um pedaço de teto. Uma capa feia. Abriu na última página e leu algo sobre um prato de azeitonas e uma tempestade que se aproximava no horizonte. Assim terminava: azeitonas, a tempestade anunciada, e estava chegando o outono.

Passou a mão pela barba de dois dias que incomodava. Odiava-se com barba, porque se os cabelos apesar da idade quase nada de fios brancos a barba crescia sempre cinzenta e sentia-se tão velho. Com a luz via no vidro da janela o próprio reflexo: as olheiras atrás dos óculos e o cansaço e a barba e os cabelos desordenados fazendo aparentes as entradas que ele tanto se esforçava para disfarçar.

Voltou a atenção ao livro; outra vez a página quinze: _se sentía amargo y lúcido, confuso y alerta, cansado y decidido_, leu, e fechou o livro em seguida. Não seria capaz de fingir interesse num comissário de polícia sofrendo também ele suas próprias crises de meia idade. Como ia fazer que fosse embora a inquietação se o coração de repente dava um salto imprevisto e disparava em galope por dez ou quinze ou trinta minutos. Porque era ridículo que insistisse em se comparar com aquele tio ausente e odiava que sentisse por ele um pouco de pena um excesso de compaixão. O impulso da fuga — claro que Téo o conhecia. Que falhasse com Gustavo era a prova de que melhor não se meter a besta inventar relacionamento acreditar que seria capaz de mudar tolerar a companhia por mais de

duas, três semanas;

mas.

Mas era ridículo: as desculpas todas. Téo não queria pensar no mal-estar aquele desespero o vazio dolorido que lhe apertava o peito mas inevitável que o levasse consigo na viagem de volta junto das mochilas e do cansaço e do livro que não conseguia ler.

Porque sim: ficar exigia também coragem — talvez exigisse mais coragem do que ir embora. A coragem inventada para provocar torcedor do time adversário e correr quando a situação ficava ruim. A coragem de saber que os amigos o defenderiam — ele que nunca havia sido tipo do mais robusto e caía fácil depois do primeiro golpe. A faculdade em São Paulo com os pais lhe pagando o aluguel do apartamento.

A coragem que ele não tinha; nunca havia tido.

_A gente conserta depois_; claro, Téo. Não seja tão dramático. Trinta e dois anos depois e Antonio que _perdí el tiempo igual que_.

Que ia dizer ao primo; aos primos?

Se nem sabia se valia a pena: contar toda a história, a sua e a do tio; as voltas absurdas entre o deserto aos pés da puna e a cordilheira e a Patagônia. O deserto sem fim planície infinita entre montanhas e a Patagônia eterno meio de caminho no rumo do fim do mundo.

Foi embora porque foi embora porque.

_Hay que seguir adelante_, Javier havia dito.

Transformar-se na estrada.

Isso Téo não precisava perguntar quando destruía casamentos com um envelope lacrado: valia a pena?

Tampouco a frustração com o caso encerrado: o tio encontrado e a história um emaranhado de desculpas pretextos tão cuidadosamente empilhados uns sobre os outros até que não fosse mais possível enxergar para além deles. Que diferença fazia? Roberto teria uma resposta e Pablo;

seus exageros, todos eles.

Pablo teria mais um motivo para odiar o pai e jogar sobre ele a culpa que sentia porque incapaz de ser para as filhas o pai ideal que ele pensou que havia perdido. Era — sim? — um pouco ridículo.

(_Hacete un favor, Juan._)

Também era ridículo que _pare o carro por favor_ e como ia explicar o desespero que lhe apertava a garganta a paisagem repetida o vento golpeando a janela do carro feito quisesse fazê-la em pedaços.

(_No seas tan dramático._)

Assim, em retrospecto.

Escondido de olhos fechados atrás de mais uma pilha de desculpas absurdas os devaneios narcisistas de um homem inseguro autoestima instável baseada em.

Bastasse dizer que agora não

agora não quero conversar

e por que lhe ocorriam as palavras sempre quando não mais precisava delas?

Tudo que também o tio foi incapaz de fazer e então trinta e dois anos passados e agora é tarde agora perdi o tempo agora não há mais volta: o pai que devia ter ido embora; e foi. Algumas coisas não têm conserto. E Téo também como aquele tio destruindo sem olhar o que lhe cruzasse a frente por incapacidade de comunicação e necessidade de preservar qualquer ilusão idiota de integridade. Pura covardia a alegria ao caminhar à noite na avenida Corrientes: era solidão e era pertencimento

e era principalmente ridículo.

Apagou a luz sobre o assento. A estrada escura corria aos seus pés e era como se estivesse voando. Pensar nas últimas semanas e sair de São Paulo sair de Buenos Aires tantas as vezes que havia fechado o zíper da mochila conferido em volta se esquecia alguma coisa e tomado o rumo de ainda outra cidade, outro povoado. Como pode viver assim?

Violeta com o polegar erguido na beira da rodovia e os filhos conformados acostumados única vida que conheciam. Que ela e Orlando e todos eles esses artesãos calças coloridas agasalhos estampados alpargatas remendadas — não soubessem viver de outra forma.

Mas as desculpas?

Em algum momento deve ter dormido. Quando acordou já era dia e o ônibus seguia vagaroso por ruas movimentadas. Passavam de sete da manhã.

— Bahía Blanca — o rapaz ao seu lado, já muito desperto, comentou.

Era a última parada antes de Buenos Aires — porque não quis esperar em Neuquén até o dia seguinte para um ônibus direto, e supôs que dali haveriam mais opções de linhas no rumo da capital federal.

Quando pisou fora do ar condicionado sentiu primeiro o ar pesado da cidade portuária. Recuperou a bagagem, encontrou os guichês das empresas de ônibus e comprou a passagem para o trecho que lhe faltava. Pensou em sentar ali num daqueles bancos feios de concreto e tentar mais uma vez vencer a página quinze do livro começado. Mas era ainda a inquietação que desde Esquel havia tomado conta dele e mais dez horas de estrada até Buenos Aires e depois o voo no final do dia seguinte não seria capaz de se concentrar em coisa nenhuma. Tudo que ainda precisava dizer ao primo Roberto. O que não podia dizer a Pablo.

Parou em frente a uma _lanhouse_ pequena. A parede de vidro deixava ver o lado de dentro: três cabines estreitas com computadores velhos e duas cabines telefônicas separadas por divisórias de acrílico. Os computadores todos ocupados. Tirou do bolso o telefone celular. Uma mensagem de Roberto: “avisame cuando llegues te busco en la terminal”.

Empurrou a porta de vidro e entrou na _lanhouse_ com o passo decidido. Parou calado ao lado do balcão: a funcionária falava ao celular, distraída folheando uma revista de moda. Tinha a franja cortada reta na altura das sobrancelhas e os cabelos longos presos meio de lado, feito houvesse saído de um filme do começo dos anos 1990. Ergueu para ele as sobrancelhas e Téo apontou na direção das cabines telefônicas.

— _Callate un minuto, boluda, hay gente_ — ela disse, e apoiou o telefone sobre a revista. Olhou Téo com a expressão aborrecida. — _Para donde?_

— Brasil.

Ela voltou ao celular. Digitou alguma coisa no computador e depois mais uma vez à pessoa do outro lado da linha _che, bueno, que lo vas a decir al pelotudo este_ o sotaque arrastado as consoantes todas quase inexistentes. Apontou uma das cabines com o queixo e voltou a atenção à revista.

Faltava pouco para oito e meia. Téo discou o número decorado e não precisou esperar muito para ouvir a voz conhecida.

— Sou eu.

Seguiu-se um silêncio. Não fosse o murmúrio de vozes e teclados do outro lado da linha, Téo pensaria que a ligação tinha caído. Encostou a cabeça na parede ao fundo da cabine e ajustou a posição do telefone junto à orelha. As mochilas amontoadas a seus pés não deixavam muito espaço para se mover. Ouviu a voz de Gustavo distante _me dá um minuto_ e em seguida diluiu-se o ruído de fundo.

— Onde você está?

— Numa _lanhouse_ horrorosa na rodoviária de Bahía Blanca.

— Bahía Blanca.

— Em três horas sai o ônibus pra Buenos Aires.

Outro silêncio.

— Amanhã à noite estou em São Paulo.

Gustavo calado. Téo olhou pela porta frágil de acrílico vagabundo a bagunça do lugar; os viajantes aproveitando a espera do ônibus para entrar em contato com a família, compartilhar fotografias, matar tempo. O colorido das revistas e publicidades e embalagens de bala e chocolate e alfajor. Depois de tanto o cinza da Patagônia aquele excesso de cores parecia aos olhos uma agressão.

— Gustavo.

— Diga.

— Como você está?

Gustavo hesitou. Téo ouviu que cumprimentava alguém e muito educado _bom dia_ e _já estou indo_.

— Vou entrar numa reunião.

— Agora?

— Agora.

— Certo.

— Téo.

Estivessem os dois brigando com as palavras. Téo já nem sabia por que havia ligado. Era sempre Gustavo que começava as conversas todas.

— Me manda um e-mail com o número do seu voo — Gustavo disse.

— Você não precisa me.

— Eu sei — ele interrompeu. — Tenho que ir.

— Espera.

Porque devesse dizer algo. Era cedo e não havia dormido quase nada na viagem noturna até ali, mas havia ligado para dizer algo. A coragem inventada. Havia se metido naquela _lanhouse_ feia e interrompido a conversa mui importante da funcionária com a amiga — porque queria dizer algo. Porque Elisa tinha razão e Roberto tinha razão e Violeta tinha razão e a mochileira inglesa tinha razão e sua mãe, claro — sua mãe sempre tinha razão.

Mas era também Gustavo quem costumava saber o que dizer.

— Téo, agora não. Estão me esperando.

— É que.

E calou-se outra vez. A moça atrás do balcão continuava ao telefone. Que tanto essa gente encontrava para falar? Que tanto podia haver para ser dito?

— Às vezes — e buscou na memória as palavras certas — às vezes as coisas dão muito errado.

— Tudo bem, Téo. — Gustavo disse, e respirou fundo antes de completar: — A gente conserta depois.

