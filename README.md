# Norte

WIP. Novel. PT_br.

_Se você fala outro idioma, mas lê português e gostaria de tentar traduzir, manda ver._

#### Sinopse

_(assim que eu tiver uma.)_

"Qué sé yo, es desierto y hay mucho viento." -- Téo Miranda.

#### Status

19/mai/2022: terminada primeira revisão do livro; ele ainda vai passar por um monte de revisões e mudanças, como convém.
17/mai/2022: terminada primeira versão do livro.

#### Projeto

Vou atualizar este repositório ([codeberg.org/olivia/norte](https://codeberg.org/olivia/norte)) conforme modifico o texto para quem estiver interessado em ler e acompanhar. Aceito ideias, sugestões, revisões, comentários e reclamações, mas não garanto que serão incorporadas ao texto. Fique à vontade pra criar um _fork_ e brincar com as cenas ou criar cenas alternativas.

Todo meu trabalho está sob a licença [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/deed.pt_BR). Você pode compartilhar como quiser, adaptar e transformar o meu trabalho, desde que dê crédito e indique as mudanças feitas e, também, distribua o resultado sob a mesma licença.

#### Como ler

No livro, os tempos se misturam. Em outras palavras: dividi o tempo da narrativa em três partes, e botei uma do lado da outra.

A ordem dos capítulos ainda está sujeita a mudanças. Os números indicam a ordem _cronológica_ da história mas não a ordem da leitura. Pode ler na ordem que estão organizados aqui -- é pra ser fora de ordem mesmo e os números são um pouco para confundir. 

#### SRC

Uso um programa open source chamado [novelWriter](https://novelwriter.io) que usa formatação em markdown e permite organizar notas, visualizar contagem de palavras e criar referências entre notas e capítulos com facilidade de forma simples. O programa exporta em markdown comum com dois cliques, que é como jogo o texto aqui no repositório. Não compartilho todos os arquivos do projeto porque escritor tem vergonha das próprias anotações. 

Já usei:

- libreoffice (pois é)
- wordgrinder (bom editor de texto de linha de comando)
- ghostwriter (editor de texto em formato markdown)

#### Arquivos adicionais no repositório

A pasta "/insp\_" guarda imagens relacionadas ao projeto.

O arquivo "notas.md" é autoexplicativo, mas não tem muita coisa lá. O arquivo de notas verdadeiro está oculto porque é uma bagunça.

Em "/src" há também arquivos gerados por scripts que calculam e informam meu progresso via contagem de palavras.

#### Aos nerds curiosos

Devo publicar em breve neste repositório os scripts que uso para publicar o romance e calcular a contagem de palavras e progresso. NERD.

#### Autora

Olivia Maia, artista e escritora desterrada.

Mais informações sobre meu trabalho no meu site: [oliviamaia.net](https://oliviamaia.net).
